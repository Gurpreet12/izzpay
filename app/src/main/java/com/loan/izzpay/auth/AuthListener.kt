package com.loan.izzpay.auth

interface AuthListener {

    fun onStarted()
    fun onStarting()
    fun onSuccess()
    fun onFailure(message:String)
}