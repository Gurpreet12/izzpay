package com.delivery.izzpayloan.auth

import android.view.View
import androidx.lifecycle.ViewModel
import com.loan.izzpay.auth.AuthListener

class AuthViewModel : ViewModel() {
  var authListener: AuthListener? = null
  //Login
  var email: String? = null
  var password: String? = null
  //Registration
  var email_reg: String? = null
  var password_reg: String? = null
  var phone_reg: String? = null
  var name_reg: String? = null
  var address_reg: String? = null
  var refer_reg: String? = null

  fun onbLoginIsButtonClick(view: View) {
    authListener?.onStarting()
    if (email.isNullOrEmpty()) {
      authListener?.onFailure("Enter the email")
      return
    } else if (password.isNullOrEmpty()) {
      authListener?.onFailure("Enter the password")
      return
    } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email.toString()).matches()) {
      authListener?.onFailure("Enter the valid email")
      return
    } else {
      authListener?.onSuccess()
    }
  }

  fun onRegisterClick(view: View) {
    authListener?.onFailure("onRegistration")
  }

  fun onForgetPswdClick(view: View) {
    authListener?.onFailure("Forget Password")

  }

  fun onRegisterDone(view: View) {

    //authListener?.onStarting()
//        if (email_reg.isNullOrEmpty()) {
//            // show error messages when no UI interact by user
//            // show the error message to stop the function.
//
//            authListener?.onFailure("Enter the email")
//            return
//        } else if (password_reg.isNullOrEmpty()) {
//            authListener?.onFailure("Enter the password")
//            return
//        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email_reg.toString()).matches()) {
//            authListener?.onFailure("Enter the valid email")
//            return
//        } else if (password_reg?.length!! < 4) {
//            authListener?.onFailure("Password must be at least 4 chars long!")
//        } else if (phone_reg.isNullOrEmpty()) {
//            authListener?.onFailure("Enter Phone No")
//        } else if (name_reg.isNullOrEmpty()) {
//            authListener?.onFailure("Enter Name")
//        } else {
    authListener?.onSuccess()
    // authListener?.onFailure("Phone")

//        }}

  }
}
