package com.loan.izzpay.fragments

import android.app.ProgressDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.loan.izzpay.R

import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.loan.izzpay.RetrofitConnections.ApiClient
import com.loan.izzpay.adapters.AdapterLoanCenter


import com.loan.izzpay.models.ListOfLoansModel
import retrofit2.Call
import retrofit2.Callback
import androidx.recyclerview.widget.LinearLayoutManager
import com.loan.izzpay.utils.Constant


class FragmentTypeOfLoans : Fragment() {

    private lateinit var adapterLoanCenter: AdapterLoanCenter

    var myRecyclerview:RecyclerView ?= null
    lateinit var progerssProgressDialog: ProgressDialog
    var dataList: ListOfLoansModel? = null

    lateinit var sharedPreferences: SharedPreferences
    private val sharedPrefDB = "izzpay_db"

    private lateinit var linearLayoutManager: LinearLayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v: View = inflater.inflate(R.layout.fragment_type_of_loans, container, false)


        myRecyclerview = v.findViewById(R.id.myRecyclerview)as RecyclerView
        linearLayoutManager = LinearLayoutManager(activity)
        myRecyclerview!!.layoutManager = linearLayoutManager

        sharedPreferences = activity!!.getSharedPreferences(sharedPrefDB, Context.MODE_PRIVATE)

        progerssProgressDialog = ProgressDialog(activity)
        progerssProgressDialog.setTitle("Please wait...")
        progerssProgressDialog.setCancelable(false)
        progerssProgressDialog.show()

        getListOfLoans()

        return v
    }


    private fun getListOfLoans() {

        if (Constant.hasNetworkAvailable(activity!!)) {

            val Access_Token = sharedPreferences.getString("Access_Token", "")

            try {
                val call: Call<ListOfLoansModel> = ApiClient.getClient.listOfLoans("Bearer "+Access_Token)
                call.enqueue(object : Callback<ListOfLoansModel> {
                    override fun onFailure(call: Call<ListOfLoansModel>, t: Throwable) {
                        progerssProgressDialog.dismiss()
                        Toast.makeText(activity, t.message, Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<ListOfLoansModel>, response: retrofit2.Response<ListOfLoansModel>) {

                        progerssProgressDialog.dismiss()

                        dataList = response.body()
                        if (dataList == null)
                        {

                            Toast.makeText(activity, "No data found", Toast.LENGTH_SHORT).show()
                        }
                        else{
                            val code = dataList!!.getMessage()
//                            Toast.makeText(activity, code, Toast.LENGTH_SHORT).show()

                            adapterLoanCenter = AdapterLoanCenter(activity!!,dataList)
                            myRecyclerview!!.adapter = adapterLoanCenter
                        }
                    }

                })

            } catch (ex: java.lang.Exception) {
                Toast.makeText(activity, ex.message.toString(), Toast.LENGTH_SHORT).show()
            }

        } else {
            Toast.makeText(activity, "No network available!", Toast.LENGTH_SHORT).show()
        }
    }
}



