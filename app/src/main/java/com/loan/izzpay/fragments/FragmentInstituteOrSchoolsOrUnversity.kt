package com.loan.izzpay.fragments

import android.app.ProgressDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.loan.izzpay.R
import com.loan.izzpay.RetrofitConnections.ApiClient
import com.loan.izzpay.adapters.TypesCustomFragmentInstituteSchoolsCollegesAdapter
import com.loan.izzpay.models.InstitutesModel
import com.loan.izzpay.utils.Constant
import retrofit2.Call
import retrofit2.Callback

class FragmentInstituteOrSchoolsOrUnversity : Fragment() {

    private lateinit var typesCustomSchoolsCollegesAdapter: TypesCustomFragmentInstituteSchoolsCollegesAdapter

    var myRecyclerview_new: RecyclerView?= null
    lateinit var progerssProgressDialog: ProgressDialog
    var dataList: InstitutesModel? = null

    private lateinit var linearLayoutManager: LinearLayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.fragment_institute_or_schools_unveristies, container, false)

        myRecyclerview_new = view.findViewById(R.id.myRecyclerview_new)as RecyclerView
        linearLayoutManager = LinearLayoutManager(activity)
        myRecyclerview_new!!.layoutManager=linearLayoutManager

        progerssProgressDialog = ProgressDialog(activity)
        progerssProgressDialog.setTitle("Please wait...")
        progerssProgressDialog.setCancelable(false)
        progerssProgressDialog.show()

        getProfile()


        return view
    }

    private fun getProfile() {

        if (Constant.hasNetworkAvailable(activity!!)) {
            try {
                val call: Call<InstitutesModel> = ApiClient.getClient.institutesApi()
                call.enqueue(object : Callback<InstitutesModel> {
                    override fun onFailure(call: Call<InstitutesModel>, t: Throwable) {
                        progerssProgressDialog.dismiss()
                        Toast.makeText(activity, t.message, Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<InstitutesModel>, response: retrofit2.Response<InstitutesModel>) {

                        progerssProgressDialog.dismiss()

                        dataList = response.body()
                        if (dataList == null)
                        {

                            Toast.makeText(activity, "No data found", Toast.LENGTH_SHORT).show()
                        }
                        else{
                            val code = dataList!!.getMessage()
//                            Toast.makeText(activity, code, Toast.LENGTH_SHORT).show()

                            typesCustomSchoolsCollegesAdapter = TypesCustomFragmentInstituteSchoolsCollegesAdapter(activity!!,dataList)
                            myRecyclerview_new!!.adapter = typesCustomSchoolsCollegesAdapter
                        }
                    }

                })

            } catch (ex: java.lang.Exception) {
                Toast.makeText(activity, ex.message.toString(), Toast.LENGTH_SHORT).show()
            }

        } else {
            Toast.makeText(activity, "No network available!", Toast.LENGTH_SHORT).show()
        }
    }

}
