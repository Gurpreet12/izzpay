package com.loan.izzpay.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import com.loan.izzpay.R
import com.loan.izzpay.activities.ActivityBaseForFragment
import com.loan.izzpay.activities.ActivityCertificateUpload
import com.loan.izzpay.activities.ActivityProofs

class FragmentHome : Fragment() {

    private lateinit var cardview1: CardView
    private lateinit var cardview2: CardView
    private lateinit var cardview_photo_proof: CardView
    private lateinit var cardview4: CardView

    private lateinit var cardview_Instant_Cash:CardView
    private lateinit var cardview_apply_loan:CardView
    private lateinit var cardview_credit_score:CardView
    private lateinit var applyLoanBtn:Button
    private lateinit var firstConstraintLayoutLoanCalculator:ConstraintLayout
    private lateinit var secondConstraintLayoutLoanEligibility:ConstraintLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.fragment_home, container, false)

        cardview_photo_proof = view.findViewById(R.id.cardview_photo_proof)

        cardview_Instant_Cash = view.findViewById(R.id.cardview_Instant_Cash)
        cardview_credit_score = view.findViewById(R.id.cardview_credit_score)
        cardview_apply_loan = view.findViewById(R.id.cardview_apply_loan)
        applyLoanBtn = view.findViewById(R.id.applyLoanBtn)
        firstConstraintLayoutLoanCalculator = view.findViewById(R.id.firstConstraintLayoutLoanCalculator)
        secondConstraintLayoutLoanEligibility = view.findViewById(R.id.secondConstraintLayoutLoanEligibility)

        cardview_photo_proof.setOnClickListener {
            startActivity(Intent(activity!!, ActivityProofs::class.java))
        }
        cardview_Instant_Cash.setOnClickListener {
            val i = Intent(activity, ActivityBaseForFragment::class.java)
            i.putExtra("checkPage", "instant_cash")
            startActivity(i)
        }
        cardview_apply_loan.setOnClickListener {
            val i = Intent(activity, ActivityBaseForFragment::class.java)
            i.putExtra("checkPage", "apply_loan")
            startActivity(i)
        }
        applyLoanBtn.setOnClickListener {
            val i = Intent(activity, ActivityBaseForFragment::class.java)
            i.putExtra("checkPage", "apply_loan")
            startActivity(i)
        }

        firstConstraintLayoutLoanCalculator.setOnClickListener {
            val i = Intent(activity, ActivityBaseForFragment::class.java)
            i.putExtra("checkPage", "calcultor")
            startActivity(i)
        }

        secondConstraintLayoutLoanEligibility.setOnClickListener {
            val i = Intent(activity, ActivityBaseForFragment::class.java)
            i.putExtra("checkPage", "eligible")
            startActivity(i)
        }
        cardview_credit_score.setOnClickListener {
            val i = Intent(activity, ActivityBaseForFragment::class.java)
            i.putExtra("checkPage", "credit_score")
            startActivity(i)
        }

        return view
    }

}
