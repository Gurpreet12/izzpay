package com.loan.izzpay.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

import androidx.fragment.app.Fragment
import com.loan.izzpay.R



class FragmentEducationLoan : Fragment() {


    private lateinit var edu_heading1:TextView
    private lateinit var descTv:TextView
    private lateinit var sign_back_arrow:ImageButton



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val v: View = inflater.inflate(R.layout.fragment_education_loan, container, false)


        val TitleLoan=getArguments()!!.getString("TitleLoan")
        val Description=getArguments()!!.getString("desc")
        val url_of_image=getArguments()!!.getString("Image")

        edu_heading1=v.findViewById(R.id.edu_heading1)
        descTv=v.findViewById(R.id.descTv)
        sign_back_arrow=v.findViewById(R.id.sign_back_arrow)

        edu_heading1.setText(TitleLoan)
//        descTv.setText(Description)

        sign_back_arrow.setOnClickListener {
            activity!!.finish()
        }
        return v
    }

}


