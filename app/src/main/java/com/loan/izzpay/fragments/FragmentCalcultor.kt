package com.loan.izzpay.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.loan.izzpay.R




class FragmentCalculator : Fragment() {

    private lateinit var result: TextView
    private lateinit var TI:TextView
    private lateinit var repayment:TextView
    private lateinit var processing_fees:TextView
    private lateinit var seekbar1:SeekBar
    private lateinit var seekbar2:SeekBar
    private lateinit var seekbar3:SeekBar

    private lateinit var cal_back_arrow: ImageView


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v: View = inflater.inflate(R.layout.fragment_calculator, container, false)

        cal_back_arrow = v.findViewById(R.id.cal_back_arrow)

        val P = v.findViewById(R.id.principal) as EditText
        val I = v.findViewById(R.id.interest) as EditText
        val M = v.findViewById(R.id.years) as EditText
        TI = v.findViewById(R.id.interest_total)
        repayment = v.findViewById(R.id.repayment)
        result = v.findViewById(R.id.emi)
        seekbar1 = v.findViewById(R.id.c_seekbar1)
        seekbar2 = v.findViewById(R.id.c_seekbar2)
        seekbar3 = v.findViewById(R.id.c_seekbar3)
        calcuate(P.text.toString(),I.text.toString(),M.text.toString())
        P.addTextChangedListener(object : TextWatcher {
            var st1:String?=null
            var st2:String?=null
            var st3:String?=null
            override fun afterTextChanged(p0: Editable?) {
                st1 = P.text.toString()
                st2 = I.text.toString()
                st3 = M.text.toString()

                if (TextUtils.isEmpty(P.text.toString())) {
                    st1 = "0"
                }
                if(I.text.toString().equals(""))
                {
                    st2="0"
                }
                if(M.text.toString().equals(""))
                {
                    st3="0"

                }
                calcuate(st1!!,st2!!,st3!!)

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0.toString().equals(""))
                {

                    seekbar1.setProgress(0)
                }
                else {
                    val enteredProgress = Integer.valueOf(p0.toString())
                    seekbar1.setProgress(enteredProgress)
                }
            }

        })
        I.addTextChangedListener(object : TextWatcher {
            var st1:String?=null
            var st2:String?=null
            var st3:String?=null
            override fun afterTextChanged(p0: Editable?) {
                st1 = P.text.toString()
                st2 = I.text.toString()
                st3 = M.text.toString()

                if (TextUtils.isEmpty(P.text.toString())) {
                    st1 = "0"
                }
                if(I.text.toString().equals(""))
                {
                    st2="0"
                }
                if(M.text.toString().equals(""))
                {
                    st3="0"
                }

                calcuate(st1!!,st2!!,st3!!)
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {


            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0.toString().equals(""))
                {

                    seekbar2.setProgress(0)
                }
                else {
                    val enteredProgress = Integer.valueOf(p0.toString())
                    seekbar2.setProgress(enteredProgress)
                }
            }
        })
        M.addTextChangedListener(object : TextWatcher {
            var st1:String?=null
            var st2:String?=null
            var st3:String?=null
            override fun afterTextChanged(p0: Editable?) {
                st1 = P.text.toString()
                st2 = I.text.toString()
                st3 = M.text.toString()

                if (TextUtils.isEmpty(P.text.toString())) {
                    st1 = "0"
                }
                if(I.text.toString().equals(""))
                {
                    st2="0"
                }
                if(M.text.toString().equals(""))
                {
                    st3="0"
                }
                calcuate(st1!!,st2!!,st3!!)
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0.toString().equals(""))
                {
                    seekbar3.setProgress(0)
                }
                else {
                    val enteredProgress = Integer.valueOf(p0.toString())
                    seekbar3.setProgress(enteredProgress)
                }
            }
        }
        )

        seekbar1.setOnSeekBarChangeListener(object:SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                val enteredvalue =p1.toString()
                P.setText(enteredvalue)
                P.setSelection(P.getText().length);

            }

            override fun onStartTrackingTouch(p0: SeekBar?) {

            }

            override fun onStopTrackingTouch(p0: SeekBar?) {

            }

        })
        seekbar2.setOnSeekBarChangeListener(object:SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                val enteredvalue =p1.toString()
                I.setText(enteredvalue)
                I.setSelection(I.getText().length);
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {

            }

            override fun onStopTrackingTouch(p0: SeekBar?) {

            }

        })
        seekbar3.setOnSeekBarChangeListener(object:SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                val enteredvalue =p1.toString()
                M.setText(enteredvalue)
                M.setSelection(M.getText().length);
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {

            }

            override fun onStopTrackingTouch(p0: SeekBar?) {

            }

        })


        cal_back_arrow.setOnClickListener { activity!!.finish() }


        return v
    }
    fun calcuate(st1: String, st2: String, st3: String)
    {
        val p = java.lang.Float.parseFloat(st1)
        val i = java.lang.Float.parseFloat(st2)
        val m = java.lang.Float.parseFloat(st3)
        val Principal = calPric(p)
        val Rate = calInt(i)
        val Months = calMonth(m)
        val Dvdnt = calDvdnt(Rate,Months)
        val FD = calFinalDvdnt(Principal, Rate, Dvdnt)
        val D = calDivider(Dvdnt)
        val emi = calEmi(FD, D)
        val TA = calTa(emi, Months)
        val ti = calTotalInt(TA, Principal)
        val repay=TA
        repayment.setText(repay.toString())
        result.setText((emi).toString())
        TI.setText((ti).toString())
        if(emi.isNaN()||emi.isInfinite())
        { result.setText("-")}
        if(ti.isNaN()||ti.isInfinite())
        { TI.setText("-")}
    }

    fun calPric(p: Float): Float {
        return (p).toFloat()
    }

    fun calInt(i: Float): Float {
        return (i / 12f / 100f).toFloat()
    }

    fun calMonth(y: Float): Float {
        return (y).toFloat()
    }

    fun calDvdnt(Rate: Float, Months: Float): Float {
        return (Math.pow((1 + Rate).toDouble(), Months.toDouble())).toFloat()
    }

    fun calFinalDvdnt(Principal: Float, Rate: Float, Dvdnt: Float): Float {
        return (Principal * Rate * Dvdnt).toFloat()
    }

    fun calDivider(Dvdnt: Float): Float {
        return (Dvdnt - 1).toFloat()
    }

    fun calEmi(FD: Float, D: Float): Float {
        return (FD / D).toFloat()
    }

    fun calTa(emi: Float, Months: Float): Float {
        return (emi * Months).toFloat()
    }

    fun calTotalInt(TA: Float, Principal: Float): Float {
        return (TA - Principal).toFloat()
    }


}




