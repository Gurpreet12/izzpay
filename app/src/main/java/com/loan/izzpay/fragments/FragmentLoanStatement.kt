package com.loan.izzpay.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.loan.izzpay.R
import com.loan.izzpay.adapters.RecyclerLoanStatementAdapter
import com.loan.izzpay.dataModels.loan_statement_pojo
import kotlinx.android.synthetic.main.fragment_notification_window.*

class FragmentLoanStatement : Fragment() {

    private val values = arrayListOf<loan_statement_pojo>(
            loan_statement_pojo(30000,"12-1-1997", "ongoing","3 Months"),
            loan_statement_pojo(150000,"2-10-2003", "ongoing","4 Months"),
            loan_statement_pojo(80000,"22-5-2003", "paid","4 Months"),
            loan_statement_pojo(660000,"11-11-2023", "paid","9 Months"),
            loan_statement_pojo(107000,"8-2-2013", "ongoing","6 Months"),
            loan_statement_pojo(500000,"1-9-2003", "ongoing","3 Months"),
            loan_statement_pojo(440000,"9-5-2022", "paid","6 Months"),
            loan_statement_pojo(10000,"23-4-2003", "ongoing","3 Months")
    )

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var recyclerView: RecyclerView
    private lateinit var sign_back_arrow: ImageButton

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v: View = inflater.inflate(R.layout.fragment_loan_statement, container, false)

        recyclerView=v.findViewById(R.id.statement_recycler)
        sign_back_arrow=v.findViewById(R.id.sign_back_arrow)

        linearLayoutManager = LinearLayoutManager(activity)
        recyclerView.layoutManager=linearLayoutManager
        val adapter= RecyclerLoanStatementAdapter(values)
        recyclerView.adapter=adapter


        sign_back_arrow.setOnClickListener {
            activity!!.finish() }

        return v
    }


}
