package com.loan.izzpay.fragments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.AppCompatSpinner

import com.loan.izzpay.R
import com.loan.izzpay.activities.ActivityBaseForFragment


class FragmentLoanForm : Fragment() {

    private lateinit var amount: EditText
    private lateinit var tenure: EditText
    private lateinit var submit_btn: Button
    private lateinit var tenure_seekbar: SeekBar
    private lateinit var loan_spinner: AppCompatSpinner

    private lateinit var cash_backimage:ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_loan_form, container, false)

        amount = v.findViewById(R.id.loanf_edittetext1)
        loan_spinner = v.findViewById(R.id.loanf_spinner1)
        tenure = v.findViewById(R.id.loanf_tenure)
        submit_btn = v.findViewById(R.id.loanf_btn1)
        tenure_seekbar = v.findViewById(R.id.loanf_seekbar1)
        submit_btn.setOnClickListener{
            val i = Intent(activity, ActivityBaseForFragment::class.java)
            i.putExtra("checkPage", "credit_score")
            startActivity(i)
        }
        val stringArr: Array<String> = resources.getStringArray(R.array.loans_types);
        val dataAdapter = ArrayAdapter<String>(activity!!, R.layout.simple_spinner_item, stringArr)
        dataAdapter.setDropDownViewResource(R.layout.simple_spinner_drop_down)
        loan_spinner.adapter = dataAdapter


        loan_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        tenure.addTextChangedListener(object : TextWatcher {

            var st3: String? = null
            override fun afterTextChanged(p0: Editable?) {
                st3 = tenure.text.toString()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0.toString().equals("")) {
                    tenure_seekbar.setProgress(0)
                } else {
                    val enteredProgress = Integer.valueOf(p0.toString())
                    tenure_seekbar.setProgress(enteredProgress)
                }
            }
        }
        )
        tenure_seekbar.setOnSeekBarChangeListener(object:SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                val enteredvalue =p1.toString()
                tenure.setText(enteredvalue)
                tenure.setSelection(tenure.getText().length);
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {

            }

            override fun onStopTrackingTouch(p0: SeekBar?) {

            }

        })

        cash_backimage = v.findViewById(R.id.forml_image1)

        cash_backimage.setOnClickListener {
            activity!!.finish()
        }

        return v
    }
}
