package com.loan.izzpay.fragments

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.DefaultValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.utils.MPPointF

import com.loan.izzpay.R


class FragmentCreditScore : Fragment(), OnChartValueSelectedListener {

    protected val parties = arrayOf("score Board", "Your Credit Score")
    private var chart: PieChart? = null
    private lateinit var tfRegular: Typeface
    private lateinit var tfLight: Typeface
    private lateinit var text1: TextView
    private lateinit var cal_back_arrow: ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_credit_score, container, false)

        tfRegular = Typeface.createFromAsset(activity!!.assets, "OpenSans-Regular.ttf")
        tfLight = Typeface.createFromAsset(activity!!.assets, "OpenSans-Light.ttf")

        text1 = v.findViewById(R.id.credit_text2)
        cal_back_arrow = v.findViewById(R.id.cal_back_arrow)
        text1.setText("You are not eligible for loan of \u20B9 80,000")
        chart = v.findViewById(R.id.chart1)
        chart!!.getDescription().isEnabled = false
        chart!!.setExtraOffsets(5f, 10f, 5f, 5f)
        chart!!.setDragDecelerationFrictionCoef(0.95f)
        //chart!!.setCenterTextTypeface(tfLight)
        chart!!.setCenterText(generateCenterSpannableText())
        chart!!.setDrawHoleEnabled(true)
        chart!!.setHoleColor(Color.WHITE)
        chart!!.setTransparentCircleColor(Color.WHITE)
        chart!!.setTransparentCircleAlpha(110)
        chart!!.setHoleRadius(58f)
        chart!!.setTransparentCircleRadius(61f)
        chart!!.setDrawCenterText(true)
        chart!!.setRotationAngle(90f)
        chart!!.setRotationEnabled(true)
        chart!!.setHighlightPerTapEnabled(true)
        chart!!.setOnChartValueSelectedListener(this)
        chart!!.animateY(1400, Easing.EaseInOutQuad)
        chart!!.legend.isEnabled = false


        chart!!.setEntryLabelColor(Color.BLACK)
        chart!!.setEntryLabelTypeface(tfRegular)
        chart!!.setEntryLabelTextSize(12f)

        setData()

        cal_back_arrow.setOnClickListener { activity!!.finish() }

        return v
    }

    private fun generateCenterSpannableText(): SpannableString {

        val s = SpannableString("Your Score is\nAverage")

        s.setSpan(RelativeSizeSpan(1.4f), 0, s.length, 0)
        s.setSpan(StyleSpan(Typeface.NORMAL), 13, s.length, 0)
//        s.setSpan(ForegroundColorSpan(Color.GRAY), 13, s.length - 14, 0)
//        s.setSpan(RelativeSizeSpan(1.8f), 13, s.length - 14, 0)
//        s.setSpan(StyleSpan(Typeface.ITALIC), s.length - 13, s.length, 0)
//        s.setSpan(ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length - 13, s.length, 0)
        return s
    }

    private fun setData() {
        val entries = ArrayList<PieEntry>()

        entries.add(PieEntry(354.5F, "Your Score"))
        entries.add(PieEntry(900F, "Total Score"))

        val dataSet = PieDataSet(entries, "Election Results")

        dataSet.setDrawIcons(false)

        dataSet.sliceSpace = 3f
        dataSet.iconsOffset = MPPointF(0f, 40f)
        dataSet.selectionShift = 5f

        val colors = ArrayList<Int>()

        for (c in ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c)

        for (c in ColorTemplate.JOYFUL_COLORS)
            colors.add(c)

        for (c in ColorTemplate.COLORFUL_COLORS)
            colors.add(c)

        for (c in ColorTemplate.LIBERTY_COLORS)
            colors.add(c)

        for (c in ColorTemplate.PASTEL_COLORS)
            colors.add(c)

        colors.add(ColorTemplate.getHoloBlue())

        dataSet.colors = colors
        //dataSet.setSelectionShift(0f);

        val data = PieData(dataSet)
        data.setValueFormatter(DefaultValueFormatter(0))
        data.setValueTextSize(11f)
        data.setValueTextColor(Color.BLACK)
        data.setValueTypeface(tfLight)
        chart!!.setData(data)

        // undo all highlights
        chart!!.highlightValues(null)

        chart!!.invalidate()
    }

    override fun onNothingSelected() {

        Log.i("PieChart", "nothing selected")
    }

    override fun onValueSelected(e: Entry?, h: Highlight?) {
        if (e == null)
            return
        Log.i("VAL SELECTED",
                "Value: " + e.y + ", index: " + h!!.getX()
                        + ", DataSet index: " + h.getDataSetIndex())
    }
}
