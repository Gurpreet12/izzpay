package com.loan.izzpay.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.loan.izzpay.R
import com.loan.izzpay.adapters.RecyclerFaqQuestions
import com.loan.izzpay.dataModels.FaqQuestions

class FragmentFaq : Fragment() {

    private val questions = arrayListOf<FaqQuestions>(
            FaqQuestions("What is the Loan Criteria? ","1. 30 Sec Sign Up using Google Facebook\n" +
                    "2. 2 Min Enter basic information \$ Check Eligibility\n" +
                    "3. 5 MinUpload docoments \$ Get your Profile verified\n" +
                    "4. 2 Min Provide Bank account details\n" +
                    "5. 30 Sec Avail Loan in your Bank Account"),
            FaqQuestions("Eligibility for Loan","1. Indian Citizen\n" +
                    "2. Age above 21 years\n" +
                    "3. KYC Address proof\n" +
                    "4. PAN ID\n" +
                    "5. Salary above ₹ 10,000\n" +
                    "6. Valid Bank Statement"),
            FaqQuestions("What are the Features?","1. Instant Loans\n" +
                    "2. Quick Approvals & Disbursals\n" +
                    "3. No Hidden Charges\n" +
                    "4. Affordable EMI plans\n" +
                    "5. No Cost EMI\n" +
                    "6. Zero Credit History")
    )
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var recyclerView: RecyclerView

    private lateinit var sign_back_arrow:ImageButton

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v: View = inflater.inflate(R.layout.fragment_faq, container, false)

        recyclerView=v.findViewById(R.id.statement_recycler)
        sign_back_arrow=v.findViewById(R.id.sign_back_arrow)
        linearLayoutManager = LinearLayoutManager(activity)
        recyclerView.layoutManager= linearLayoutManager as RecyclerView.LayoutManager?
        val adapter= RecyclerFaqQuestions(questions)
        recyclerView.adapter=adapter


        sign_back_arrow.setOnClickListener {
            activity!!.finish()
        }



        return v
    }


}
