package com.loan.izzpay.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.loan.izzpay.R

class FragmentMarriageLoan : Fragment() {
    private lateinit var calculate_btn: Button
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v: View =inflater.inflate(R.layout.fragment_marriage_loan, container, false)
        calculate_btn=v.findViewById(R.id.marriage_cal_btn)
        return v

}
}
