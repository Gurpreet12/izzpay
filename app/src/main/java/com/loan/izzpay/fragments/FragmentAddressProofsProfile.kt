package com.loan.izzpay.fragments

import android.app.ProgressDialog
import android.content.*
import android.os.Bundle
import android.widget.*
import androidx.fragment.app.Fragment
import com.loan.izzpay.R

import android.view.*
import com.loan.izzpay.RetrofitConnections.ApiClient
import com.loan.izzpay.models.DataModelLogin
import com.loan.izzpay.models.ModelGetAllAddress
import com.loan.izzpay.utils.Constant
import retrofit2.Call
import retrofit2.Callback
import java.lang.Exception


class FragmentAddressProofsProfile : Fragment() {


    private lateinit var residential_type_edt: EditText
    private lateinit var house_no_edt: EditText
    private lateinit var street_name_edt: EditText
    private lateinit var landmark_edt: EditText
    private lateinit var city_edt: EditText
    private lateinit var state_edt: EditText
    private lateinit var proceedProfessionalBtn: RelativeLayout

    var v: View? = null

    lateinit var progerssProgressDialog: ProgressDialog
    var dataList: ModelGetAllAddress? = null
    var dataModelLogin: DataModelLogin? = null

    lateinit var sharedPreferences: SharedPreferences
    private val sharedPrefDB = "izzpay_db"

    lateinit var sign_back_arrow: ImageButton




    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        v = inflater.inflate(R.layout.fragment_address_proofs_profile, container, false)

        getActivity()!!.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)

        sharedPreferences = activity!!.getSharedPreferences(sharedPrefDB, Context.MODE_PRIVATE)

        init()

        mListeners()

        return v
    }

    private fun init() {

        sign_back_arrow = v!!.findViewById(R.id.sign_back_arrow)
        residential_type_edt = v!!.findViewById(R.id.residential_type_edt)
        house_no_edt = v!!.findViewById(R.id.house_no_edt)
        street_name_edt = v!!.findViewById(R.id.street_name_edt)
        landmark_edt = v!!.findViewById(R.id.landmark_edt)
        city_edt = v!!.findViewById(R.id.city_edt)
        state_edt = v!!.findViewById(R.id.state_edt)
        proceedProfessionalBtn = v!!.findViewById(R.id.proceedProfessionalBtn)

        progerssProgressDialog = ProgressDialog(activity)
        progerssProgressDialog.setTitle("Please wait...")
        progerssProgressDialog.setCancelable(false)

        getAllAddresses()

    }


    private fun mListeners() {

        residential_type_edt.setOnClickListener {
            val popup = PopupMenu(requireActivity(), residential_type_edt)
            popup.getMenuInflater()
                    .inflate(R.menu.resdidential, popup.getMenu())
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    residential_type_edt.setText(item.getTitle())
                    return true
                }
            })
            popup.show()
        }


        proceedProfessionalBtn.setOnClickListener {




            if (!residential_type_edt.text.toString().isEmpty())
            {
                residential_type_edt.setError(null)
                if (!house_no_edt.text.toString().isEmpty())
                {
                    house_no_edt.setError(null)
                    if (!street_name_edt.text.toString().isEmpty())
                    {
                        street_name_edt.setError(null)
                        if (!landmark_edt.text.toString().isEmpty())
                        {
                            landmark_edt.setError(null)
                            if (!state_edt.text.toString().isEmpty())
                            {
                                state_edt.setError(null)
                                if (!city_edt.text.toString().isEmpty())
                                {
                                    city_edt.setError(null)

                                    mUpdateAddressUserProfile(residential_type_edt.text.toString(),house_no_edt.text.toString(),street_name_edt.text.toString(),landmark_edt.text.toString(),state_edt.text.toString(),city_edt.text.toString())
                                }
                                else{
                                    city_edt.setError("enter your state")
                                }
                            }
                            else{
                                state_edt.setError("enter your city")
                            }
                        }
                        else{
                            landmark_edt.setError("enter your landmark")
                        }
                    }
                    else{
                        street_name_edt.setError("enter your street name")
                    }
                }
                else{
                    house_no_edt.setError("enter your house no or building no")
                }
            }
            else{
                residential_type_edt.setError("enter your residential type")
            }
        }


        city_edt.setOnClickListener {
            val popup = PopupMenu(requireActivity(), city_edt)
            popup.getMenuInflater()
                    .inflate(R.menu.gender, popup.getMenu())
            popup.setOnMenuItemClickListener(object: PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem):Boolean {


                    return true
                }
            })
            popup.show()
        }


        state_edt.setOnClickListener {
            val popup = PopupMenu(requireActivity(), state_edt)
            popup.getMenuInflater().inflate(R.menu.states, popup.getMenu())
            popup.setOnMenuItemClickListener(object: PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem):Boolean {
                    state_edt.setText(item.getTitle())
                    return true
                }
            })
            popup.show()
        }

        sign_back_arrow.setOnClickListener {
            val fm = activity!!.getSupportFragmentManager();
            fm.popBackStack();

        }
    }


    private fun mUpdateAddressUserProfile(residentType: String, address1: String, address2: String, country: String, state: String, city: String) {

        if (Constant.hasNetworkAvailable(activity!!)) {

            val Access_Token = sharedPreferences.getString("Access_Token", "")


//            val paramObject = JSONObject()
//
//            val abc = HashMap<String,String>()
//            abc.put("fatherName", father_name)
//            abc.put("motherName3", mother_name)
//            abc.put("highestEducation", highest_education)
//            abc.put("martialStatus", marital_status)

            progerssProgressDialog.show()

            try {
                val call: Call<DataModelLogin> = ApiClient.getClient.mUpdateAddressUserProfile("Bearer "+Access_Token,residentType, address1, address2,country,state,city,"789454")
                call.enqueue(object : Callback<DataModelLogin> {
                    override fun onFailure(call: Call<DataModelLogin>, t: Throwable) {
                        progerssProgressDialog.dismiss()
                        Toast.makeText(activity, t.message, Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<DataModelLogin>, response: retrofit2.Response<DataModelLogin>) {

                        progerssProgressDialog.dismiss()

                        try {
                            if (response.code() == 200)
                            {
                                dataModelLogin = response.body()

                                Toast.makeText(activity, dataModelLogin!!.message, Toast.LENGTH_SHORT).show()

                            }
                            else if (response.code() == 409)
                            {
                                Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show()
                            }
                            else if (response.code() == 404)
                            {
                                Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show()
                            }
                        } catch (exception: Exception) {
                            Toast.makeText(activity, exception.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                })

            } catch (ex: Exception) {
                progerssProgressDialog.hide()
                Toast.makeText(activity, ex.message.toString(), Toast.LENGTH_SHORT).show()
            }

        } else {
            Toast.makeText(activity, "No network available!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun getAllAddresses() {

        if (Constant.hasNetworkAvailable(activity!!)) {

            val Access_Token = sharedPreferences.getString("Access_Token", "")

            progerssProgressDialog.show()

            try {
                val call: Call<ModelGetAllAddress> = ApiClient.getClient.addressAll("Bearer "+Access_Token)
                call.enqueue(object : Callback<ModelGetAllAddress> {
                    override fun onFailure(call: Call<ModelGetAllAddress>, t: Throwable) {
                        progerssProgressDialog.dismiss()
                        Toast.makeText(activity, t.message, Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<ModelGetAllAddress>, response: retrofit2.Response<ModelGetAllAddress>) {

                        progerssProgressDialog.dismiss()

                        try {
                            if (response.code() ==200)
                            {
                                dataList = response.body()
                                if (dataList == null)
                                {
                                    Toast.makeText(activity!!,"Data is not found",Toast.LENGTH_SHORT).show();
                                }
                                else{
                                    val dataSet = response.body()

                                    val addressType = dataSet!!.getData()!!.get(0).addressType
                                    val houseNumber = dataSet!!.getData()!!.get(0).addressLine1
                                    val StreetNumber = dataSet!!.getData()!!.get(0).addressLine2
                                    val landMark = dataSet!!.getData()!!.get(0).country
                                    val State = dataSet!!.getData()!!.get(0).state
                                    val city = dataSet!!.getData()!!.get(0).city

                                    residential_type_edt.setText(addressType)
                                    house_no_edt.setText(houseNumber)
                                    street_name_edt.setText(StreetNumber)
                                    landmark_edt.setText(landMark)
                                    city_edt.setText(city)
                                    state_edt.setText(State)

                                }
                            }
                            else if (response.code() == 400)
                            {
                                Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show()
                            }
                            else if (response.code() == 404)
                            {
                                Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show()
                            }
                            else if (response.code() == 500)
                            {
                                Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show()
                            }

                        }catch (exception:Exception)
                        {
                            Toast.makeText(activity, exception.message, Toast.LENGTH_SHORT).show()
                        }


                    }

                })

            } catch (ex: java.lang.Exception) {
                Toast.makeText(activity, ex.message.toString(), Toast.LENGTH_SHORT).show()
            }

        } else {
            Toast.makeText(activity, "No network available!", Toast.LENGTH_SHORT).show()
        }
    }
}
