package com.loan.izzpay.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import com.loan.izzpay.R

class FragmentSmartRepay : Fragment() {

    lateinit var sign_back_arrow:ImageButton

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v: View = inflater.inflate(R.layout.fragment_smart_repay, container, false)

        sign_back_arrow = v.findViewById(R.id.sign_back_arrow)

        sign_back_arrow.setOnClickListener {
            activity!!.finish()
        }

        return v
    }


}
