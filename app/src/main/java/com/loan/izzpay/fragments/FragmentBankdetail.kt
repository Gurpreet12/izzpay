package com.loan.izzpay.fragments

import android.Manifest
import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import android.provider.Settings
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import com.facebook.FacebookSdk.getApplicationContext
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.multi.MultiplePermissionsListener

import com.loan.izzpay.R
import com.loan.izzpay.activities.UploadSelfProfileImage.ImagePickerActivity
import java.io.File


class FragmentBankdetail : Fragment() {


    private var buttonUpload: Button? = null
    private lateinit var constraintlay1: ConstraintLayout
    private lateinit var constraintlay2:ConstraintLayout
    private lateinit var constraintlay3:ConstraintLayout
    private lateinit var View1:View
    private lateinit var View2:View
    private lateinit var View3:View
    private var upload_pdf1: ImageView? = null
    private var upload_pdf2: ImageView? = null
    private var upload_pdf3: ImageView? = null
    private var upload_Image1: ImageView? = null
    private var upload_Image2: ImageView? = null
    private var upload_Image3: ImageView? = null
    private var myView: View? = null
    private var myTextView: TextView? = null
    private var myvalue: ImageView? = null
    private var displayName: String? = null

    private var TextView1: TextView? = null
    private var TextView2: TextView? = null
    private var TextView3: TextView? = null
    private val PICK_PDF_REQUEST = 1
    private val STORAGE_PERMISSION_CODE = 123
    lateinit var mcontext: FragmentBankdetail
    private val REQUEST_IMAGE = 1
    private var finalFile: File? = null
    private val imgResource1: Int  = R.drawable.ic_file_upload
    private val imgResource: Int  = R.drawable.ic_pdf_file


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_bankdetail, container, false)

        requestStoragePermission();

        mcontext = FragmentBankdetail()
        buttonUpload = v.findViewById(R.id.bank_upload_btn)
        constraintlay1=v.findViewById(R.id.bank_constraint1)
        constraintlay2=v.findViewById(R.id.bank_constraint2)
        constraintlay3=v.findViewById(R.id.bank_constraint3)
        View1=v.findViewById(R.id.bank_view1)
        View2=v.findViewById(R.id.bank_view2)
        View3=v.findViewById(R.id.bank_view3)
        upload_pdf1 = v.findViewById(R.id.bank_pdf_upload1)
        upload_pdf2 = v.findViewById(R.id.bank_pdf_upload2)
        upload_pdf3 = v.findViewById(R.id.bank_pdf_upload3)
        upload_Image1 = v.findViewById(R.id.bank_capture_img1)
        upload_Image2 = v.findViewById(R.id.bank_capture_img2)
        upload_Image3 = v.findViewById(R.id.bank_capture_img3)
        TextView1 = v.findViewById(R.id.bank_pdf_text1)
        TextView2 = v.findViewById(R.id.bank_pdf_text2)
        TextView3 = v.findViewById(R.id.bank_pdf_text3)
        upload_pdf1!!.setBackgroundResource(imgResource1)
        upload_pdf2!!.setBackgroundResource(imgResource1)
        upload_pdf3!!.setBackgroundResource(imgResource1)
        constraintlay1.setOnClickListener {
            showFileChooser(upload_pdf1!!,TextView1!!)
        }
        constraintlay2.setOnClickListener {
            showFileChooser(upload_pdf2!!,TextView2!!)
        }
        constraintlay3.setOnClickListener {
            showFileChooser(upload_pdf3!!,TextView3!!)
        }
        buttonUpload!!.setOnClickListener {
        }
        upload_Image1!!.setOnClickListener {
            CameraOption(View1)
        }
        upload_Image2!!.setOnClickListener {
            CameraOption(View2)
        }
        upload_Image3!!.setOnClickListener {
            CameraOption(View3)
        }
        return v
    }


    private fun showFileChooser(imageholder:ImageView,text:TextView) {
        myvalue = imageholder
        myTextView=text
        val intent = Intent()
        intent.type = "application/pdf"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Pdf"), PICK_PDF_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICK_PDF_REQUEST && resultCode == RESULT_OK && data != null && data.data != null) {
            val fileUri = data.data
            val uriString = fileUri.toString()
            val myFile = File(uriString)
            val path = myFile.getAbsolutePath()

            if (uriString.startsWith("content://")) {
                var cursor: Cursor? = null
                try {
                    cursor = getActivity()!!.getContentResolver().query(fileUri!!, null, null, null, null)
                    if (cursor != null && cursor.moveToFirst()) {
                        displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                    }
                } finally {
                    cursor!!.close()
                }
            } else if (uriString.startsWith("file://")) {
                displayName = myFile.getName()
            }
            showPdfName(myTextView!!)
            changeImage(myvalue!!)


        }
        else if(requestCode == REQUEST_IMAGE && resultCode == RESULT_OK ) {
            val myUri = data!!.getParcelableExtra<Uri>("path")

            finalFile = File(myUri.getPath()!!)
            if(!finalFile.toString().isEmpty())
            {
                onVisbilityofView(myView!!)
            }
            Log.e("as", finalFile.toString())
            //               self_image_view.setImageURI(myUri)
        }
    }
    private fun requestStoragePermission() {
        if (ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return

        if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), STORAGE_PERMISSION_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {
            //If permission is granted
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                Toast.makeText(activity!!, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show()
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(activity!!, "Oops you just denied the permission", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun CameraOption(selectView: View) {
        val mydata:View=selectView
        Dexter.withActivity(activity)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        if (report.areAllPermissionsGranted()) {
                            launchCameraIntent(mydata)
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<com.karumi.dexter.listener.PermissionRequest>?, token: PermissionToken?) {
                        token!!.continuePermissionRequest()
                    }
                }).check()
    }


    private fun showSettingsDialog() {

        val builder = AlertDialog.Builder(activity!!)
        builder.setTitle(getString(R.string.dialog_permission_title))
        builder.setMessage(getString(R.string.dialog_permission_message))
        builder.setPositiveButton(getString(R.string.go_to_settings)) { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton(getString(android.R.string.cancel)) { dialog, which -> dialog.cancel() }
        builder.show()

    }

    // navigating user to app settings
    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", getApplicationContext().getPackageName(), null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    private fun launchCameraIntent(value:View) {
        myView=value
        val intent = Intent(getApplicationContext(), ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000)

        startActivityForResult(intent, REQUEST_IMAGE)
    }
    private fun onVisbilityofView(view:View){
        view.visibility=View.VISIBLE
    }

    private fun changeImage(imageView: ImageView){
        imageView.setBackgroundResource(imgResource)

    }
    private fun showPdfName(textView:TextView){
        textView.setText(displayName)

    }


}
