package com.loan.izzpay.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

import com.loan.izzpay.R


class FragmentContactUs : Fragment() {

    lateinit var cal_back_arrow:ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_contact_us, container, false)

        cal_back_arrow = view.findViewById(R.id.cal_back_arrow)

        cal_back_arrow.setOnClickListener {
            activity!!.finish()
        }

        return view
    }


}
