package com.loan.izzpay.fragments

import android.app.ProgressDialog
import android.content.*
import android.os.Bundle
import android.widget.*
import androidx.fragment.app.Fragment


import android.view.*
import com.loan.izzpay.R
import com.loan.izzpay.RetrofitConnections.ApiClient
import com.loan.izzpay.models.ProfileModel
import com.loan.izzpay.utils.Constant
import retrofit2.Call
import retrofit2.Callback
import java.lang.Exception





class FragmentProfessionalProfile : Fragment() {


    private lateinit var profession_edt: EditText
    private lateinit var company_edt: EditText
    private lateinit var designation_edt: EditText
    private lateinit var office_area_edt: EditText
    private lateinit var office_city_edt: EditText
    private lateinit var office_pincode_tv: EditText
    private lateinit var yearExpEdt: EditText
    private lateinit var monthExpEdt: EditText
    private lateinit var montly_salary_edt: EditText
    private lateinit var proceedProfessionalBtn: RelativeLayout



    var v: View? = null

    lateinit var progerssProgressDialog: ProgressDialog
    var dataList: ProfileModel? = null

    lateinit var sharedPreferences: SharedPreferences
    private val sharedPrefDB = "izzpay_db"

    lateinit var sign_back_arrow: ImageButton




    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        v = inflater.inflate(R.layout.fragment_prefessional_profile, container, false)

        getActivity()!!.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)

        sharedPreferences = activity!!.getSharedPreferences(sharedPrefDB, Context.MODE_PRIVATE)

        init()

        mListeners()

        return v
    }

    override fun onResume() {

        super.onResume()

        val login = sharedPreferences.getString("userID", "")


    }

    private fun init() {


        sign_back_arrow = v!!.findViewById(R.id.sign_back_arrow)

        profession_edt = v!!.findViewById(R.id.profession_edt)
        company_edt = v!!.findViewById(R.id.company_edt)
        designation_edt = v!!.findViewById(R.id.designation_edt)
        office_area_edt = v!!.findViewById(R.id.office_area_edt)
        office_city_edt = v!!.findViewById(R.id.office_city_edt)
        office_pincode_tv = v!!.findViewById(R.id.office_pincode_tv)
        yearExpEdt = v!!.findViewById(R.id.yearExpEdt)
        monthExpEdt = v!!.findViewById(R.id.monthExpEdt)
        montly_salary_edt = v!!.findViewById(R.id.montly_salary_edt)
        proceedProfessionalBtn = v!!.findViewById(R.id.proceedProfessionalBtn)

        progerssProgressDialog = ProgressDialog(activity)
        progerssProgressDialog.setTitle("Please wait...")
        progerssProgressDialog.setCancelable(false)



        val strProfession = arguments?.getString("profession")
        val strCompanyName = arguments?.getString("companyName")
        val strDesignation = arguments?.getString("designation")
        val strTotalExperience = arguments?.getString("totalExperience")
        val strMontlyIncome = arguments?.getString("montlyIncome")


        val officeArea = arguments?.getString("officeArea")
        val officeCity = arguments?.getString("officeCity")
        val officePincode = arguments?.getString("officePincode")
        val monthOfExperienced = arguments?.getString("monthOfExperienced")

        office_area_edt.setText(officeArea)
        office_city_edt.setText(officeCity)
        office_pincode_tv.setText(officePincode)
        monthExpEdt.setText(monthOfExperienced)



        profession_edt.setText(strProfession)
        company_edt.setText(strCompanyName)
        designation_edt.setText(strDesignation)



        yearExpEdt.setText(strTotalExperience)
        montly_salary_edt.setText(strMontlyIncome)

    }


    private fun mListeners() {


        profession_edt.setOnClickListener {
            val popup = PopupMenu(requireActivity(), profession_edt)
            popup.getMenuInflater()
                    .inflate(R.menu.profession, popup.getMenu())
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    profession_edt.setText(item.getTitle())
                    return true
                }
            })
            popup.show()
        }
        yearExpEdt.setOnClickListener {
            val popup = PopupMenu(requireActivity(), yearExpEdt)
            popup.getMenuInflater()
                    .inflate(R.menu.years_experienced, popup.getMenu())
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    yearExpEdt.setText(item.getTitle())
                    return true
                }
            })
            popup.show()
        }
        monthExpEdt.setOnClickListener {
            val popup = PopupMenu(requireActivity(), monthExpEdt)
            popup.getMenuInflater()
                    .inflate(R.menu.month_experienced, popup.getMenu())
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    monthExpEdt.setText(item.getTitle())
                    return true
                }
            })
            popup.show()
        }


        proceedProfessionalBtn.setOnClickListener {

            if (!profession_edt.text.toString().isEmpty())
            {
                profession_edt.setError(null)
                if (!company_edt.text.toString().isEmpty())
                {
                    company_edt.setError(null)
                    if (!designation_edt.text.toString().isEmpty())
                    {
                        designation_edt.setError(null)
                        if (!office_area_edt.text.toString().isEmpty())
                        {
                            office_area_edt.setError(null)
                            if (!office_city_edt.text.toString().isEmpty())
                            {
                                office_city_edt.setError(null)
                                if (!office_pincode_tv.text.toString().isEmpty())
                                {
                                    office_pincode_tv.setError(null)

                                    if (office_pincode_tv.text.toString().length == 6) {
                                        office_pincode_tv.setError(null)

                                        if (!yearExpEdt.text.toString().isEmpty())
                                        {
                                            yearExpEdt.setError(null)

                                            if (!monthExpEdt.text.toString().isEmpty())
                                            {
                                                monthExpEdt.setError(null)
                                                if (!montly_salary_edt.text.toString().isEmpty())
                                                {
                                                    montly_salary_edt.setError(null)

                                                    mUpdateProfessionUserProfile(profession_edt.text.toString(),company_edt.text.toString(),designation_edt.text.toString(),office_area_edt.text.toString(),
                                                            office_city_edt.text.toString(),office_pincode_tv.text.toString(),yearExpEdt.text.toString(),monthExpEdt.text.toString(),montly_salary_edt.text.toString())

                                                }
                                                else{
                                                    montly_salary_edt.setError("enter your monthly salary")
                                                }
                                            }
                                            else{
                                                monthExpEdt.setError("enter your experience in month")
                                            }
                                        }
                                        else{
                                            yearExpEdt.setError("enter your experience in year")
                                        }
                                    }
                                    else{
                                        office_pincode_tv.setError("enter 6 digits pincode")
                                    }



                                }
                                else{
                                    office_pincode_tv.setError("enter your office pincode")
                                }
                            }
                            else{
                                office_city_edt.setError("enter your city")
                            }
                        }
                        else{
                            office_area_edt.setError("enter your office area")
                        }
                    }
                    else{
                        designation_edt.setError("enter your designation")
                    }
                }
                else{
                    company_edt.setError("enter your company name")
                }
            }
            else{
                profession_edt.setError("enter your profession")
            }
        }

        sign_back_arrow.setOnClickListener {
            val fm = activity!!.getSupportFragmentManager();
            fm.popBackStack();

        }

    }


    private fun mUpdateProfessionUserProfile(profession: String, company: String, designation: String, officeArea: String, officeCity: String, officePinCode: String, yearExp: String, monthExp: String, monthlyIncome: String) {

        if (Constant.hasNetworkAvailable(activity!!)) {

            val Access_Token = sharedPreferences.getString("Access_Token", "")


//            val paramObject = JSONObject()
//
//            val abc = HashMap<String,String>()
//            abc.put("fatherName", father_name)
//            abc.put("motherName3", mother_name)
//            abc.put("highestEducation", highest_education)
//            abc.put("martialStatus", marital_status)

            progerssProgressDialog.show()

            try {
                val call: Call<ProfileModel> = ApiClient.getClient.mUpdateProfessionUserProfile("Bearer "+Access_Token,profession, company, designation,officeArea,officeCity,officePinCode,monthExp,yearExp,monthlyIncome)
                call.enqueue(object : Callback<ProfileModel> {
                    override fun onFailure(call: Call<ProfileModel>, t: Throwable) {
                        progerssProgressDialog.dismiss()
                        Toast.makeText(activity, t.message, Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<ProfileModel>, response: retrofit2.Response<ProfileModel>) {

                        progerssProgressDialog.dismiss()

                        try {
                            if (response.code() == 200)
                            {
                                dataList = response.body()

                                Toast.makeText(activity, dataList!!.getMessage().toString(), Toast.LENGTH_SHORT).show()

                            }
                            else if (response.code() == 409)
                            {
                                Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show()
                            }
                            else if (response.code() == 404)
                            {
                                Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show()
                            }
                        } catch (exception: Exception) {
                            Toast.makeText(activity, exception.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                })

            } catch (ex: Exception) {
                progerssProgressDialog.hide()
                Toast.makeText(activity, ex.message.toString(), Toast.LENGTH_SHORT).show()
            }

        } else {
            Toast.makeText(activity, "No network available!", Toast.LENGTH_SHORT).show()
        }
    }



}
