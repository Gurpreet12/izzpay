package com.loan.izzpay.fragments

import android.app.ProgressDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.loan.izzpay.R
import com.loan.izzpay.RetrofitConnections.ApiClient
import com.loan.izzpay.adapters.NotificationAdapter
import com.loan.izzpay.pojo.NotificationModelClass
import com.loan.izzpay.utils.Constant
import retrofit2.Call
import retrofit2.Callback


class FragmentNotificationBell : Fragment() {

    private lateinit var recyclerView_notification: RecyclerView
    private lateinit var mAdapter: NotificationAdapter
    private var stringArrayList = arrayListOf<NotificationModelClass>()
    private lateinit var constraintLayout: ConstraintLayout


    lateinit var progerssProgressDialog: ProgressDialog
    var dataList: NotificationModelClass? = null

    lateinit var sharedPreferences: SharedPreferences
    private val sharedPrefDB = "izzpay_db"
    private var sign_back_arrow:ImageButton ?= null

    private lateinit var linearLayoutManager: LinearLayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v: View = inflater.inflate(R.layout.fragment_notification_window, container, false)


        sharedPreferences = activity!!.getSharedPreferences(sharedPrefDB, Context.MODE_PRIVATE)

        progerssProgressDialog = ProgressDialog(activity)
        progerssProgressDialog.setTitle("Please wait...")
        progerssProgressDialog.setCancelable(false)

        recyclerView_notification = v.findViewById(R.id.recyclerView_notification) as RecyclerView
        sign_back_arrow = v.findViewById(R.id.sign_back_arrow)

        linearLayoutManager = LinearLayoutManager(activity)
        recyclerView_notification.layoutManager = linearLayoutManager


        constraintLayout = v.findViewById(R.id.constraintLay)

        getListOfNotifications()
//        populateRecyclerView()
//        initializeAdapter()
        enableSwipeToDeleteAndUndo()


        sign_back_arrow!!.setOnClickListener {
            activity!!.finish()
        }
        return v
    }



    private fun enableSwipeToDeleteAndUndo() {
//        val swipeToDeleteCallback = object : Activitytesting(activity!!) {
//            override fun onSwiped(@NonNull viewHolder: RecyclerView.ViewHolder, i: Int) {
//                val position = viewHolder.getAdapterPosition()
//                val item = stringArrayList.get(viewHolder.adapterPosition)
//                mAdapter!!.removeItem(position)
//                val snackbar = Snackbar
//                        .make(constraintLayout!!, "Item was removed from the list.", Snackbar.LENGTH_LONG)
//                snackbar.setAction("UNDO", object : View.OnClickListener {
//                    override fun onClick(view: View) {
//                        //   val item = mAdapter.getData().get(position)
//                        mAdapter!!.restoreItem(item, position)
//                        recyclerView_notification!!.scrollToPosition(position)
//                    }
//                })
//                snackbar.setActionTextColor(resources.getColor(R.color.blue_color_1))
//                snackbar.show()
//            }
//        }
//
//
//        val itemTouchhelper = ItemTouchHelper(swipeToDeleteCallback)
//        itemTouchhelper.attachToRecyclerView(recyclerView_notification)
    }

    private lateinit var typesCustomAdapter: NotificationAdapter

    private fun getListOfNotifications() {

        if (Constant.hasNetworkAvailable(activity!!)) {

            val Access_Token = sharedPreferences.getString("Access_Token", "")

            progerssProgressDialog.show()

            try {
                val call: Call<NotificationModelClass> = ApiClient.getClient.notifications("Bearer "+Access_Token)
                call.enqueue(object : Callback<NotificationModelClass> {
                    override fun onFailure(call: Call<NotificationModelClass>, t: Throwable) {
                        progerssProgressDialog.dismiss()
                        Toast.makeText(activity, t.message, Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<NotificationModelClass>, response: retrofit2.Response<NotificationModelClass>) {

                        progerssProgressDialog.dismiss()

                        try {
                            if (response.code() ==200)
                            {
                                dataList = response.body()
                                if (dataList == null)
                                {

                                    Toast.makeText(activity, "No data found", Toast.LENGTH_SHORT).show()
                                }
                                else{
                                    val code = dataList!!.getMessage()
                                    typesCustomAdapter = NotificationAdapter(activity!!,dataList)
                                    recyclerView_notification!!.adapter = typesCustomAdapter
                                }
                            }

                        }catch (exception:Exception)
                        {
                            Toast.makeText(activity, exception.message, Toast.LENGTH_SHORT).show()
                        }


                    }

                })

            } catch (ex: java.lang.Exception) {
                Toast.makeText(activity, ex.message.toString(), Toast.LENGTH_SHORT).show()
            }

        } else {
            Toast.makeText(activity, "No network available!", Toast.LENGTH_SHORT).show()
        }
    }
}