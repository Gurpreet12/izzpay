package com.loan.izzpay.fragments

import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.*
import android.os.Bundle
import android.widget.*
import androidx.fragment.app.Fragment
import com.loan.izzpay.R

import android.view.*
import com.bumptech.glide.Glide
import com.loan.izzpay.RetrofitConnections.ApiClient
import com.loan.izzpay.activities.ActivityBaseForFragment
import com.loan.izzpay.models.ProfileModel
import com.loan.izzpay.utils.Constant
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import java.lang.Exception
import java.util.*







class FragmentPersonalProfile : Fragment() {


    private lateinit var first_name_edt: EditText
    private lateinit var last_name_edt: EditText
    private lateinit var fathers_name_edt: EditText
    private lateinit var mother_name_edt: EditText
    private lateinit var date_of_birth_edt: EditText
    private lateinit var gender_edt: EditText
    private lateinit var marital_status_edt: EditText
    private lateinit var phoneEdt: EditText
    private lateinit var gmail_edt: EditText
    private lateinit var highestEducationEdt: EditText

    private lateinit var proceedRl: RelativeLayout
    private lateinit var profilePicImg: CircleImageView

    var v: View? = null

    lateinit var progerssProgressDialog: ProgressDialog
    var dataList: ProfileModel? = null

    lateinit var sharedPreferences: SharedPreferences
    private val sharedPrefDB = "izzpay_db"

    lateinit var sign_back_arrow: ImageButton


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        v = inflater.inflate(R.layout.fragment_personal_profile, container, false)

        getActivity()!!.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)

        sharedPreferences = activity!!.getSharedPreferences(sharedPrefDB, Context.MODE_PRIVATE)

        init()

        mListeners()

        return v
    }

    override fun onResume() {

        super.onResume()

        val login = sharedPreferences.getString("userID", "")

    }

    private fun init() {


        sign_back_arrow = v!!.findViewById(R.id.sign_back_arrow)
        first_name_edt = v!!.findViewById(R.id.first_name_edt)
        last_name_edt = v!!.findViewById(R.id.last_name_edt)
        fathers_name_edt = v!!.findViewById(R.id.fathers_name_edt)
        mother_name_edt = v!!.findViewById(R.id.mother_name_edt)
        phoneEdt = v!!.findViewById(R.id.phoneEdt)
        date_of_birth_edt = v!!.findViewById(R.id.date_of_birth_edt)
        gender_edt = v!!.findViewById(R.id.gender_edt)
        marital_status_edt = v!!.findViewById(R.id.marital_status_edt)
        gmail_edt = v!!.findViewById(R.id.gmail_edt)
        highestEducationEdt = v!!.findViewById(R.id.highestEducationEdt)
        proceedRl = v!!.findViewById(R.id.proceedRl)
        profilePicImg = v!!.findViewById(R.id.profilePicImg)


        progerssProgressDialog = ProgressDialog(activity)
        progerssProgressDialog.setTitle("Please wait...")
        progerssProgressDialog.setCancelable(false)



        var ImgProfile = arguments?.getString("ImgProfile")
        val First_name = arguments?.getString("strFirstName")
        val Last_name = arguments?.getString("strLastName")
        val phone_no = arguments?.getString("strPhone")
        val email_ = arguments?.getString("strEmail")
        val dof = arguments?.getString("strDOB")
        val gender_ = arguments?.getString("strGender")
        val ImageUrl = arguments?.getString("strProfileImg")
        val father = arguments?.getString("father")
        val mothername = arguments?.getString("mothername")
        val martial = arguments?.getString("martial")
        val highestEdu = arguments?.getString("highestEdu")


        first_name_edt.setText(First_name)
        last_name_edt.setText(Last_name)
        phoneEdt.setText(phone_no)
        gmail_edt.setText(email_)
        date_of_birth_edt.setText(dof)
        gender_edt.setText(gender_)
        marital_status_edt.setText(martial)
        highestEducationEdt.setText(highestEdu)
        fathers_name_edt.setText(father)
        mother_name_edt.setText(mothername)

        Glide.with(activity!!)
                .load(Constant.mImageUrl+ImageUrl)
                .placeholder(R.drawable.ic_user_photo)
                .into(profilePicImg)


    }


    private fun mListeners() {




        gender_edt.setOnClickListener {
            val popup = PopupMenu(requireActivity(), gender_edt)
            popup.getMenuInflater()
                    .inflate(R.menu.gender, popup.getMenu())
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    gender_edt.setText(item.getTitle())
                    return true
                }
            })
            popup.show()
        }
        marital_status_edt.setOnClickListener {
            val popup = PopupMenu(activity, marital_status_edt)
            popup.getMenuInflater().inflate(R.menu.marital_status, popup.getMenu())
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    marital_status_edt.setText(item.getTitle())
                    return true
                }
            })
            popup.show()
        }

        highestEducationEdt.setOnClickListener {

            val popup = PopupMenu(activity, marital_status_edt)
            popup.getMenuInflater().inflate(R.menu.highest_education, popup.getMenu())
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    highestEducationEdt.setText(item.getTitle())
                    return true
                }
            })
            popup.show()
        }

        sign_back_arrow.setOnClickListener {
            val fm = activity!!.getSupportFragmentManager();
            fm.popBackStack();

        }

        date_of_birth_edt.setOnClickListener {

            val cal = Calendar.getInstance()
            val y = cal.get(Calendar.YEAR)
            val m = cal.get(Calendar.MONTH)
            val d = cal.get(Calendar.DAY_OF_MONTH)


            val datepickerdialog: DatePickerDialog = DatePickerDialog(activity!!, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                date_of_birth_edt.setText("" + dayOfMonth + "-" + monthOfYear + "-" + year)

            }, y, m, d)
            datepickerdialog.datePicker.maxDate = cal.timeInMillis
            datepickerdialog.show()
        }
        profilePicImg.setOnClickListener {
            val i = Intent(activity, ActivityBaseForFragment::class.java)
            i.putExtra("checkPage", "proof_self")
            startActivity(i)
        }

        proceedRl.setOnClickListener {


            if (!first_name_edt.text.toString().isEmpty()) {
                first_name_edt.setError(null)
                if (!last_name_edt.text.toString().isEmpty()) {
                    last_name_edt.setError(null)
                    if (!fathers_name_edt.text.toString().isEmpty()) {
                        fathers_name_edt.setError(null)
                        if (!mother_name_edt.text.toString().isEmpty()) {
                            mother_name_edt.setError(null)

                            if (!date_of_birth_edt.text.toString().isEmpty()) {

                                date_of_birth_edt.setError(null)

                                if (!gender_edt.text.toString().isEmpty()) {

                                    gender_edt.setError(null)

                                    if (!marital_status_edt.text.toString().isEmpty()) {
                                        marital_status_edt.setError(null)

                                        if (!phoneEdt.text.toString().isEmpty()) {
                                            phoneEdt.setError(null)

                                            if (phoneEdt.text.toString().length == 10) {
                                                phoneEdt.setError(null)
                                                if (!gmail_edt.text.toString().isEmpty()) {
                                                    gmail_edt.setError(null)

                                                    if (!highestEducationEdt.text.toString().isEmpty()) {
                                                        highestEducationEdt.setError(null)

                                                        mUpdateUserProfile(first_name_edt.text.toString(),last_name_edt.text.toString(),fathers_name_edt.text.toString(),mother_name_edt.text.toString(),date_of_birth_edt.text.toString(),gender_edt.text.toString(),marital_status_edt.text.toString(),phoneEdt.text.toString(),gmail_edt.text.toString(),highestEducationEdt.text.toString())
                                                    } else {
                                                        highestEducationEdt.setError("enter your education")
                                                    }
                                                } else {
                                                    gmail_edt.setError("enter your email address")
                                                }
                                            } else {
                                                phoneEdt.setError("enter 10 digits phone number")
                                            }

                                        } else {
                                            phoneEdt.setError("enter phone number")
                                        }

                                    } else {
                                        marital_status_edt.setError("enter your marital status")
                                    }


                                } else {
                                    gender_edt.setError("enter your gender")
                                }


                            } else {
                                date_of_birth_edt.setError("enter your date of birth")
                            }
                        } else {
                            mother_name_edt.setError("enter your mother's name")
                        }
                    } else {
                        fathers_name_edt.setError("enter your father's name")
                    }
                } else {
                    last_name_edt.setError("enter your last name")
                }
            } else {
                first_name_edt.setError("enter your first name")
            }
        }
    }


    private fun mUpdateUserProfile(first_name: String, last_name: String, father_name: String, mother_name: String, dateOfBirth: String, gender: String,  marital_status: String, mobile: String, email: String, highest_education: String) {

        if (Constant.hasNetworkAvailable(activity!!)) {

            val Access_Token = sharedPreferences.getString("Access_Token", "")
            val userID = sharedPreferences.getString("Access_Token", "")


//            val paramObject = JSONObject()
//
//            val abc = HashMap<String,String>()
//            abc.put("fatherName", father_name)
//            abc.put("motherName3", mother_name)
//            abc.put("highestEducation", highest_education)
//            abc.put("martialStatus", marital_status)

            progerssProgressDialog.show()

            try {
                val call: Call<ProfileModel> = ApiClient.getClient.updateUserProfile("Bearer "+Access_Token,first_name, last_name, father_name,mother_name,dateOfBirth,gender,marital_status,highest_education,mobile,email)
                call.enqueue(object : Callback<ProfileModel> {
                    override fun onFailure(call: Call<ProfileModel>, t: Throwable) {
                        progerssProgressDialog.dismiss()
                        Toast.makeText(activity, t.message, Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<ProfileModel>, response: retrofit2.Response<ProfileModel>) {

                        progerssProgressDialog.dismiss()

                        try {
                            if (response.code() == 200)
                            {
                                dataList = response.body()

                                Toast.makeText(activity, dataList!!.getMessage().toString(), Toast.LENGTH_SHORT).show()

                            }
                            else if (response.code() == 409)
                            {
                                Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show()
                            }
                            else if (response.code() == 404)
                            {
                                Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show()
                            }
                        } catch (exception: Exception) {
                            Toast.makeText(activity, exception.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                })

            } catch (ex: Exception) {
                progerssProgressDialog.hide()
                Toast.makeText(activity, ex.message.toString(), Toast.LENGTH_SHORT).show()
            }

        } else {
            Toast.makeText(activity, "No network available!", Toast.LENGTH_SHORT).show()
        }
    }

}
