package com.loan.izzpay.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.facebook.FacebookSdk
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.loan.izzpay.R
import com.loan.izzpay.RetrofitConnections.ApiClient
import com.loan.izzpay.RetrofitConnections.ApiInterface
import com.loan.izzpay.dataModels.MyResponse
import com.loan.izzpay.models.AadharUploadModel
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class FragmentAadhar : Fragment() {


    private var self_btnCamera: TextView? = null
    private var self_gallery: TextView? = null
    private var self_image_view: ImageView? = null
    private var self_upload: Button? = null

    val INTENT_IMAGE_PICKER_OPTION = "image_picker_option"
    val INTENT_ASPECT_RATIO_X = "aspect_ratio_x"
    val INTENT_ASPECT_RATIO_Y = "aspect_ratio_Y"
    val INTENT_LOCK_ASPECT_RATIO = "lock_aspect_ratio"
    val INTENT_IMAGE_COMPRESSION_QUALITY = "compression_quality"
    val INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT = "set_bitmap_max_width_height"
    val INTENT_BITMAP_MAX_WIDTH = "max_width"
    val INTENT_BITMAP_MAX_HEIGHT = "max_height"
    val REQUEST_IMAGE_CAPTURE = 0
    val REQUEST_GALLERY_IMAGE = 1
    private val lockAspectRatio = false
    private val setBitmapMaxWidthHeight = false
    private val ASPECT_RATIO_X = 16
    private val ASPECT_RATIO_Y = 9
    private val bitmapMaxWidth = 1000
    private val bitmapMaxHeight = 1000
    private val IMAGE_COMPRESSION = 80
    var fileName: String? = null
    private val REQUEST_IMAGE = 1
    var myUri: Uri? = null
    var intent: Intent? = null
    var finalFile: File? = null
    var profile_image: CircleImageView? = null
    var image: MultipartBody.Part? = null
    private var self_image1: ImageView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val v: View = inflater.inflate(R.layout.fragment_proof_aadhar, container, false)



//        self_btnCamera = v.findViewById(R.id.self_btnCamera)
//        self_gallery = v.findViewById(R.id.self_gallery)
//        self_image_view = v.findViewById(R.id.self_image_view)
//        self_upload = v.findViewById(R.id.self_upload)
//        self_image1 = v.findViewById(R.id.self_image1)
//
//        self_btnCamera!!.setOnClickListener{
//
//            Dexter.withActivity(activity)
//                    .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                    .withListener(object: MultiplePermissionsListener() {
//                        override fun onPermissionsChecked(report: MultiplePermissionsReport) {
//                            if (report.areAllPermissionsGranted())
//                            {
//                                launchCameraIntent()
//                            }
//                            if (report.isAnyPermissionPermanentlyDenied())
//                            {
//                                showSettingsDialog()
//                            }
//                        }
//                        override fun onPermissionRationaleShouldBeShown(permissions:List<PermissionRequest>, token: PermissionToken) {
//                            token.continuePermissionRequest()
//                        }
//                    }).check()
//        }
//
//        self_gallery!!.setOnClickListener{
//            Dexter.withActivity(activity)
//                    .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                    .withListener(object:MultiplePermissionsListener() {
//                        override fun onPermissionsChecked(report:MultiplePermissionsReport) {
//                            if (report.areAllPermissionsGranted())
//                            {
//                                launchGalleryIntent()
//                            }
//                            if (report.isAnyPermissionPermanentlyDenied())
//                            {
//                                showSettingsDialog()
//                            }
//                        }
//                        override fun onPermissionRationaleShouldBeShown(permissions:List<PermissionRequest>, token:PermissionToken) {
//                            token.continuePermissionRequest()
//                        }
//                    }).check()
//        }
//        self_upload!!.setOnClickListener{
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
//            {
//                uploadPhoto("19")
//            }
//        }
//        self_image1!!.setOnClickListener{
//            activity!!.finish()
//        }


        return v
    }





//    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
//        //called when user presses ALLOW or DENY from Permission Request Popup
//        when (requestCode) {
//            PERMISSION_CODE -> {
//                if (grantResults.size > 0 && grantResults[0] ==
//                        PackageManager.PERMISSION_GRANTED) {
//                    //permission from popup was granted
//                    openCamera()
//                } else {
//                    //permission from popup was denied
//                    Toast.makeText(activity!!, "Permission denied", Toast.LENGTH_SHORT).show()
//                }
//
//            }
//        }
//    }

//    @SuppressLint("MissingSuperCall")
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        if (resultCode == Activity.RESULT_OK) {
//            //set image captured to image view
//            image_view.setImageURI(image_uri)
//
////            myUri = data!!.getParcelableExtra("path")
////            file = File(myUri!!.getPath()!!)
//
//        }
//        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE){
//            image_view.setImageURI(data?.data) // handle chosen image
//        }
//    }

    lateinit var progerssProgressDialog: ProgressDialog
    

//    private fun uploadFile() {
//
//
//        val value1: Int = 19
//
//        if (flag == 0) {
//            val compressionRatio = 15//1 == originalImage, 2 = 50% compression, 4=25% compress
//            file = File("")
//            try {
//                val bitmap = BitmapFactory.decodeFile(file!!.getPath())
//                bitmap.compress(Bitmap.CompressFormat.JPEG, compressionRatio, FileOutputStream(file))
//
//            } catch (t: Throwable) {
//                Log.e("ERROR", "Error compressing file." + t.toString())
//                t.printStackTrace()
//            }
//        } else if (flag == 1) {
//            file = File("")
//        }
//
//        progerssProgressDialog = ProgressDialog(activity)
//        progerssProgressDialog.setTitle("Please wait...")
//        progerssProgressDialog.setCancelable(false)
//        progerssProgressDialog.show()
//
//
//        var requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
//        // Toast.makeText(applicationContext,requestFile.toString(),Toast.LENGTH_SHORT).show()
//
//        val uid = RequestBody.create(okhttp3.MultipartBody.FORM, "21")
//        val typeProof = RequestBody.create(okhttp3.MultipartBody.FORM, "Aadhar")
//        val details = RequestBody.create(okhttp3.MultipartBody.FORM, "345689701234")
//
//        val body: MultipartBody.Part = MultipartBody.Part.createFormData("image", file!!.name, requestFile);
////        Log.d("filename", file.name)
//
//
//        val ApiService = ApiClient.buildService(ApiInterface::class.java)
//        val requestCall = ApiService.uploadAadhar(body,uid,typeProof,details)
//        requestCall.enqueue(object : Callback<AadharUploadModel> {
//            override fun onResponse(call: Call<AadharUploadModel>, response: Response<AadharUploadModel>) {
//                progerssProgressDialog.dismiss()
//                try {
//                    Toast.makeText(FacebookSdk.getApplicationContext(), response.body()!!.getMessage(), Toast.LENGTH_LONG).show()
//
//                } catch (e: IOException) {
//                    e.printStackTrace()
//                }
//            }
//
//            override fun onFailure(call: Call<AadharUploadModel>, t: Throwable) {
//                Log.d("error_msg", t.message)
//                progerssProgressDialog.dismiss()
//                Toast.makeText(FacebookSdk.getApplicationContext(), t.message, Toast.LENGTH_LONG).show()
//            }
//        })
//    }

}

