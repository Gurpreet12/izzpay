package com.loan.izzpay.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import com.loan.izzpay.R

class FragmentEligibility  :Fragment() {

    private lateinit var cal_back_arrow: ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v: View = inflater.inflate(R.layout.fragment_eligibility, container, false)

        cal_back_arrow = v.findViewById(R.id.cal_back_arrow)

        cal_back_arrow.setOnClickListener { activity!!.finish() }


        return v
    }}