package com.loan.izzpay.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

import com.loan.izzpay.R


class FragmentInstantCashScreen : Fragment() {

    private lateinit var cash_backimage:ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_cash_screen, container, false)

        cash_backimage = view.findViewById(R.id.cash_backimage)

        cash_backimage.setOnClickListener {
            activity!!.finish()
        }

        return view
    }


}
