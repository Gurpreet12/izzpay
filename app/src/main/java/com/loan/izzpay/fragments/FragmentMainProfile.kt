package com.loan.izzpay.fragments

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.*
import androidx.fragment.app.Fragment
import com.loan.izzpay.R

import android.view.*
import androidx.cardview.widget.CardView
import com.loan.izzpay.RetrofitConnections.ApiClient
import com.loan.izzpay.activities.ActivityProofs
import com.loan.izzpay.models.ProfileModel
import com.loan.izzpay.utils.Constant
import retrofit2.Call
import retrofit2.Callback
import java.lang.Exception


class FragmentMainProfile : Fragment() {


    private lateinit var professionalImg: ImageView
    private lateinit var addressProofImg: ImageView


    var v: View? = null

    lateinit var progerssProgressDialog: ProgressDialog
    var dataList: ProfileModel? = null

    lateinit var sharedPreferences: SharedPreferences
    private val sharedPrefDB = "izzpay_db"

    lateinit var sign_back_arrow: ImageButton


    lateinit var personal_profile_cardview: CardView
    lateinit var professional_profile_cardview: CardView
    lateinit var current_residential_address_cardview: CardView
    lateinit var financial_cardview: CardView
    lateinit var moveProfessionalProfile: ImageView
    lateinit var moveCurrentResdentialAddressTv: ImageView
    var dashBoardProfile: ProfileModel? = null


     var strFirstName: String?=null
     var strLastName: String?=null
     var strPhone: String?=null
     var strEmail: String?=null
     var strDOB: String?=null
     var strGender: String?=null
     var strProfileImg: String?=null
     var fatherName: String?=null
     var motherName: String?=null
     var MartialStatus: String?=null
     var highestEducation: String?=null

    var strProfession: String?=null
    var strCompanyName: String?=null
    var strDesignation: String?=null
    var strTotalExperience: String?=null
    var strMontlyIncome: String?=null

    var strOfficeArea: String?=null
    var strOfficeCity: String?=null
    var strOficePincode: String?=null
    var strMonthOfExperienced: String?=null





    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        v = inflater.inflate(R.layout.fragment_main_profile, container, false)

        getActivity()!!.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)

        init()

        mListeners()

        return v
    }

    override fun onResume() {
        super.onResume()

        mDashBoard()
    }

    private fun init() {


        professional_profile_cardview = v!!.findViewById(R.id.professional_profile_cardview)
        moveProfessionalProfile = v!!.findViewById(R.id.moveProfessionalProfile)
        moveCurrentResdentialAddressTv = v!!.findViewById(R.id.moveCurrentResdentialAddressTv)
        financial_cardview = v!!.findViewById(R.id.financial_cardview)
        sign_back_arrow = v!!.findViewById(R.id.sign_back_arrow)

        personal_profile_cardview = v!!.findViewById(R.id.personal_profile_cardview)
        professional_profile_cardview = v!!.findViewById(R.id.professional_profile_cardview)
        current_residential_address_cardview = v!!.findViewById(R.id.current_residential_address_cardview)
        professionalImg = v!!.findViewById(R.id.professionalImg)
        addressProofImg = v!!.findViewById(R.id.addressProofImg)

        progerssProgressDialog = ProgressDialog(activity)
        progerssProgressDialog.setTitle("Please wait...")
        progerssProgressDialog.setCancelable(false)
        progerssProgressDialog.show()

        sharedPreferences = activity!!.getSharedPreferences(sharedPrefDB, Context.MODE_PRIVATE)

    }


    private fun mListeners() {

        personal_profile_cardview.setOnClickListener {


            val bundle = Bundle()
            bundle.putString("strFirstName", strFirstName)
            bundle.putString("strLastName", strLastName)
            bundle.putString("strPhone", strPhone)
            bundle.putString("strEmail", strEmail)
            bundle.putString("strDOB", strDOB)
            bundle.putString("strGender", strGender)
            bundle.putString("strProfileImg", strProfileImg)
            bundle.putString("father", fatherName)
            bundle.putString("mothername", motherName)
            bundle.putString("martial", MartialStatus)
            bundle.putString("highestEdu", highestEducation)
            val transcation = activity!!.supportFragmentManager.beginTransaction()
            transcation.isAddToBackStackAllowed
            val fragment_edit_profie = FragmentPersonalProfile()
            fragment_edit_profie.arguments = bundle
            transcation.replace(R.id.frame_layout_base_fragment, fragment_edit_profie)
            transcation.addToBackStack(null)
            transcation.commit()

        }
        professional_profile_cardview.setOnClickListener {


            val bundle = Bundle()
            bundle.putString("profession", strProfession)
            bundle.putString("companyName", strCompanyName)
            bundle.putString("designation", strDesignation)
            bundle.putString("officeArea", strOfficeArea)
            bundle.putString("officeCity", strOfficeCity)
            bundle.putString("officePincode", strOficePincode)
            bundle.putString("monthOfExperienced", strMonthOfExperienced)
            bundle.putString("totalExperience", strTotalExperience)
            bundle.putString("montlyIncome", strMontlyIncome)


            val transcation = activity!!.supportFragmentManager.beginTransaction()
            transcation.isAddToBackStackAllowed
            val fragmentProfessionalProfile = FragmentProfessionalProfile()
            fragmentProfessionalProfile.arguments = bundle
            transcation.replace(R.id.frame_layout_base_fragment, fragmentProfessionalProfile)
            transcation.addToBackStack(null)
            transcation.commit()

        }
        current_residential_address_cardview.setOnClickListener {

            val transcation = activity!!.supportFragmentManager.beginTransaction()
            transcation.isAddToBackStackAllowed
            val fragmentAddressProofsProfile = FragmentAddressProofsProfile()
            transcation.replace(R.id.frame_layout_base_fragment, fragmentAddressProofsProfile)
            transcation.addToBackStack(null)
            transcation.commit()
        }
        financial_cardview.setOnClickListener {
            val intent = Intent(activity, ActivityProofs::class.java)
            activity!!.startActivity(intent)
        }


        sign_back_arrow.setOnClickListener {

            activity!!.finish()
        }
    }





    private fun mDashBoard() {

        if (Constant.hasNetworkAvailable(activity!!)) {

            val Access_Token = sharedPreferences.getString("Access_Token", "")

            progerssProgressDialog.show()

            try {
                val call: Call<ProfileModel> = ApiClient.getClient.dashBoardProfile("Bearer "+Access_Token)
                call.enqueue(object : Callback<ProfileModel> {
                    override fun onFailure(call: Call<ProfileModel>, t: Throwable) {
                        progerssProgressDialog.dismiss()
                        Toast.makeText(activity, t.message, Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<ProfileModel>, response: retrofit2.Response<ProfileModel>) {

                        progerssProgressDialog.dismiss()

                        try {

                            dashBoardProfile = response.body()

                            if (dashBoardProfile!!.getSuccess() == true)
                            {
                                val msg = dashBoardProfile!!.getMessage()
                                val first = dashBoardProfile!!.getData()!!.get(0).firstName
                                val last = dashBoardProfile!!.getData()!!.get(0).lastName
                                val email = dashBoardProfile!!.getData()!!.get(0).email
                                val phone = dashBoardProfile!!.getData()!!.get(0).phone
                                val dof = dashBoardProfile!!.getData()!!.get(0).dateOfBirth
                                val gender = dashBoardProfile!!.getData()!!.get(0).gender
                                val father = dashBoardProfile!!.getData()!!.get(0).fatherName
                                val mothername = dashBoardProfile!!.getData()!!.get(0).motherName
                                val martial = dashBoardProfile!!.getData()!!.get(0).martialStatus
                                val highestEdu = dashBoardProfile!!.getData()!!.get(0).highestEducation
                                val profileImage = dashBoardProfile!!.getData()!!.get(0).profileImage


                                val profession = dashBoardProfile!!.getData()!!.get(0).profession
                                val companyName = dashBoardProfile!!.getData()!!.get(0).companyName
                                val designation = dashBoardProfile!!.getData()!!.get(0).designation
                                val totalExperience = dashBoardProfile!!.getData()!!.get(0).totalExperience
                                val montlyIncome = dashBoardProfile!!.getData()!!.get(0).montlyIncome

                                val officeArea = dashBoardProfile!!.getData()!!.get(0).officeArea
                                val officeCity = dashBoardProfile!!.getData()!!.get(0).officeCity
                                val officePincode = dashBoardProfile!!.getData()!!.get(0).officePincode
                                val monthOfExperienced = dashBoardProfile!!.getData()!!.get(0).monthOfExperienced

                                strFirstName = first.toString()
                                strLastName = last.toString()
                                strPhone = phone.toString()
                                strEmail = email.toString()
                                strDOB = dof.toString()
                                strGender = gender.toString()
                                strProfileImg = profileImage.toString()
                                fatherName= father
                                motherName= mothername
                                MartialStatus=martial
                                highestEducation = highestEdu

                                strProfession = profession
                                strCompanyName = companyName
                                strDesignation = designation
                                strTotalExperience = totalExperience
                                strMontlyIncome = montlyIncome

                                strOfficeArea = officeArea
                                strOfficeCity = officeCity
                                strOficePincode = officePincode
                                strMonthOfExperienced = monthOfExperienced



                            }
                            else
                            {
                                Toast.makeText(activity!!, dashBoardProfile!!.getMessage().toString(), Toast.LENGTH_SHORT).show()
                            }




                        } catch (exception: Exception) {
                            Toast.makeText(activity!!, exception.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                })

            } catch (ex: java.lang.Exception) {
                Toast.makeText(activity!!, ex.message.toString(), Toast.LENGTH_SHORT).show()
            }

        } else {
            Toast.makeText(activity!!, "No network available!", Toast.LENGTH_SHORT).show()
        }
    }

}
