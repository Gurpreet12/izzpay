package com.loan.izzpay.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.loan.izzpay.R
import com.loan.izzpay.adapters.RecyclerLoanTransactionDescription
import com.loan.izzpay.dataModels.LoanTransactionDetailsPojo


class FragmentLoanStatementDetails  : Fragment() {
    private val data = arrayListOf<LoanTransactionDetailsPojo>(
            LoanTransactionDetailsPojo(10000,"paid", "11-01-2020","Emi 1"),
            LoanTransactionDetailsPojo(10000,"paid", "11-02-2020","Emi 1"),
            LoanTransactionDetailsPojo(10000,"due", "11-03-2020","Emi 1")
    )
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var recyclerView: RecyclerView
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v: View = inflater.inflate(R.layout.fragment_loan_statement_details, container, false)
        recyclerView=v.findViewById(R.id.det_recycler)
        linearLayoutManager = LinearLayoutManager(activity)
        recyclerView.layoutManager=linearLayoutManager
        val adapter= RecyclerLoanTransactionDescription(data)
        recyclerView.adapter=adapter
        return v
    }
}