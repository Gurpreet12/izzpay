package com.loan.izzpay.fragments

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.loan.izzpay.R
import com.loan.izzpay.RetrofitConnections.ApiClient
import com.loan.izzpay.adapters.AdapterCustomFragmentInstitutesAll
import com.loan.izzpay.models.ModelsAllInstitutes
import com.loan.izzpay.utils.Constant
import retrofit2.Call
import retrofit2.Callback

class FragmentInstitutes : Fragment() {


    private lateinit var typesCustomSchoolsCollegesAdapter: AdapterCustomFragmentInstitutesAll

    var myRecyclerview_new: RecyclerView?= null
    lateinit var progerssProgressDialog: ProgressDialog
    var dataList: ModelsAllInstitutes? = null

    private lateinit var linearLayoutManager: LinearLayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.fragment_institutes, container, false)

        myRecyclerview_new = view.findViewById(R.id.myRecyclerview_new)as RecyclerView
        linearLayoutManager = LinearLayoutManager(activity)
        myRecyclerview_new!!.layoutManager=linearLayoutManager

        progerssProgressDialog = ProgressDialog(activity)
        progerssProgressDialog.setTitle("Please wait...")
        progerssProgressDialog.setCancelable(false)
        progerssProgressDialog.show()

        getInstitutes()

        return view
    }

    private fun getInstitutes() {

        if (Constant.hasNetworkAvailable(activity!!)) {
            try {
                val call: Call<ModelsAllInstitutes> = ApiClient.getClient.allInstitutes(1)
                call.enqueue(object : Callback<ModelsAllInstitutes> {
                    override fun onFailure(call: Call<ModelsAllInstitutes>, t: Throwable) {
                        progerssProgressDialog.dismiss()
                        Toast.makeText(activity, t.message, Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<ModelsAllInstitutes>, response: retrofit2.Response<ModelsAllInstitutes>) {

                        progerssProgressDialog.dismiss()

                        dataList = response.body()
                        if (dataList == null)
                        {

                            Toast.makeText(activity, "No data found", Toast.LENGTH_SHORT).show()
                        }
                        else{
                            val code = dataList!!.getMessage()
//                            Toast.makeText(activity, code, Toast.LENGTH_SHORT).show()

                            typesCustomSchoolsCollegesAdapter = AdapterCustomFragmentInstitutesAll(activity!!,dataList)
                            myRecyclerview_new!!.adapter = typesCustomSchoolsCollegesAdapter
                        }
                    }

                })

            } catch (ex: java.lang.Exception) {
                Toast.makeText(activity, ex.message.toString(), Toast.LENGTH_SHORT).show()
            }

        } else {
            Toast.makeText(activity, "No network available!", Toast.LENGTH_SHORT).show()
        }
    }
}
