package com.loan.izzpay.dataModels

data class FaqQuestions (
        var questions: String,
        var answer: String
)