package com.loan.izzpay.dataModels

import com.google.gson.annotations.SerializedName

data class PhotoDetails (
        var code:Int,
        val message: String,
        @SerializedName("data")
        val data:List<PhotosInfo>
)
data class PhotosInfo (
        var id:Int,
        var uid:Int,
        var proofType:String,
        var image:Any,
        var details:String

)