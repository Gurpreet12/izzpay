package com.loan.izzpay.dataModels

import com.google.gson.annotations.SerializedName

data class ImagePojo(

        var id:Int,
        @SerializedName("profileImage")
        var profileImage:Any,
        var gender:String

)