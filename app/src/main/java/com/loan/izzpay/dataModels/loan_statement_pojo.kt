package com.loan.izzpay.dataModels

data class loan_statement_pojo(
        val amount: Int,
        val date: String,
        val status: String,
        val tenure: String

)