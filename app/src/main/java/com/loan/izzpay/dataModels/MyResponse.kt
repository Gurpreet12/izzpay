package com.loan.izzpay.dataModels

data class MyResponse (
        var code:Int,
        var message:String,
        var data:ImagePojo

)