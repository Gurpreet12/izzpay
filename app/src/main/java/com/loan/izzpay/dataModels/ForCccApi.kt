package com.loan.izzpay.dataModels

import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import androidx.appcompat.widget.AppCompatSpinner

data class ForCccApi (
    var first_name: String? = null,
    var last_name: String? = null,
    var phone_no: String? = null,
    var email: String? = null,
    var date_of_birth: String? = null,
    var pan_no: String? = null,
    var state: String? = null,
    var city: String? = null,
    var pincode: String? = null,
    var address_line1: String? = null,
    var address_line2: String? = null,
    var gender: String? = null,
    var applicant_constitution: String? = null,
    var vendor_name: String? = null,
    var customer_id: String? = null,
    var requested_loan_amount: String? = null,
    var request_id: String? = null,
    var address_type: String? = null,
    var journey: String? = null


)