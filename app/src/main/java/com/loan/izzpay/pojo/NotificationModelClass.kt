package com.loan.izzpay.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName




class NotificationModelClass {


    @SerializedName("code")
    @Expose
    private var code: Int? = null
    @SerializedName("success")
    @Expose
    private var success: Boolean? = null
    @SerializedName("data")
    @Expose
    private var data: List<Datum>? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null

    fun getCode(): Int? {
        return code
    }

    fun setCode(code: Int?) {
        this.code = code
    }

    fun getSuccess(): Boolean? {
        return success
    }

    fun setSuccess(success: Boolean?) {
        this.success = success
    }

    fun getData(): List<Datum>? {
        return data
    }

    fun setData(data: List<Datum>) {
        this.data = data
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    inner class Datum {

        @SerializedName("id")
        @Expose
        var id: Int? = null
        @SerializedName("title")
        @Expose
        var title: String? = null
        @SerializedName("description")
        @Expose
        var description: String? = null
        @SerializedName("image")
        @Expose
        var image: String? = null
        @SerializedName("type")
        @Expose
        var type: String? = null
        @SerializedName("uid")
        @Expose
        var uid: Int? = null
        @SerializedName("created")
        @Expose
        var created: String? = null
        @SerializedName("readStatus")
        @Expose
        var readStatus: Int? = null

    }
}