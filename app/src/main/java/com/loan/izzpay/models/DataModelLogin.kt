package com.loan.izzpay.models

data class DataModelLogin (
        val message: String,
        val success: Boolean,
        val code:String
)