package com.loan.izzpay.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class DashBoardProfile {

    @SerializedName("code")
    @Expose
    private var code: Int? = null
    @SerializedName("success")
    @Expose
    private var success: Boolean? = null
    @SerializedName("data")
    @Expose
    private var data: List<Datum>? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null

    fun getCode(): Int? {
        return code
    }

    fun setCode(code: Int?) {
        this.code = code
    }

    fun getSuccess(): Boolean? {
        return success
    }

    fun setSuccess(success: Boolean?) {
        this.success = success
    }

    fun getData(): List<Datum>? {
        return data
    }

    fun setData(data: List<Datum>) {
        this.data = data
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    inner class Datum {

        @SerializedName("id")
        @Expose
        var id: Int? = null
        @SerializedName("loginType")
        @Expose
        var loginType: String? = null
        @SerializedName("loginSocialToken")
        @Expose
        var loginSocialToken: String? = null
        @SerializedName("first_name")
        @Expose
        var firstName: String? = null
        @SerializedName("last_name")
        @Expose
        var lastName: String? = null
        @SerializedName("date_of_birth")
        @Expose
        var dateOfBirth: String? = null
        @SerializedName("customer_id")
        @Expose
        var customerId: String? = null
        @SerializedName("journey")
        @Expose
        var journey: Any? = null
        @SerializedName("nationality")
        @Expose
        var nationality: String? = null
        @SerializedName("gender")
        @Expose
        var gender: String? = null
        @SerializedName("applicant_constitution")
        @Expose
        var applicantConstitution: Any? = null
        @SerializedName("request_id")
        @Expose
        var requestId: Any? = null
        @SerializedName("vendor_name")
        @Expose
        var vendorName: Any? = null
        @SerializedName("citizenship")
        @Expose
        var citizenship: String? = null
        @SerializedName("requested_loan_amount")
        @Expose
        var requestedLoanAmount: Any? = null
        @SerializedName("profileImage")
        @Expose
        var profileImage: Any? = null
        @SerializedName("accountStatus")
        @Expose
        var accountStatus: String? = null
        @SerializedName("userType")
        @Expose
        var userType: String? = null
        @SerializedName("email")
        @Expose
        var email: String? = null
        @SerializedName("email_verified_status")
        @Expose
        var emailVerifiedStatus: String? = null
        @SerializedName("phone")
        @Expose
        var phone: String? = null
        @SerializedName("phone_verified_status")
        @Expose
        var phoneVerifiedStatus: String? = null
        @SerializedName("created_at")
        @Expose
        var createdAt: String? = null
        @SerializedName("updated_at")
        @Expose
        var updatedAt: Any? = null
        @SerializedName("kycStatus")
        @Expose
        var kycStatus: Int? = null

    }
}