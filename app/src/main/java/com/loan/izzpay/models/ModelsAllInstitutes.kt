package com.loan.izzpay.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class ModelsAllInstitutes {

    inner class Datum {

        @SerializedName("id")
        @Expose
        var id: Long? = null
        @SerializedName("instiTypeId")
        @Expose
        var instiTypeId: Long? = null
        @SerializedName("instiName")
        @Expose
        var instiName: String? = null
        @SerializedName("i_image")
        @Expose
        var iImage: Any? = null
        @SerializedName("i_logo")
        @Expose
        var iLogo: String? = null
        @SerializedName("i_status")
        @Expose
        var iStatus: String? = null
        @SerializedName("i_email")
        @Expose
        var iEmail: String? = null
        @SerializedName("i_phoneNo")
        @Expose
        var iPhoneNo: String? = null
        @SerializedName("created_at")
        @Expose
        var createdAt: String? = null

    }

    @SerializedName("code")
    @Expose
    private var code: Long? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("data")
    @Expose
    private var data: List<Datum>? = null

    fun getCode(): Long? {
        return code
    }

    fun setCode(code: Long?) {
        this.code = code
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    fun getData(): List<Datum>? {
        return data
    }

    fun setData(data: List<Datum>) {
        this.data = data
    }
}