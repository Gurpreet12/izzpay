package com.loan.izzpay.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import android.R.attr.data
import android.R.attr.data







class LoginModel {

    @SerializedName("code")
    @Expose
    private var code: Int? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("data")
    @Expose
    private var data: Data? = null

    fun getCode(): Int? {
        return code
    }

    fun setCode(code: Int?) {
        this.code = code
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    fun getData(): Data? {
        return data
    }

    fun setData(data: Data) {
        this.data = data
    }


    inner class Data {

        @SerializedName("id")
        @Expose
        var id: Int? = null
        @SerializedName("loginType")
        @Expose
        var loginType: String? = null
        @SerializedName("loginSocialToken")
        @Expose
        var loginSocialToken: String? = null
        @SerializedName("last_name")
        @Expose
        var lastName: String? = null
        @SerializedName("citizenship")
        @Expose
        var citizenship: String? = null
        @SerializedName("pincode")
        @Expose
        var pincode: Any? = null
        @SerializedName("aadhar")
        @Expose
        var aadhar: Any? = null
        @SerializedName("address_line2")
        @Expose
        var addressLine2: Any? = null
        @SerializedName("city")
        @Expose
        var city: Any? = null
        @SerializedName("first_name")
        @Expose
        var firstName: String? = null
        @SerializedName("address_line1")
        @Expose
        var addressLine1: Any? = null
        @SerializedName("passport_no")
        @Expose
        var passportNo: Any? = null
        @SerializedName("state")
        @Expose
        var state: Any? = null
        @SerializedName("date_of_birth")
        @Expose
        var dateOfBirth: Any? = null
        @SerializedName("customer_id")
        @Expose
        var customerId: Any? = null
        @SerializedName("email")
        @Expose
        var email: String? = null
        @SerializedName("journey")
        @Expose
        var journey: Any? = null
        @SerializedName("nationality")
        @Expose
        var nationality: String? = null
        @SerializedName("address_type")
        @Expose
        var addressType: Any? = null
        @SerializedName("voter_id")
        @Expose
        var voterId: Any? = null
        @SerializedName("mobile")
        @Expose
        var mobile: String? = null
        @SerializedName("gender")
        @Expose
        var gender: String? = null
        @SerializedName("applicant_constitution")
        @Expose
        var applicantConstitution: Any? = null
        @SerializedName("drv_licence_no")
        @Expose
        var drvLicenceNo: Any? = null
        @SerializedName("request_id")
        @Expose
        var requestId: Any? = null
        @SerializedName("vendor_name")
        @Expose
        var vendorName: Any? = null
        @SerializedName("requested_loan_amount")
        @Expose
        var requestedLoanAmount: Any? = null
        @SerializedName("accountStatus")
        @Expose
        var accountStatus: Any? = null
        @SerializedName("deviceType")
        @Expose
        var deviceType: String? = null
        @SerializedName("deviceToken")
        @Expose
        var deviceToken: Any? = null
        @SerializedName("profileImage")
        @Expose
        var profileImage: Any? = null
        @SerializedName("loginToken")
        @Expose
        var loginToken: Any? = null
        @SerializedName("remember_token")
        @Expose
        var rememberToken: Any? = null
        @SerializedName("created_at")
        @Expose
        var createdAt: String? = null
        @SerializedName("updated_at")
        @Expose
        var updatedAt: String? = null
        @SerializedName("otp")
        @Expose
        var otp: Int? = null

    }
}