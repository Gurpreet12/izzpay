package com.loan.izzpay.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



data class Mydata (
        var code:Int,
        var success:String,
        var message:String,
        val data:List<dataInside>
)
data class dataInside (
        @SerializedName("profileImage")
        @Expose
        var profileImage: String ,
        @SerializedName("first_name")
        @Expose
        var first_name:String

)