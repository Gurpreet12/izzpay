package com.loan.izzpay.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DefaultModel {

    @SerializedName("code")
    @Expose
    private var code: Int? = null
    @SerializedName("success")
    @Expose
    private var success: Boolean? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    fun getSuccess(): Boolean? {
        return success
    }

    fun setSuccess(success: Boolean?) {
        this.success = success
    }
    fun getCode(): Int? {
        return code
    }

    fun setCode(code: Int?) {
        this.code = code
    }

}