package com.loan.izzpay.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import android.R.attr.data
import android.R.attr.data




class AadharUploadModel {

    @SerializedName("code")
    @Expose
    private var code: Int? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("data")
    @Expose
    private var data: Data? = null

    fun getCode(): Int? {
        return code
    }

    fun setCode(code: Int?) {
        this.code = code
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    fun getData(): Data? {
        return data
    }

    fun setData(data: Data) {
        this.data = data
    }

    inner class Data {

        @SerializedName("image")
        @Expose
        var image: String? = null
        @SerializedName("uid")
        @Expose
        var uid: String? = null
        @SerializedName("proofType")
        @Expose
        var proofType: String? = null
        @SerializedName("details")
        @Expose
        var details: String? = null

    }
}