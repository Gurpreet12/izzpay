package com.loan.izzpay.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class ModelGetAllAddress {

    @SerializedName("code")
    @Expose
    private var code: Int? = null
    @SerializedName("success")
    @Expose
    private var success: Boolean? = null
    @SerializedName("data")
    @Expose
    private var data: List<Datum>? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null

    fun getCode(): Int? {
        return code
    }

    fun setCode(code: Int?) {
        this.code = code
    }

    fun getSuccess(): Boolean? {
        return success
    }

    fun setSuccess(success: Boolean?) {
        this.success = success
    }

    fun getData(): List<Datum>? {
        return data
    }

    fun setData(data: List<Datum>) {
        this.data = data
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    inner class Datum {

        @SerializedName("id")
        @Expose
        var id: Int? = null
        @SerializedName("user_id")
        @Expose
        var userId: Int? = null
        @SerializedName("pincode")
        @Expose
        var pincode: String? = null
        @SerializedName("address_line1")
        @Expose
        var addressLine1: String? = null
        @SerializedName("address_line2")
        @Expose
        var addressLine2: String? = null
        @SerializedName("city")
        @Expose
        var city: String? = null
        @SerializedName("state")
        @Expose
        var state: String? = null
        @SerializedName("address_type")
        @Expose
        var addressType: String? = null
        @SerializedName("country")
        @Expose
        var country: String? = null
        @SerializedName("created_at")
        @Expose
        var createdAt: String? = null
        @SerializedName("updated_at")
        @Expose
        var updatedAt: String? = null

    }

}




