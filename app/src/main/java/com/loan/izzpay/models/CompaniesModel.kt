package com.loan.izzpay.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.loan.izzpay.models.CompaniesModel.Datum





class CompaniesModel {

    @SerializedName("status")
    @Expose
    private var status: String? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null
    @SerializedName("data")
    @Expose
    private var data: List<Datum>? = null
    @SerializedName("error")
    @Expose
    private var error: Error? = null

    fun getStatus(): String? {
        return status
    }

    fun setStatus(status: String) {
        this.status = status
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String) {
        this.message = message
    }

    fun getData(): List<Datum>? {
        return data
    }

    fun setData(data: List<Datum>) {
        this.data = data
    }

    fun getError(): Error? {
        return error
    }

    fun setError(error: Error) {
        this.error = error
    }
    inner class Error {

        @SerializedName("status")
        @Expose
        var status: String? = null
        @SerializedName("message")
        @Expose
        var message: String? = null
        @SerializedName("data")
        @Expose
        var data: List<Any>? = null

    }
    inner class Datum {

        @SerializedName("F2CompCode")
        @Expose
        var f2CompCode: String? = null
        @SerializedName("F2CompName")
        @Expose
        var f2CompName: String? = null
        @SerializedName("F2Commperc")
        @Expose
        var f2Commperc: Int? = null

    }
}