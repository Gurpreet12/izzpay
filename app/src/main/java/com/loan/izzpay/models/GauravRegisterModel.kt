package com.loan.izzpay.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import android.R.attr.data
import android.R.attr.data
import com.loan.izzpay.models.GauravRegisterModel.LoginUserData



class GauravRegisterModel {

    @SerializedName("code")
    @Expose
    private var code: Int? = null
    @SerializedName("success")
    @Expose
    private var success: Boolean? = null
    @SerializedName("data")
    @Expose
    private var data: Data? = null
    @SerializedName("message")
    @Expose
    private var message: String? = null

    fun getCode(): Int? {
        return code
    }

    fun setCode(code: Int?) {
        this.code = code
    }

    fun getSuccess(): Boolean? {
        return success
    }

    fun setSuccess(success: Boolean?) {
        this.success = success
    }

    fun getData(): Data? {
        return data
    }

    fun setData(data: Data) {
        this.data = data
    }

    fun getMessage(): String? {
        return message
    }

    public class Headers {
    }
    inner class LoginUserData {

        @SerializedName("userId")
        @Expose
        var userId: Int? = null
        @SerializedName("deviceType")
        @Expose
        var deviceType: String? = null
        @SerializedName("deviceToken")
        @Expose
        var deviceToken: String? = null
        @SerializedName("latitude")
        @Expose
        var latitude: String? = null
        @SerializedName("longitute")
        @Expose
        var longitute: String? = null
        @SerializedName("IEMI")
        @Expose
        var iemi: String? = null

    }

    inner class Original {

        @SerializedName("access_token")
        @Expose
        var accessToken: String? = null
        @SerializedName("token_type")
        @Expose
        var tokenType: String? = null
        @SerializedName("loginUserData")
        @Expose
        var loginUserData: LoginUserData? = null
        @SerializedName("expires_in")
        @Expose
        var expiresIn: Int? = null

    }

    fun setMessage(message: String) {
        this.message = message
    }

    inner class Data {

        @SerializedName("headers")
        @Expose
        var headers: Headers? = null
        @SerializedName("original")
        @Expose
        var original: Original? = null
        @SerializedName("exception")
        @Expose
        var exception: Any? = null

    }
}