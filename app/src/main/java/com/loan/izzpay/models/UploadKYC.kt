package com.loan.izzpay.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UploadKYC {

        @SerializedName("code")
        @Expose
        private var code: Int? = null
        @SerializedName("success")
        @Expose
        private var success: Boolean? = null
        @SerializedName("data")
        @Expose
        private var data: Data? = null
        @SerializedName("message")
        @Expose
        private var message: String? = null

        fun getCode(): Int? {
                return code
        }

        fun setCode(code: Int?) {
                this.code = code
        }

        fun getSuccess(): Boolean? {
                return success
        }

        fun setSuccess(success: Boolean?) {
                this.success = success
        }

        fun getData(): Data? {
                return data
        }

        fun setData(data: Data) {
                this.data = data
        }

        fun getMessage(): String? {
                return message
        }

        fun setMessage(message: String) {
                this.message = message
        }

        inner class Data {

                @SerializedName("image")
                @Expose
                var image: String? = null
                @SerializedName("uid")
                @Expose
                var uid: Int? = null
                @SerializedName("proofType")
                @Expose
                var proofType: String? = null
                @SerializedName("details")
                @Expose
                var details: String? = null

        }
}