package com.loan.izzpay.RetrofitConnections

import androidx.annotation.NonNull
import com.loan.izzpay.dataModels.DataModel
import com.loan.izzpay.models.*
import com.loan.izzpay.pojo.NotificationModelClass
import retrofit2.Call
import retrofit2.http.*
import retrofit2.http.GET
import okhttp3.RequestBody
import okhttp3.MultipartBody
import retrofit2.http.POST
import retrofit2.http.Multipart

interface ApiInterface {

    // This is used for sign up.Before Sign up mobile number need to verify from this api.
    @FormUrlEncoded
    @POST("signup/verification/sendOtpMobile")
    fun sendOTForSignUp(@Field("phoneNo") mobileNo: String): Call<OTPVerifyOnSignUpCase>

    // This is used for sign up and very your mobile.If not otp is verify then this api show wrong otp entered.
    @FormUrlEncoded
    @POST("signup/verification/verifyOTP")
    fun getSignUp(@Field("first_name") first_name: String,
                  @Field("last_name") last_name: String,
                  @Field("deviceType") deviceType: String,
                  @Field("deviceToken") deviceToken: String,
                  @Field("type") type: String,
                  @Field("otp") otp: String,
                  @Field("emailOrPhone") emailOrPhone: String,
                  @Field("IEMI") IEMI: String,
                  @Field("latitude") latitude: String,
                  @Field("longitute") longitute: String,
                  @Field("email") email: String,
                  @Field("gender") gender: String,
                  @Field("date_of_birth") date_of_birth: String): Call<GauravRegisterModel>

    // this api is login for send otp

    @FormUrlEncoded
    @POST("user/login")
    fun loginOTPSendForSignIn(@Field("emailOrPhone") mobileNo: String): Call<OTPVerifyOnSignUpCase>



    // this api verify the login otp verification
    @FormUrlEncoded
    @POST("user/login/verify")
    fun loginOTPVerifydForSignIn(@Field("emailOrPhone") mobileNo: String,
                                 @Field("type") type: String,
                                 @Field("deviceType") deviceType: String,
                                 @Field("deviceToken") deviceToken: String,
                                 @Field("IEMI") IEMI: String,
                                 @Field("latitude") latitude: String,
                                 @Field("longitute") longitute: String,
                                 @Field("otp") otp: String): Call<GauravRegisterModel>


    // This api is used for logout because need to refresh token and notification token
    @POST("user/logout")
    fun Logout(@Header("Authorization") apiKey: String): Call<DefaultModel>

    // This api used for loan types
    @GET("view/viewLoanCategories")
    fun listOfLoans(@Header("Authorization") apiKey: String): Call<ListOfLoansModel>

    // This api used for loan types
    @GET("user/showNotification")
    fun notifications(@Header("Authorization") apiKey: String): Call<NotificationModelClass>


    // This api is get Profile
    @POST("user/dashboard")
    fun dashBoardProfile(@Header("Authorization") apiKey: String): Call<ProfileModel>


    // This is working api
    @POST("user/editProfile")
    fun updateUserProfile(@Header("Authorization") apiKey: String,@Query("first_name") first_name: String, @Query("last_name") last_name: String,
                          @Query("fatherName") fatherName: String, @Query("motherName") motherName: String,
                          @Query("date_of_birth") date_of_birth: String, @Query("gender") gender: String,
                          @Query("martialStatus") martialStatus: String, @Query("highestEducation") highestEducation: String,
                          @Query("phone") mobile: String, @Query("email") email: String): Call<ProfileModel>


    // This is working api
    @POST("user/editProfile")
    fun mUpdateProfessionUserProfile(@Header("Authorization") apiKey: String,@Query("profession") profession: String, @Query("designation") designation: String,
                                     @Query("companyName") companyName: String,
                                     @Query("office_area") office_area: String,
                                     @Query("office_city") office_city: String,
                                     @Query("office_pincode") office_pincode: String,
                                     @Query("monthOfExperienced") monthOfExperienced: String,
                                     @Query("totalExperience") totalExperience: String,
                                     @Query("montlyIncome") montlyIncome: String): Call<ProfileModel>

    // This is working api
    @POST("add/Address")
    fun mUpdateAddressUserProfile(@Header("Authorization") apiKey: String, @Query("address_line1") address_line1: String, @Query("address_line2") address_line2: String,
                                     @Query("city") city: String,
                                     @Query("state") state: String,
                                     @Query("country") country: String,
                                     @Query("address_type") address_type: String,
                                     @Query("pincode") pincode: String): Call<DataModelLogin>



    // This api used for loan types
    @GET("getAll/Address")
    fun addressAll(@Header("Authorization") apiKey: String): Call<ModelGetAllAddress>

    // This api used for edit your profile
    @Multipart
    @NonNull
    @POST("user/editProfile")
    fun uploadUserProfile(@HeaderMap token: Map<String,String>, @Part file:MultipartBody.Part, @Part("first_name") firstName: RequestBody):Call<ProfileModel>


    // This api used for upload KYC Documents all
    @Multipart
    @NonNull
    @POST("addProofByUid")
    fun addProofs(@HeaderMap token: Map<String,String>, @Part("proofType") proofType: RequestBody, @Part("details") details: RequestBody, @Part file:MultipartBody.Part):Call<UploadKYC>


    @GET("showAllProofByUid")
    fun showKYCProofsAndCheck(@Header("Authorization") apiKey: String): Call<ModelKYCDocumentShow>

//    @Multipart
//    @POST("user/editProfile")
//    fun uploadData(@HeaderMap token: Map<String,String>, @Part file:MultipartBody.Part, @Part("first_name") firstName: RequestBody):Call<Mydata>


    @Multipart
    @POST("user/editProfile")
    fun uploadImage(@HeaderMap token: Map<String,String>, @Part file:MultipartBody.Part):Call<Mydata>



//    @Multipart
//    @POST("user/editProfile")
//    fun uploadUserProfile(@Header("Authorization") apiKey: String,@Part("first_name") first_name: RequestBody,@Part file: MultipartBody.Part): Call<ProfileModel>


    @Multipart
    @POST("addVerificationProof")
    fun uploadAadhar(@Part("uid") uid: RequestBody, @Part("proofType") proofType: RequestBody, @Part("details") details: RequestBody, @Part file: MultipartBody.Part): Call<AadharUploadModel>


    @GET("showList/InstitutionType")
    fun institutesApi():Call<InstitutesModel>


    @FormUrlEncoded
    @POST("showList/Institutions")
    fun allInstitutes(@Field("uniTypeId") uniTypeId:Int): Call<ModelsAllInstitutes>

//    @POST("user/dashboard")
//    fun getDataTokenChart(@Header("Authorization") apiKey: String,@Query("email_id") email_id: String,@Query("password") password: String): Call<DashBoardProfile>
//
//
//    @POST("user/dashboard")
//    fun dashBoardProfile(@Header("Authorization") apiKey: String): Call<DashBoardProfile>




}