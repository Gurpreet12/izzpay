package com.loan.izzpay.RetrofitConnections;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient_ {

    public   static Retrofit retrofit = null;

    //  (https:/gmt-technologies.co.uk/)  THIS IS BASE URL FOR ALL API

    //    private static String Base_Url="https:/gmt-technologies.co.uk/";
    private static String Base_Url="http://sandbox.izz-pay.com/api/auth/";


    @RequiresApi(api = Build.VERSION_CODES.N)
    public static Retrofit getClient() {

        if (retrofit == null) {
          /*  Dispatcher dispatcher = new Dispatcher();
            dispatcher.setMaxRequests(1);
            dispatcher.cancelAll();*/
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    //     .dispatcher(dispatcher)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60,TimeUnit.MINUTES)
                    .readTimeout(120, TimeUnit.SECONDS)
                    .addInterceptor(interceptor)
                    .build();
       /*     Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();*/

            retrofit = new Retrofit.Builder()
                    .baseUrl(Base_Url).client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
