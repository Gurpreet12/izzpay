package com.loan.izzpay.activities

import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputFilter
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatSpinner
import androidx.constraintlayout.widget.ConstraintLayout
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.snackbar.Snackbar
import com.loan.izzpay.R
import com.loan.izzpay.utils.Constant
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class ActivityCustomerFormMandatory : AppCompatActivity() {

    private lateinit var dob: EditText
    private lateinit var aadhar: EditText
    private lateinit var pan: EditText
    private lateinit var driving_lic_no: EditText
    private lateinit var voter_id: EditText
    private lateinit var passport_no: EditText
    private lateinit var state: AutoCompleteTextView
    private lateinit var city: AutoCompleteTextView
    private lateinit var pincode: EditText
    private lateinit var permanent_address: EditText
    private lateinit var current_address: EditText
    private lateinit var gender: EditText
    private lateinit var exp_year: AppCompatSpinner
    private lateinit var exp_months: AppCompatSpinner
    private lateinit var employment_type: EditText
    private lateinit var desigation: EditText
    private lateinit var company_name: EditText
    private lateinit var monthly_salary: EditText
    private lateinit var checkbox: CheckBox
    private lateinit var Submit: Button

    var listSpinner = ArrayList<String>()
    var listAll = ArrayList<String>()
    var listState = ArrayList<String>()
    var listCity = ArrayList<String>()
    var act: AutoCompleteTextView? = null
    private lateinit var constraint: ConstraintLayout


    val c = Calendar.getInstance()
    val year = c.get(Calendar.YEAR)
    val month = c.get(Calendar.MONTH)
    val day = c.get(Calendar.DAY_OF_MONTH)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer_form_mandatory)

        dob = findViewById(R.id.form_dob)
        aadhar = findViewById(R.id.form_adhar)
        pan = findViewById(R.id.form_pan)
        driving_lic_no = findViewById(R.id.form_driving_lic)
        voter_id = findViewById(R.id.form_voter_id)
        passport_no = findViewById(R.id.form_passport_no)
        state = findViewById(R.id.form_state)
        city = findViewById(R.id.form_city)
        pincode = findViewById(R.id.form_pincode)
        permanent_address = findViewById(R.id.form_p_address)
        current_address = findViewById(R.id.form_c_address)
        gender = findViewById(R.id.form_gender)
        exp_year = findViewById(R.id.form_experience_yr)
        exp_months = findViewById(R.id.form_experience_months)
        employment_type = findViewById(R.id.form_employment_type)
        desigation = findViewById(R.id.form_designation)
        company_name = findViewById(R.id.form_employer_name)
        monthly_salary = findViewById(R.id.form_monthly_salary)
        checkbox = findViewById(R.id.same)
        //coordinate = findViewById(R.id.coordinate)
        constraint = findViewById(R.id.constraint_layout)
        driving_lic_no.filters = arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(15))
        pan.filters = arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(10))
        passport_no.filters = arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(8))
        voter_id.filters = arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(10))


        val get_dob = dob.text.toString()
        val get_aadhar = aadhar.text.toString()
        val get_pan = pan.text.toString()
        val get_driving_lic = driving_lic_no.text.toString()
        val get_voter_id = voter_id.text.toString()
        val get_passport_no = passport_no.text.toString()
        val get_state = state.text.toString()
        val get_city = city.text.toString()
        val get_pincode = pincode.text.toString()
        val get_permanent_add = permanent_address.text.toString()
        val get_current_add = current_address.text.toString()
        val get_gender = gender.text.toString()
        val get_emplyo_type = employment_type.text.toString()
        val get_designation = desigation.text.toString()
        val get_company_name = company_name.text.toString()
        val get_monthly_salary = monthly_salary.text.toString()
        Submit = findViewById(R.id.submit_btn)


        checkbox.setOnClickListener {
            if (checkbox.isChecked) {
                val get_permanent_add = permanent_address.text.toString()
                current_address.setText(get_permanent_add)
            }
        }


        dob.setOnClickListener {

            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, month, day ->
                // Display Selected date in TextView
                dob.setText("" + year + "-" + (month + 1) + "-" + day)
            }, year, month, day)
            dpd.show()

        }



        gender.setOnClickListener {
            select_gender()
        }
        employment_type.setOnClickListener {
            select_employment_type()
        }
        desigation.setOnClickListener {
            select_designation()
        }


        Submit.setOnClickListener {

        }
    }

    private fun select_gender() {
        val inflater = LayoutInflater.from(this)
        val builderView =
                inflater!!.inflate(R.layout.select_gender, null);

        val builder = AlertDialog.Builder(this)
        builder.setView(builderView);
        val alert = builder.create();
        alert.show()

        val male = builderView.findViewById(R.id.gender_text1) as TextView
        val female = builderView.findViewById(R.id.gender_text2) as TextView
        val other = builderView.findViewById(R.id.gender_text3) as TextView


        male.setOnClickListener {
            alert.hide()
            gender.setText("Male")
        }
        female.setOnClickListener {
            alert.hide()
            gender.setText("Female")
        }
        other.setOnClickListener {
            alert.hide()
            gender.setText("Other")
        }
    }

    private fun select_employment_type() {
        val inflater = LayoutInflater.from(this)
        val builderView =
                inflater!!.inflate(R.layout.select_employement, null);

        val builder = AlertDialog.Builder(this);
        builder.setView(builderView);
        val alert = builder.create();
        alert.show()

        val salaried =
                builderView.findViewById(R.id.occupation_text1) as TextView
        val self_employed = builderView.findViewById(R.id.occupation_text2) as TextView


        salaried.setOnClickListener {
            alert.hide()
            employment_type.setText("Salaried")


        }
        self_employed.setOnClickListener {
            alert.hide()
            employment_type.setText("Self-employed")
        }
    }

    private fun select_designation() {
        val inflater = LayoutInflater.from(this)
        val builderView =
                inflater!!.inflate(R.layout.select_designation, null);

        val builder = AlertDialog.Builder(this);
        builder.setView(builderView);
        val alert = builder.create();
        alert.show()

        val manager =
                builderView.findViewById(R.id.designation_manager) as TextView
        val senior = builderView.findViewById(R.id.designation_senior) as TextView
        val junior = builderView.findViewById(R.id.designation_junior) as TextView



        manager.setOnClickListener {
            alert.hide()
            desigation.setText("Manager")


        }
        senior.setOnClickListener {
            alert.hide()
            desigation.setText("Senior")


        }
        junior.setOnClickListener {
            alert.hide()
            desigation.setText("Junior")
        }
    }
}
