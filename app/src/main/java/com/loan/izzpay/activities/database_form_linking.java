package com.loan.izzpay.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class database_form_linking extends AsyncTask<String, Void, String> {


    Context context;
    /**
     * progress dialog to show user that the backup is processing.
     */
    private ProgressDialog dialog;


        database_form_linking(Context ctx) {
        context = ctx;
    }

    @Override
    protected String doInBackground(String... strings) {
        {
            String type = strings[0];
            String login_url = "https://izz-pay.com/izzpay-web/mvc/api/cccNumber";
            if (type.equals("insert")) {
                try {
                    String user_first_name = strings[1];
                    String user_last_name = strings[2];
                    String user_phone = strings[3];
                    String user_email = strings[4];
                    String user_dob = strings[5];
                   // String user_aadhar = strings[6];
                    String user_pan = strings[6];
                  //  String user_driving_lic = strings[8];
                   // String user_voter_id = strings[9];
                   // String user_passport_no = strings[10];
                    String user_state = strings[7];
                    String user_city = strings[8];
                    String user_pincode = strings[9];
                    String user_permanent_add = strings[10];
                    String user_current_add = strings[11];
                    String user_gender = strings[12];
                  //  String user_exp_yrs = strings[17];
                   // String user_exp_months = strings[18];
                    String user_emplyo_type = strings[13];
                    String user_company_name = strings[14];
                   // String user_designation = strings[21];
                    String user_monthly_salary = strings[15];
                    URL url = new URL(login_url);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                 String post_data = URLEncoder.encode("first_name", "UTF-8") + "=" + URLEncoder.encode(user_first_name, "UTF-8") + "&" +
                         URLEncoder.encode("last_name", "UTF-8") + "=" + URLEncoder.encode(user_last_name, "UTF-8") + "&" +
                            URLEncoder.encode("mobile", "UTF-8") + "=" + URLEncoder.encode(user_phone, "UTF-8") + "&" +
                            URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(user_email, "UTF-8") + "&" +
                            URLEncoder.encode("date_of_birth", "UTF-8") + "=" + URLEncoder.encode(user_dob, "UTF-8") + "&" +
                            //URLEncoder.encode("aadhar", "UTF-8") + "=" + URLEncoder.encode(user_aadhar, "UTF-8") + "&" +
                            URLEncoder.encode("pan_no", "UTF-8") + "=" + URLEncoder.encode(user_pan, "UTF-8") + "&" +
                           // URLEncoder.encode("drv_licence_no", "UTF-8") + "=" + URLEncoder.encode(user_driving_lic, "UTF-8")+"&" +
                          //  URLEncoder.encode("voter_id", "UTF-8") + "=" + URLEncoder.encode(user_voter_id, "UTF-8")+"&" +
                          //  URLEncoder.encode("passport_no", "UTF-8") + "=" + URLEncoder.encode(user_passport_no, "UTF-8")+"&" +
                            URLEncoder.encode("state", "UTF-8") + "=" + URLEncoder.encode(user_state, "UTF-8")+"&" +
                            URLEncoder.encode("city", "UTF-8") + "=" + URLEncoder.encode(user_city, "UTF-8")+"&" +
                            URLEncoder.encode("pincode", "UTF-8") + "=" + URLEncoder.encode(user_pincode, "UTF-8")+"&" +
                            URLEncoder.encode("address_line1", "UTF-8") + "=" + URLEncoder.encode(user_permanent_add, "UTF-8")+"&" +
                            URLEncoder.encode("address_line2", "UTF-8") + "=" + URLEncoder.encode(user_current_add, "UTF-8")+"&" +
                            URLEncoder.encode("gender", "UTF-8") + "=" + URLEncoder.encode(user_gender, "UTF-8")+"&" +
                         //   URLEncoder.encode("total_exp", "UTF-8") + "=" + URLEncoder.encode(user_exp_yrs, "UTF-8")+"&" +
                          //  URLEncoder.encode("total_exp1", "UTF-8") + "=" + URLEncoder.encode(user_exp_months, "UTF-8")+"&" +
                            URLEncoder.encode("applicant_constitution", "UTF-8") + "=" + URLEncoder.encode(user_emplyo_type, "UTF-8")+"&" +
                            URLEncoder.encode("vendor_name", "UTF-8") + "=" + URLEncoder.encode(user_company_name, "UTF-8")+"&" +
                          //  URLEncoder.encode("address_type", "UTF-8") + "=" + URLEncoder.encode(user_designation, "UTF-8")+"&" +
                         URLEncoder.encode("journey", "UTF-8") + "=" + URLEncoder.encode("OpenMarketPl", "UTF-8")+"&" +
                         URLEncoder.encode("address_type", "UTF-8") + "=" + URLEncoder.encode("RESI", "UTF-8")+"&" +
                         URLEncoder.encode("request_id", "UTF-8") + "=" + URLEncoder.encode("887893", "UTF-8")+"&" +
                         URLEncoder.encode("requested_loan_amount", "UTF-8") + "=" + URLEncoder.encode("100000", "UTF-8")+"&" +
                         URLEncoder.encode("customer_id", "UTF-8") + "=" + URLEncoder.encode(user_monthly_salary, "UTF-8");
                    bufferedWriter.write(post_data);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                    String result = "";
                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) {
                        result += line;
                        Log.d("Api result",result);
                    }
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();
                    return result;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }


    }

    @Override
    protected void onPreExecute() {
        dialog = new ProgressDialog(context);
        dialog.setTitle("IZZPAY");
        dialog.setCancelable(false);
        dialog.setMessage("PLease wait, don't press back");
    }

    protected void onPostExecute(String result) {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }
}