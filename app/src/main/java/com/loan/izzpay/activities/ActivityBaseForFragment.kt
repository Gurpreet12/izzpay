package com.loan.izzpay.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.loan.izzpay.fragments.*
import com.loan.izzpay.R

import android.net.Uri
import com.loan.izzpay.activities.UploadSelfAadharCardImage.ActivitySelfAadharPicUpload


class ActivityBaseForFragment : AppCompatActivity() {

    private val sharedPrefDB = "izzpay_db"
    lateinit var sharedPreferences: SharedPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base_fragment)

        sharedPreferences = this.getSharedPreferences(sharedPrefDB, Context.MODE_PRIVATE)

        val profileName = intent.getStringExtra("data_desciption")
        val url_of_image = intent.getStringExtra("image_url")
        val bundle = Bundle()


        bundle.putString("desc", profileName)
        bundle.putString("url_of_image", url_of_image)



        if (savedInstanceState == null) {
            val transcation = supportFragmentManager.beginTransaction()
            transcation.isAddToBackStackAllowed

            val checkPage = intent.extras!!.getString("checkPage")
            val key_1 = intent.extras!!.getString("key_1")
            val TitleLoan = intent.extras!!.getString("TitleLoan")
            val Description = intent.getStringExtra("Description")
            val url_of_image = intent.getStringExtra("Image")


            val bundle = Bundle()
            bundle.putString("TitleLoan", TitleLoan)
            bundle.putString("desc", Description)
            bundle.putString("url_of_image", url_of_image)


            if (checkPage!!.equals("types")) {
                val fragmentAddMoneyToUserAccount = FragmentTypeOfLoans()
                transcation.replace(R.id.frame_layout_base_fragment, fragmentAddMoneyToUserAccount)
                transcation.commit()
            }
            if (checkPage!!.equals("instant_cash")) {
                val fragmentInstantCashScreen = FragmentInstantCashScreen()
                transcation.replace(R.id.frame_layout_base_fragment, fragmentInstantCashScreen)
                transcation.commit()
            }
            if (checkPage!!.equals("apply_loan")) {
                val fragmentLoanForm = FragmentLoanForm()
                transcation.replace(R.id.frame_layout_base_fragment, fragmentLoanForm)
                transcation.commit()
            }
            if (checkPage!!.equals("credit_score")) {
                val fragmentCreditScore = FragmentCreditScore()
                transcation.replace(R.id.frame_layout_base_fragment, fragmentCreditScore)
                transcation.commit()
            }
            if (checkPage!!.equals("bank_details")) {
                val fragmentBankdetail = FragmentBankdetail()
                transcation.replace(R.id.frame_layout_base_fragment, fragmentBankdetail)
                transcation.commit()
            }
            if (checkPage.equals("marriage_loan")) {
                val fragment_marriage_loan = FragmentMarriageLoan()
                transcation.replace(R.id.frame_layout_base_fragment, fragment_marriage_loan)
                transcation.commit()
            }
            if (checkPage.equals("home_loan")) {
                val fragment_home_loan = FragmenHomeLoan()
                transcation.replace(R.id.frame_layout_base_fragment, fragment_home_loan)
                transcation.commit()
            }
            if (checkPage.equals("car_loan")) {
                val fragment_car_loan = FragmentCarLoan()
                transcation.replace(R.id.frame_layout_base_fragment, fragment_car_loan)
                transcation.commit()
            }
            if (checkPage.equals("personal_loan")) {
                val fragment_personal_loan = FragmentPersonalLoan()
                transcation.replace(R.id.frame_layout_base_fragment, fragment_personal_loan)
                transcation.commit()
            }
            if (checkPage.equals("business_loan")) {
                val fragment_business_loan = FragmentBusinessLoan()
                transcation.replace(R.id.frame_layout_base_fragment, fragment_business_loan)
                transcation.commit()
            }
            if (checkPage.equals("profile")) {
                val fragmentProfile = FragmentMainProfile()
                transcation.replace(R.id.frame_layout_base_fragment, fragmentProfile)
                transcation.commit()
            }
            if (checkPage.equals("edit_profile")) {
                val fragment_edit_profie = FragmentPersonalProfile()
                transcation.replace(R.id.frame_layout_base_fragment, fragment_edit_profie)
                transcation.commit()
            }
            if (checkPage.equals("calcultor")) {
                val fragment_calculator = FragmentCalculator()
                transcation.replace(R.id.frame_layout_base_fragment, fragment_calculator)
                transcation.commit()
            }
            if (checkPage.equals("your_limit")) {
                val fragment_your_limit = FragmentLoanStatementDetails()
                transcation.replace(R.id.frame_layout_base_fragment, fragment_your_limit)
                transcation.commit()
            }
            if (checkPage.equals("description")) {
                val fragment_residental = FragmentLoanStatementDetails()
                transcation.replace(R.id.frame_layout_base_fragment, fragment_residental)
                transcation.commit()
            }
            if (checkPage.equals("home_loan")) {
                val fragment_home_loan = FragmenHomeLoan()
                transcation.replace(R.id.frame_layout_base_fragment, fragment_home_loan)
                transcation.commit()
            }
            if (checkPage.equals("instututes")) {
                val fragmentInstitute = FragmentInstituteOrSchoolsOrUnversity()
                transcation.replace(R.id.frame_layout_base_fragment, fragmentInstitute)
                transcation.commit()
            }
            if (checkPage.equals("smart_repay")) {
                val fragment_smart_repay = FragmentSmartRepay()
                transcation.replace(R.id.frame_layout_base_fragment, fragment_smart_repay)
                transcation.commit()
            }
            if (checkPage.equals("loan_statement")) {
                val fragment_loan_statement = FragmentLoanStatement()
                transcation.replace(R.id.frame_layout_base_fragment, fragment_loan_statement)
                transcation.commit()
            }
            if (checkPage.equals("eligible")) {
                val fragment_smart_repay = FragmentEligibility()
                transcation.replace(R.id.frame_layout_base_fragment, fragment_smart_repay)
                transcation.commit()
            }
            if (checkPage.equals("rate_us")) {
                val rateIntent = Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + application.getPackageName()))
                startActivity(rateIntent)

            }
            if (checkPage.equals("about_us")) {
                val fragment_about_us = FragmentAboutUs()
                transcation.replace(R.id.frame_layout_base_fragment, fragment_about_us)
                transcation.commit()
            }
            if (checkPage.equals("logout")) {

                val editor: SharedPreferences.Editor = sharedPreferences.edit()
                editor.putString("login", "0")
                editor.apply()
                editor.commit()

                val intent = Intent(applicationContext, ActivityStart::class.java)
                startActivity(intent)
                finish()
            }
            if (checkPage.equals("faq")) {
                val fragment_faq = FragmentFaq()
                transcation.replace(R.id.frame_layout_base_fragment, fragment_faq)
                transcation.commit()
            }
            if (checkPage.equals("all_institutes")) {
                val fragmentInstitutes = FragmentInstitutes()
                transcation.replace(R.id.frame_layout_base_fragment, fragmentInstitutes)
                transcation.commit()
            }
            if (checkPage.equals("refer_earn")) {
                val fragment_refer_earn = FragmentReferEarn()
                transcation.replace(R.id.frame_layout_base_fragment, fragment_refer_earn)
                transcation.commit()
            }
            if (checkPage.equals("notification_bell")) {
                val fragment_notification_bell = FragmentNotificationBell()
                transcation.replace(R.id.frame_layout_base_fragment, fragment_notification_bell)
                transcation.commit()
            }
            if (checkPage.equals("proof_self")) {
                val i = Intent(this@ActivityBaseForFragment, ActivityImageUpload::class.java)
                startActivity(i)
                finish()
            }
            if (checkPage.equals("proof_aadhar")) {
                val i = Intent(this@ActivityBaseForFragment, ActivitySelfAadharPicUpload::class.java)
                i.putExtra("TypeOfProof", "Aadhar")
                startActivity(i)
                finish()

            }
            if (checkPage.equals("proof_pancard")) {
                val i = Intent(this@ActivityBaseForFragment, ActivitySelfAadharPicUpload::class.java)
                i.putExtra("TypeOfProof", "PanCard")
                startActivity(i)
                finish()
            }
            if (checkPage.equals("proof_identityCard")) {
                val i = Intent(this@ActivityBaseForFragment, ActivitySelfAadharPicUpload::class.java)
                i.putExtra("TypeOfProof", "IdentityCard")
                startActivity(i)
                finish()
            }
            if (checkPage.equals("proof_icard")) {
                val fragment_id_card = FragmentIdCard()
                transcation.replace(R.id.frame_layout_base_fragment, fragment_id_card)
                transcation.commit()
            }
            if (checkPage.equals("proof_bank_statement")) {
                val fragment_bank_statement = FragmentBankStatement()
                transcation.replace(R.id.frame_layout_base_fragment, fragment_bank_statement)
                transcation.commit()
            }
            if (checkPage.equals("proof_salary_slip")) {
                val fragment_salary_slip = FragmentSalarySlip()
                transcation.replace(R.id.frame_layout_base_fragment, fragment_salary_slip)
                transcation.commit()
            }
            if (checkPage.equals("proof_residental")) {
                val fragment_residental = FragmentResidentalProof()
                transcation.replace(R.id.frame_layout_base_fragment, fragment_residental)
                transcation.commit()
            }
            if (checkPage.equals("contact_us")) {
                val fragmentContactUs = FragmentContactUs()
                transcation.replace(R.id.frame_layout_base_fragment, fragmentContactUs)
                transcation.commit()
            }
        }

    }
}