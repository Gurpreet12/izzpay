package com.loan.izzpay.activities

import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.loan.izzpay.R
import com.loan.izzpay.RetrofitConnections.ApiClient
import com.loan.izzpay.models.GauravRegisterModel
import retrofit2.Call
import retrofit2.Callback
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.loan.izzpay.models.OTPVerifyOnSignUpCase
import android.os.CountDownTimer
import android.text.TextUtils
import android.view.MenuItem
import java.util.*
import android.view.inputmethod.InputMethodManager
import java.lang.Exception
import java.util.concurrent.TimeUnit
import android.Manifest
import android.annotation.SuppressLint

import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager

import android.os.Looper
import android.provider.Settings
import android.telephony.TelephonyManager
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.*
import com.loan.izzpay.utils.Constant


class GauravActivityRegister : AppCompatActivity() {

    lateinit var first_name_edt: EditText
    lateinit var last_name_edt: EditText
    lateinit var telephone_edt: EditText
    lateinit var gmail_edt: EditText
    lateinit var date_of_birth_edt: EditText
    lateinit var gender_edt: EditText
    lateinit var submitRl: RelativeLayout
    lateinit var sign_back_arrow: ImageButton
    lateinit var latTextView: TextView
    lateinit var lonTextView: TextView

    lateinit var progerssProgressDialog: ProgressDialog
    var dataList: GauravRegisterModel? = null
    var verifyMobileNoModel: OTPVerifyOnSignUpCase? = null
    lateinit var sharedPreferences: SharedPreferences

    private val sharedPrefDB = "izzpay_db"

    var counter: Int = 0
    var radio = null
    lateinit var resend_otp_tv: TextView

    val PERMISSION_ID = 42
    lateinit var mFusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gaurav_register)

        mInit()

        mListeners()

        date_of_birth_edt.setOnClickListener {

            val cal = Calendar.getInstance()
            val y = cal.get(Calendar.YEAR)
            val m = cal.get(Calendar.MONTH)
            val d = cal.get(Calendar.DAY_OF_MONTH)


            val datepickerdialog: DatePickerDialog = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                date_of_birth_edt.setText("" + dayOfMonth + "-" + monthOfYear + "-" + year)

            }, y, m, d)
            datepickerdialog.datePicker.maxDate = cal.timeInMillis
            datepickerdialog.show()
        }


        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        getLastLocation()
    }

    private fun mInit() {

        first_name_edt = findViewById(R.id.first_name_edt)
        last_name_edt = findViewById(R.id.last_name_edt)
        telephone_edt = findViewById(R.id.telephone_edt)
        gmail_edt = findViewById(R.id.gmail_edt)
        date_of_birth_edt = findViewById(R.id.date_of_birth_edt)
        gender_edt = findViewById(R.id.gender_edt)
        submitRl = findViewById(R.id.submitRl)
        sign_back_arrow = findViewById(R.id.sign_back_arrow)
        latTextView = findViewById(R.id.latTextView)
        lonTextView = findViewById(R.id.lonTextView)

        sharedPreferences = this.getSharedPreferences(sharedPrefDB, Context.MODE_PRIVATE)

        progerssProgressDialog = ProgressDialog(this)
        progerssProgressDialog.setTitle("Please wait...")
        progerssProgressDialog.setCancelable(false)


    }

    fun String.isEmailValid(): Boolean {
        return !TextUtils.isEmpty(this) && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
    }

    private fun mListeners() {


        gender_edt.setOnClickListener {
            val popup = PopupMenu(this@GauravActivityRegister, gender_edt)
            popup.getMenuInflater()
                    .inflate(R.menu.gender, popup.getMenu())
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    gender_edt.setText(item.getTitle())
                    return true
                }
            })
            popup.show()
        }

        sign_back_arrow.setOnClickListener {

            finish()
        }



        submitRl.setOnClickListener {

            val permission = ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_PHONE_STATE)

            if (permission != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_PHONE_STATE), 1)
            } else {

            }

            val tel = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager


            if (!first_name_edt.text.toString().isEmpty()) {

                first_name_edt.setError(null)

                if (!last_name_edt.text.toString().isEmpty()) {

                    last_name_edt.setError(null)

                    if (!telephone_edt.text.toString().isEmpty()) {


                        if (telephone_edt.text.toString().length == 10) {

                            telephone_edt.setError(null)

                            if (!gmail_edt.text.toString().isEmpty()) {

                                gmail_edt.setError(null)
                                if (!date_of_birth_edt.text.toString().isEmpty()) {

                                    date_of_birth_edt.setError(null)

                                    if (!gender_edt.text.toString().isEmpty()) {

                                        gender_edt.setError(null)

                                        if (permission != PackageManager.PERMISSION_GRANTED) {
                                            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_PHONE_STATE), 1)
                                            Toast.makeText(applicationContext, "IMEI number is not found", Toast.LENGTH_SHORT).show()
                                        } else {
                                            val s1 = tel.deviceId

                                            if (s1.isEmpty()) {
                                                Toast.makeText(applicationContext, "IMEI number is not found", Toast.LENGTH_SHORT).show()

                                            } else {

                                                val lat = latTextView.text.toString()
                                                val lng = lonTextView.text.toString()
                                                
                                                if (latTextView.text.toString().equals("Latitude: ") && lonTextView.text.toString().equals("Longitude: ")) {
                                                    Toast.makeText(applicationContext, "Location is not found", Toast.LENGTH_SHORT).show()

                                                } else {
                                                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                                                    imm!!.hideSoftInputFromWindow(getCurrentFocus()!!.getWindowToken(), 0)
                                                    apiSignUpFirstSendOTPVerifyForPhone(telephone_edt.text.toString())
                                                }
                                            }
                                        }


                                    } else {
                                        gender_edt.setError("enter your gendere")
                                    }
                                } else {
                                    date_of_birth_edt.setError("enter your date of birth")
                                }
                            } else {
                                gmail_edt.setError("enter your email")
                            }
                        } else {
                            telephone_edt.setError("enter 10 digits mobile no")
                        }
                    } else {
                        telephone_edt.setError("enter your phone no")
                    }
                } else {
                    last_name_edt.setError("enter your last name")
                }
            } else {
                first_name_edt.setError("enter your first name")
            }

        }
    }


    private fun apiSignUpFirstSendOTPVerifyForPhone(strPhone: String) {
        if (Constant.hasNetworkAvailable(applicationContext)) {

            progerssProgressDialog.show()

            try {
                val call: Call<OTPVerifyOnSignUpCase> = ApiClient.getClient.sendOTForSignUp(strPhone)
                call.enqueue(object : Callback<OTPVerifyOnSignUpCase> {
                    override fun onFailure(call: Call<OTPVerifyOnSignUpCase>, t: Throwable) {
                        progerssProgressDialog.dismiss()
                        Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<OTPVerifyOnSignUpCase>, response: retrofit2.Response<OTPVerifyOnSignUpCase>) {

                        progerssProgressDialog.dismiss()

                        try {
                            if (response.code() == 409) {
                                verifyMobileNoModel = response.body()
                                Toast.makeText(applicationContext, verifyMobileNoModel!!.getMessage().toString(), Toast.LENGTH_SHORT).show()
                            } else if (response.code() == 200) {

                                verifyMobileNoModel = response.body()


                                if (verifyMobileNoModel!!.getSuccess() == true) {
                                    if (verifyMobileNoModel != null) {
                                        Toast.makeText(applicationContext, verifyMobileNoModel!!.getMessage(), Toast.LENGTH_SHORT).show()
                                        dilaogVerifyOTPForSignUp(strPhone)

                                    } else {
                                        Toast.makeText(applicationContext, "No data found", Toast.LENGTH_SHORT).show()
                                    }
                                } else {
                                    Toast.makeText(applicationContext, verifyMobileNoModel!!.getMessage().toString(), Toast.LENGTH_SHORT).show()
                                }

                            }
                        } catch (exception: Exception) {
                            Toast.makeText(applicationContext, exception.message, Toast.LENGTH_SHORT).show()
                        }
                    }

                })

            } catch (ex: java.lang.Exception) {
                Toast.makeText(this, ex.message.toString(), Toast.LENGTH_SHORT).show()
            }

        } else {
            Toast.makeText(this, "No network available!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun dilaogVerifyOTPForSignUp(phone: String) {


        val dialogBuilder = AlertDialog.Builder(this)
        val layoutView = layoutInflater.inflate(R.layout.mobile_verify_user_on_sign_up, null)
        val verifyMobileBtn: Button = layoutView.findViewById(R.id.verifyMobileBtn)
        val otpEdt: EditText = layoutView.findViewById(R.id.otpEdt)
        val back_arrow_imgBtn: ImageButton = layoutView.findViewById(R.id.back_arrow_imgBtn)
        resend_otp_tv = layoutView.findViewById(R.id.resend_otp_tv)
        dialogBuilder.setView(layoutView)
        val alertDialog = dialogBuilder.create()
        alertDialog.getWindow()!!.getAttributes().windowAnimations = R.style.DialogAnimation
        alertDialog.show()

        resend_otp_tv.isEnabled = false
        resend_otp_tv.setOnClickListener {
            Toast.makeText(applicationContext, "sorry", Toast.LENGTH_SHORT).show()
        }
        val timer = object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val text = String.format(Locale.getDefault(), "%02d : %02d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % 60,
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60)
                resend_otp_tv.setText(text)
            }

            override fun onFinish() {
                resend_otp_tv.setText("Resend")
                resend_otp_tv.isEnabled = true

                resend_otp_tv.setOnClickListener {
                    alertDialog.dismiss()
                    apiSignUpFirstSendOTPVerifyForPhone(phone)

                }

            }

        }
        timer.start()

        back_arrow_imgBtn.setOnClickListener {
            alertDialog.dismiss()
        }

        verifyMobileBtn.setOnClickListener {
            if (otpEdt.text.isEmpty()) {
                otpEdt.setError("Enter your otp")
            } else {
                mSignUp(otpEdt.text.toString(), first_name_edt.text.toString(), last_name_edt.text.toString(), gmail_edt.text.toString(), telephone_edt.text.toString(), gender_edt.text.toString(), date_of_birth_edt.text.toString())
            }

        }
    }

    private fun mSignUp(otp: String, first_name: String, last_name: String, email: String, mobile: String, gender: String, dob: String) {

        if (Constant.hasNetworkAvailable(applicationContext)) {

            progerssProgressDialog.show()

            try {
                val call: Call<GauravRegisterModel> = ApiClient.getClient.getSignUp(first_name, last_name, "Android", "2476J3RJBFEFN ", "Phone", otp, mobile, "IMEI", "xxxxxx", "xxxxxxx", email, gender, dob)
                call.enqueue(object : Callback<GauravRegisterModel> {
                    override fun onFailure(call: Call<GauravRegisterModel>, t: Throwable) {
                        progerssProgressDialog.dismiss()
                        Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<GauravRegisterModel>, response: retrofit2.Response<GauravRegisterModel>) {

                        progerssProgressDialog.dismiss()

                        try {

                            if (response.code() == 409) {
                                dataList = response.body()
                                Toast.makeText(applicationContext, dataList!!.getMessage(), Toast.LENGTH_SHORT).show()
                            } else if (response.code() == 200) {

                                dataList = response.body()

                                if (dataList!!.getSuccess() == true) {
                                    if (dataList!!.getData() != null) {

                                        val accessToken = dataList!!.getData()!!.original!!.accessToken
                                        val userID = dataList!!.getData()!!.original!!.loginUserData!!.userId
                                        val editor: SharedPreferences.Editor = sharedPreferences.edit()
                                        editor.putString("userID", userID.toString())
                                        editor.putString("Access_Token", accessToken)
                                        editor.apply()
                                        editor.commit()
                                        val intent = Intent(applicationContext, ActivityHome::class.java)
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                        startActivity(intent)
                                    } else {
                                        Toast.makeText(applicationContext, "No data found", Toast.LENGTH_SHORT).show()
                                    }
                                } else {
                                    Toast.makeText(applicationContext, dataList!!.getMessage().toString(), Toast.LENGTH_SHORT).show()
                                }

                            } else {
                                Toast.makeText(applicationContext, "Something went wrong", Toast.LENGTH_SHORT).show()
                            }
                        } catch (exception: Exception) {
                            Toast.makeText(applicationContext, exception.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                })

            } catch (ex: Exception) {
                Toast.makeText(this, ex.message.toString(), Toast.LENGTH_SHORT).show()
            }

        } else {
            Toast.makeText(this, "No network available!", Toast.LENGTH_SHORT).show()
        }
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {

                mFusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                    var location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                        latTextView.text = location.latitude.toString()
                        lonTextView.text = location.longitude.toString()
                    }
                }
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        var mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient!!.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            var mLastLocation: Location = locationResult.lastLocation
            findViewById<TextView>(R.id.latTextView).text = mLastLocation.latitude.toString()
            findViewById<TextView>(R.id.lonTextView).text = mLastLocation.longitude.toString()
        }
    }

    private fun isLocationEnabled(): Boolean {
        var locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        )
    }

    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSION_ID
        )
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLastLocation()
            }
        }
    }
}
