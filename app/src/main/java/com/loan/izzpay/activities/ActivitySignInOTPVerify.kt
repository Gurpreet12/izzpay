package com.loan.izzpay.activities

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.CountDownTimer
import android.text.TextUtils
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.loan.izzpay.R
import com.loan.izzpay.RetrofitConnections.ApiClient
import com.loan.izzpay.models.GauravRegisterModel
import com.loan.izzpay.models.LoginModel
import com.loan.izzpay.models.OTPVerifyOnSignUpCase
import com.loan.izzpay.utils.Constant.hasNetworkAvailable
import retrofit2.Call
import retrofit2.Callback
import java.lang.Double.parseDouble
import java.lang.Exception
import java.util.*
import java.util.concurrent.TimeUnit


class ActivitySignInOTPVerify : AppCompatActivity() {


    lateinit var back_arrow_imgBtn: ImageButton

    lateinit var progerssProgressDialog: ProgressDialog
    var dataList: OTPVerifyOnSignUpCase? = null

    lateinit var telephone_edt: EditText

    lateinit var send_otp_code: Button
    lateinit var verify_code: Button

    var strLocalOTP: String = ""

    lateinit var resend_otp_tv: TextView

    private val sharedPrefDB = "izzpay_db"

    lateinit var sharedPreferences: SharedPreferences

    var verifyPhoneModel: GauravRegisterModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp_verify)

        init()

        mListeners()
    }

    private fun init() {

        back_arrow_imgBtn = findViewById(R.id.back_arrow_imgBtn)
        send_otp_code = findViewById(R.id.send_otp_code)
        telephone_edt = findViewById(R.id.telephone_edt)

        progerssProgressDialog = ProgressDialog(this)
        progerssProgressDialog.setTitle("Please wait...")
        progerssProgressDialog.setCancelable(false)

        sharedPreferences = this.getSharedPreferences(sharedPrefDB, Context.MODE_PRIVATE)

    }

    private fun mListeners() {
        back_arrow_imgBtn.setOnClickListener {
            finish()
        }

        send_otp_code.setOnClickListener {

            val phone = telephone_edt.text.toString()
            if (!phone.isEmpty()) {
                telephone_edt.setError(null)
                if (phone.length == 10) {
                    telephone_edt.setError(null)
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm!!.hideSoftInputFromWindow(getCurrentFocus()!!.getWindowToken(), 0)
                    mSendOTPForSignIn(phone.trim())
                } else {
                    telephone_edt.setError("enter 10 digits number")
                    Toast.makeText(applicationContext, "enter 10 digits number", Toast.LENGTH_SHORT).show()
                }
            } else {
                telephone_edt.setError("enter phone no")

            }
        }
    }

    fun String.isEmailValid(): Boolean {
        return !TextUtils.isEmpty(this) && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
    }


    private fun mSendOTPForSignIn(phoneStr: String) {

        if (hasNetworkAvailable(applicationContext)) {

            progerssProgressDialog.show()

            try {
                val call: Call<OTPVerifyOnSignUpCase> = ApiClient.getClient.loginOTPSendForSignIn(phoneStr)
                call.enqueue(object : Callback<OTPVerifyOnSignUpCase> {
                    override fun onFailure(call: Call<OTPVerifyOnSignUpCase>, t: Throwable) {
                        progerssProgressDialog.dismiss()
                        Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<OTPVerifyOnSignUpCase>, response: retrofit2.Response<OTPVerifyOnSignUpCase>) {
                        progerssProgressDialog.dismiss()

                        try {
                            dataList = response.body()

                            if (response.code() == 200) {


                                if (dataList != null) {

                                    val message = dataList!!.getMessage()

                                    if (dataList!!.getSuccess() == true)
                                    {
                                        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
                                        verifyUserForSignInDialog(phoneStr)
                                    }
                                    else{
                                        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
                                    }

                                } else {
                                    Toast.makeText(applicationContext, "No data found", Toast.LENGTH_SHORT).show()
                                }
                            } else if (response.code() == 409) {
                                Toast.makeText(applicationContext, response.message().toString(), Toast.LENGTH_SHORT).show()
                            } else if (response.code() == 501) {
                                Toast.makeText(applicationContext, response.message().toString(), Toast.LENGTH_SHORT).show()
                            }
                        } catch (e: Exception) {
                            Toast.makeText(applicationContext, "Already exit user,or something went wrong", Toast.LENGTH_SHORT).show()
                        }


                    }

                })

            } catch (ex: Exception) {
                Toast.makeText(this, ex.message.toString(), Toast.LENGTH_SHORT).show()
            }

        } else {
            Toast.makeText(this, "No network available!", Toast.LENGTH_SHORT).show()
        }
    }



    private fun verifyUserForSignInDialog(strPhone:String) {

        val dialogBuilder = AlertDialog.Builder(this)
        val layoutView = layoutInflater.inflate(R.layout.mobile_verify_user_on_sign_up, null)

        val verifyMobileBtn: Button = layoutView.findViewById(R.id.verifyMobileBtn)
        val titleTv: TextView = layoutView.findViewById(R.id.titleTv)
        val otpDescTv: TextView = layoutView.findViewById(R.id.otpDescTv)
        val otpEdt: EditText = layoutView.findViewById(R.id.otpEdt)
        val back_arrow_imgBtn: ImageButton = layoutView.findViewById(R.id.back_arrow_imgBtn)

        resend_otp_tv = layoutView.findViewById(R.id.resend_otp_tv)
        dialogBuilder.setView(layoutView)
        val alertDialog = dialogBuilder.create()
        alertDialog.getWindow()!!.getAttributes().windowAnimations = R.style.DialogAnimation
        alertDialog.show()

        titleTv.setText("Verify your mobile number")
        otpDescTv.setText("OTP has been sent to your mobile, please enter it below")

        val timer = object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val text = String.format(Locale.getDefault(), "%02d : %02d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % 60,
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60)
                resend_otp_tv.setText(text)
            }

            override fun onFinish() {
                resend_otp_tv.setText("Resend")
                resend_otp_tv.isEnabled = true

                resend_otp_tv.setOnClickListener {
                    alertDialog.dismiss()
                    mSendOTPForSignIn(strPhone)
                }

            }

        }
        timer.start()

        back_arrow_imgBtn.setOnClickListener {
            alertDialog.dismiss()
        }

        verifyMobileBtn.setOnClickListener {

            if (otpEdt.text.isEmpty()) {

                Toast.makeText(applicationContext, "Enter the otp", Toast.LENGTH_SHORT).show()

            } else {

                val otpMy = otpEdt.text.toString()
                alertDialog.dismiss()
                apiVerifyOTPForSignIpPhone(strPhone,otpMy)

            }

        }
    }


    private fun apiVerifyOTPForSignIpPhone(phone: String, smsCode: String) {

        if (hasNetworkAvailable(applicationContext)) {

            progerssProgressDialog.show()

            try {
                val call: Call<GauravRegisterModel> = ApiClient.getClient.loginOTPVerifydForSignIn(phone,"Phone","Android","2476J3RJBFEFN","IMEI","xxxxxx","xxxxxx", smsCode)
                call.enqueue(object : Callback<GauravRegisterModel> {
                    override fun onFailure(call: Call<GauravRegisterModel>, t: Throwable) {
                        progerssProgressDialog.dismiss()
                        Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<GauravRegisterModel>, response: retrofit2.Response<GauravRegisterModel>) {

                        progerssProgressDialog.dismiss()

                        try {
                            if (response.code() == 200) {

                                verifyPhoneModel = null

                                verifyPhoneModel = response.body()

                                if (verifyPhoneModel!!.getSuccess() == true) {

                                    if (verifyPhoneModel!!.getData() != null) {

                                        Toast.makeText(applicationContext, verifyPhoneModel!!.getMessage().toString(), Toast.LENGTH_SHORT).show()
                                        val accessToken = verifyPhoneModel!!.getData()!!.original!!.accessToken
                                        val userID = verifyPhoneModel!!.getData()!!.original!!.loginUserData!!.userId
                                        val editor: SharedPreferences.Editor = sharedPreferences.edit()
                                        editor.putString("userID", userID.toString())
                                        editor.putString("Access_Token", accessToken)
                                        editor.apply()
                                        editor.commit()
                                        val intent = Intent(applicationContext, ActivityHome::class.java)
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                        startActivity(intent)
                                    }
                                }
                                else{
                                    Toast.makeText(applicationContext, verifyPhoneModel!!.getMessage().toString(), Toast.LENGTH_SHORT).show()
                                }
                            }
                            else if (response.code() == 409) {
                                verifyPhoneModel = response.body()
                                Toast.makeText(applicationContext, verifyPhoneModel!!.getMessage(), Toast.LENGTH_SHORT).show()
                            }
                            else if (response.code() == 409) {
                                Toast.makeText(applicationContext, verifyPhoneModel!!.getMessage(), Toast.LENGTH_SHORT).show()
                            } else if (response.code() == 404) {
                                Toast.makeText(applicationContext, verifyPhoneModel!!.getMessage(), Toast.LENGTH_SHORT).show()
                            }
                            else if (response.code() == 501) {
                                Toast.makeText(applicationContext, verifyPhoneModel!!.getMessage(), Toast.LENGTH_SHORT).show()
                            }
                        }
                        catch (exception:Exception){
                            Toast.makeText(applicationContext,exception.message,Toast.LENGTH_SHORT).show()
                        }
                    }

                })

            } catch (ex: java.lang.Exception) {
                Toast.makeText(this, ex.message.toString(), Toast.LENGTH_SHORT).show()
            }

        } else {
            Toast.makeText(this, "No network available!", Toast.LENGTH_SHORT).show()
        }
    }
}