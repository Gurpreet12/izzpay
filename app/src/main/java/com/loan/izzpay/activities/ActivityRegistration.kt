package com.loan.izzpay.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.text.InputFilter
import android.text.InputType
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatSpinner
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.widget.NestedScrollView
import com.android.volley.*
import com.android.volley.DefaultRetryPolicy.DEFAULT_MAX_RETRIES
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.RequestFuture
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.snackbar.Snackbar
import com.loan.izzpay.R
import com.loan.izzpay.utils.Constant.hasNetworkAvailable
import kotlinx.android.synthetic.main.activity_registration.*
import kotlinx.android.synthetic.main.content_activity_home.*
import kotlinx.android.synthetic.main.testing_new.*
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.nio.charset.Charset
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern
import javax.xml.datatype.DatatypeConstants.MONTHS

class ActivityForm : AppCompatActivity() {


    private lateinit var full_name: EditText
    private lateinit var phone_no: EditText
    private lateinit var email: EditText
    private lateinit var dob: EditText
    private lateinit var aadhar: EditText
    private lateinit var pan: EditText
    private lateinit var driving_lic_no: EditText
    private lateinit var voter_id: EditText
    private lateinit var passport_no: EditText
    private lateinit var state: AutoCompleteTextView
    private lateinit var city: AutoCompleteTextView
    private lateinit var pincode: EditText
    private lateinit var permanent_address: EditText
    private lateinit var current_address: EditText
    private lateinit var gender: EditText
    private lateinit var exp_year: AppCompatSpinner
    private lateinit var exp_months: AppCompatSpinner
    private lateinit var employment_type: EditText
    private lateinit var desigation: EditText
    private lateinit var company_name: EditText
    private lateinit var monthly_salary: EditText
    private lateinit var checkbox: CheckBox
    private lateinit var Submit: Button

    var listSpinner = ArrayList<String>()
    var listAll = ArrayList<String>()
    var listState = ArrayList<String>()
    var listCity = ArrayList<String>()
    var act: AutoCompleteTextView? = null
    private lateinit var constraint: ConstraintLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        val url: String = "https://35.154.26.90/staging-partner.abfldirect.com/api/get-ccc"

        full_name = findViewById(R.id.form_name)
        phone_no = findViewById(R.id.form_phone_no)
        email = findViewById(R.id.form_email)
        dob = findViewById(R.id.form_dob)
        aadhar = findViewById(R.id.form_adhar)
        pan = findViewById(R.id.form_pan)
        driving_lic_no = findViewById(R.id.form_driving_lic)
        voter_id = findViewById(R.id.form_voter_id)
        passport_no = findViewById(R.id.form_passport_no)
        state = findViewById(R.id.form_state)
        city = findViewById(R.id.form_city)
        pincode = findViewById(R.id.form_pincode)
        permanent_address = findViewById(R.id.form_p_address)
        current_address = findViewById(R.id.form_c_address)
        gender = findViewById(R.id.form_gender)
        exp_year = findViewById(R.id.form_experience_yr)
        exp_months = findViewById(R.id.form_experience_months)
        employment_type = findViewById(R.id.form_employment_type)
        desigation = findViewById(R.id.form_designation)
        company_name = findViewById(R.id.form_employer_name)
        monthly_salary = findViewById(R.id.form_monthly_salary)
        checkbox = findViewById(R.id.same)
        //coordinate = findViewById(R.id.coordinate)
        constraint = findViewById(R.id.constraint_layout)
        driving_lic_no.filters = arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(15))
        pan.filters = arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(10))
        passport_no.filters = arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(8))
        voter_id.filters = arrayOf(InputFilter.AllCaps(), InputFilter.LengthFilter(10))
        val get_name = full_name.text.toString()
        val get_phone = phone_no.text.toString()
        val get_email = email.text.toString()
        val get_dob = dob.text.toString()
        val get_aadhar = aadhar.text.toString()
        val get_pan = pan.text.toString()
        val get_driving_lic = driving_lic_no.text.toString()
        val get_voter_id = voter_id.text.toString()
        val get_passport_no = passport_no.text.toString()
        val get_state = state.text.toString()
        val get_city = city.text.toString()
        val get_pincode = pincode.text.toString()
        val get_permanent_add = permanent_address.text.toString()
        val get_current_add = current_address.text.toString()
        val get_gender = gender.text.toString()
        val get_emplyo_type = employment_type.text.toString()
        val get_designation = desigation.text.toString()
        val get_company_name = company_name.text.toString()
        val get_monthly_salary = monthly_salary.text.toString()
        Submit = findViewById(R.id.submit_btn)
        callAll()
        checkbox.setOnClickListener {
            if (checkbox.isChecked) {
                val get_permanent_add = permanent_address.text.toString()
                current_address.setText(get_permanent_add)
            }
        }

//        val objectRequest: JsonObjectRequest = JsonObjectRequest(Request.Method.GET, url, null, object : Response.Listener<JSONObject> {
//            override fun onResponse(response: JSONObject?) {
//                Log.e("Request response", response.toString())
//            }
//        },
//                object : Response.ErrorListener {
//                    override fun onErrorResponse(error: VolleyError?) {
//                        Log.e("Request response", error.toString())
//                    }
//                })
//
//        requestQueue.add(objectRequest)

        val stringArr: Array<String> = resources.getStringArray(R.array.years);
        val dataAdapter = ArrayAdapter<String>(this, R.layout.simple_spinner_item, stringArr)
        dataAdapter.setDropDownViewResource(R.layout.simple_spinner_drop_down)
        form_experience_yr.adapter = dataAdapter


        form_experience_yr.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }
        val stringArr1: Array<String> = resources.getStringArray(R.array.months);
        val dataAdapter1 = ArrayAdapter<String>(this, R.layout.simple_spinner_item, stringArr1)
        dataAdapter1.setDropDownViewResource(R.layout.simple_spinner_drop_down)
        form_experience_months.adapter = dataAdapter1


        form_experience_months.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        dob.setOnClickListener {

            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, month, day ->
                // Display Selected date in TextView
                dob.setText("" + year + "-" + (month + 1) + "-" + day)
            }, year, month, day)
            dpd.show()

        }



        gender.setOnClickListener {
            select_gender()
        }
        employment_type.setOnClickListener {
            select_employment_type()
        }
        desigation.setOnClickListener {
            select_designation()
        }
        Submit.setOnClickListener {


            var email_valid = email.getText().toString().trim()
            if (full_name.length() == 0) {
                // full_name.error = "Please Enter Your Name!!"
                val snack = Snackbar.make(it, "Enter your full Name.", Snackbar.LENGTH_SHORT)
                snack.view.setBackgroundColor(Color.parseColor("#1CCDD3"))
                snack.show()
            } else if (phone_no.length() == 0) {
                // phone.error = "Please Enter Your Phone no.!!"
                Snackbar.make(it, "Enter your Phone no.", Snackbar.LENGTH_SHORT).show()
            } else if (phone_no.length() < 10) {
                //   phone_no.error = "Phone Number should be of 10 digits!! "
                Snackbar.make(it, "Phone Number should be of 10 digits!!", Snackbar.LENGTH_SHORT).show()
            } else if (email.length() == 0) {
                // phone.error = "Please Enter Your Phone no.!!"
                Snackbar.make(it, "Enter your Email.", Snackbar.LENGTH_SHORT).show()

            } else if (!(Patterns.EMAIL_ADDRESS.matcher(email_valid).matches())) {
                // email.setError("Please enter a valid ok address");
                Snackbar.make(it, "Enter valid Email.", Snackbar.LENGTH_SHORT).show()
            } else {
                if (hasNetworkAvailable(applicationContext)) {
                    // submit.isEnabled = false
                    Toast.makeText(this, "success", Toast.LENGTH_SHORT).show()
                    //startPayment()
                    //get_data()


                } else {
                    Toast.makeText(
                            getApplicationContext(),
                            "No network available!",
                            Toast.LENGTH_SHORT
                    ).show()
                }
            }


            // startActivity(Intent(this, ActivityHome::class.java))
            val requestQueue = Volley.newRequestQueue(this)
            val postRequest = object : StringRequest(Request.Method.POST, url,
                    object : Response.Listener<String> {
                        override fun onResponse(response: String) {
                            // response
                            Log.d("TAG", response)

                            try {

                                val responseObj = JSONObject(response)

                                // Parsing json object response
                                // response will be a json object
                                val error = responseObj.getBoolean("error")
                                val message = responseObj.getString("message")

                                if (!error) {
                                    val intent = Intent(applicationContext, ActivityHome::class.java)
                                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                    startActivity(intent)

                                    Toast.makeText(applicationContext, "jkdhsjdb", Toast.LENGTH_LONG).show()

                                } else run { Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show() }

                            } catch (e: JSONException) {
//                    Toast.makeText(getApplicationContext(),
//                            "Error: " + e.getMessage(),
//                            Toast.LENGTH_LONG).show();
                                Log.e("TAG", "Error: " + e.message);

                            }

                        }
                    },
                    object : Response.ErrorListener {
                        override fun onErrorResponse(error: VolleyError) {
                            // error
                            Log.d("Error.Response", error.toString())
                        }
                    }
            ) {
                override fun getParams(): MutableMap<String, String> {
//                    val name_string=get_name.split("\\s".toRegex())
//                    val first_name=name_string[0]
//                    val last_name=name_string[1]
                    val params = HashMap<String, String>()
                    params.put("first_name", "Gurpreet")
                    params.put("last_name", "Kaur")
                    params.put("mobile", get_phone)
                    params.put("email", get_email)
                    params.put("date_of_birth", get_dob)
                    params.put("aadhar", get_aadhar)
                    params.put("pan_no", get_pan)
                    params.put("drv_licence_no", get_driving_lic)
                    params.put("voter_id", get_voter_id)
                    params.put("passport_no", get_passport_no)
                    params.put("state", get_state)
                    params.put("city", get_city)
                    params.put("pincode", get_pincode)
                    params.put("address_line1", get_permanent_add)
                    params.put("address_line2", get_current_add)
                    params.put("gender", get_gender)
                    params.put("applicant_constitution", get_emplyo_type)
                    params.put("vendor_name", get_company_name)
                    params.put("address_type", get_designation)
                    params.put("customer_id", get_monthly_salary)
                    return params
                }

                override fun getHeaders(): MutableMap<String, String> {
                    val params = HashMap<String, String>()
                    params.put("Content-Type", "application/json")
                    //  val credentials = "username:password"
                    val auth = "Basic" + "57bcc29eae6d4156b89a5e3a2001464e996733"
                    // val auth = "Basic "+ Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    params.put("Authorization", auth)
                    return params
                }

                override fun getBodyContentType(): String = "application/json"
            }


            postRequest.setRetryPolicy(DefaultRetryPolicy(
                    60000,
                    //6*DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))
            requestQueue.add(postRequest)


        }
    }

    private fun select_employment_type() {
        val inflater = LayoutInflater.from(this)
        val builderView =
                inflater!!.inflate(R.layout.select_employement, null);

        val builder = AlertDialog.Builder(this);
        builder.setView(builderView);
        val alert = builder.create();
        alert.show()

        val salaried =
                builderView.findViewById(R.id.occupation_text1) as TextView
        val self_employed = builderView.findViewById(R.id.occupation_text2) as TextView


        salaried.setOnClickListener {
            alert.hide()
            employment_type.setText("Salaried")


        }
        self_employed.setOnClickListener {
            alert.hide()
            employment_type.setText("Self-employed")
        }
    }

    private fun select_designation() {
        val inflater = LayoutInflater.from(this)
        val builderView =
                inflater!!.inflate(R.layout.select_designation, null);

        val builder = AlertDialog.Builder(this);
        builder.setView(builderView);
        val alert = builder.create();
        alert.show()

        val manager =
                builderView.findViewById(R.id.designation_manager) as TextView
        val senior = builderView.findViewById(R.id.designation_senior) as TextView
        val junior = builderView.findViewById(R.id.designation_junior) as TextView



        manager.setOnClickListener {
            alert.hide()
            desigation.setText("Manager")


        }
        senior.setOnClickListener {
            alert.hide()
            desigation.setText("Senior")


        }
        junior.setOnClickListener {
            alert.hide()
            desigation.setText("Junior")
        }
    }


    private fun select_gender() {
        val inflater = LayoutInflater.from(this)
        val builderView =
                inflater!!.inflate(R.layout.select_gender, null);

        val builder = AlertDialog.Builder(this);
        builder.setView(builderView);
        val alert = builder.create();
        alert.show()

        val male = builderView.findViewById(R.id.gender_text1) as TextView
        val female = builderView.findViewById(R.id.gender_text2) as TextView
        val other = builderView.findViewById(R.id.gender_text3) as TextView


        male.setOnClickListener {
            alert.hide()
            gender.setText("Male")
        }
        female.setOnClickListener {
            alert.hide()
            gender.setText("Female")
        }
        other.setOnClickListener {
            alert.hide()
            gender.setText("Other")
        }
    }

    fun getJson(): String? {
        var json: String? = null
        try {
            val charset: Charset = Charsets.UTF_8
            // Opening cities.json file
            val `is` = getAssets().open("cities.json")
            // is there any content in the file
            val size = `is`.available()
            val buffer = ByteArray(size)
            // read values in the byte array
            `is`.read(buffer)
            // close the stream --- very important
            `is`.close()
            // convert byte to string
            json = String(buffer, charset)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return json
        }
        return json
    }

    fun callAll() {
        obj_list()
        addCity()
        addState()
    }

    fun obj_list() {
        // Exceptions are returned by JSONObject when the object cannot be created
        try {
            // Convert the string returned to a JSON object
            val jsonObject = JSONObject(getJson())
            // Get Json array
            val array = jsonObject.getJSONArray("array")
            // Navigate through an array item one by one
            for (i in 0 until array.length()) {
                // select the particular JSON data
                val `object` = array.getJSONObject(i)
                val city = `object`.getString("name")
                val state = `object`.getString("state")
                // add to the lists in the specified format
                listSpinner.add((i + 1).toString() + " : " + city + " , " + state)
                listAll.add(city + " , " + state)
                listCity.add(city)
                listState.add(state)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    fun addCity() {
        act = findViewById(R.id.form_city) as AutoCompleteTextView
        adapterSetting(listCity)
    }

    fun addState() {
        val set = HashSet<String>(listState)
        act = findViewById(R.id.form_state) as AutoCompleteTextView
        adapterSetting(ArrayList(set))
    }

    fun adapterSetting(arrayList: ArrayList<String>) {
        val adapter =
                ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, arrayList)
        act!!.setAdapter(adapter)
        hideKeyBoard()
    }

    fun hideKeyBoard() {
        act!!.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
            }
        })
    }

    fun get_data() {
        val insert_name_string = full_name.text.toString().split("\\s".toRegex())
        val insert_first_name = insert_name_string[0]
        val insert_last_name = insert_name_string[1]
        val insert_phone = phone_no.text.toString()
        val insert_email = email.text.toString()
        val insert_dob = dob.text.toString()
        val insert_aadhar = aadhar.text.toString()
        val insert_pan = pan.text.toString()
        val insert_driving_lic = driving_lic_no.text.toString()
        val insert_voter_id = voter_id.text.toString()
        val insert_passport_no = passport_no.text.toString()
        val insert_state = state.text.toString()
        val insert_city = city.text.toString()
        val insert_pincode = pincode.text.toString()
        val insert_permanent_add = permanent_address.text.toString()
        val insert_current_add = current_address.text.toString()
        val insert_exp_yrs = exp_year.selectedItem.toString()
        val insert_exp_months = exp_months.selectedItem.toString()
        val insert_gender = gender.text.toString()
        val insert_emplyo_type = employment_type.text.toString()
        val insert_designation = desigation.text.toString()
        val insert_company_name = company_name.text.toString()
        val insert_monthly_salary = monthly_salary.text.toString()
        val type = "insert"
        val backgroundWorker = database_form_linking(this)
        backgroundWorker.execute(
                type,
                insert_first_name,
                insert_last_name,
                insert_phone,
                insert_email,
                insert_dob,
                insert_aadhar,
                insert_pan,
                insert_driving_lic,
                insert_voter_id,
                insert_passport_no,
                insert_state,
                insert_city,
                insert_pincode,
                insert_permanent_add,
                insert_current_add,
                insert_gender,
                insert_exp_yrs,
                insert_exp_months,
                insert_emplyo_type,
                insert_company_name,
                insert_designation,
                insert_monthly_salary
        )
    }
//    fun test_firstAndLastName_success() {
//        val ptrn = Pattern.compile("(\\w+)\\s+(\\w+)")
//        val matcher = ptrn.matcher("FirstName LastName")
//        if (matcher.find())
//        {
//            Assert.assertEquals("FirstName", matcher.group(1))
//            Assert.assertEquals("LastName", matcher.group(2))
//        }
//        else
//        {
//            Assert.fail()
//        }
//    }
}