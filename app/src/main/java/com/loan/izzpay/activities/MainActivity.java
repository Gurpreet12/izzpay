package com.loan.izzpay.activities;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toolbar;

import com.loan.izzpay.R;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView name, email, mobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }}
