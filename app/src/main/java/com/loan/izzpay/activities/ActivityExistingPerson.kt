package com.loan.izzpay.activities

import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.loan.izzpay.R
import java.util.HashMap

class ActivityExistingPerson :AppCompatActivity(){


    private lateinit var user_name:EditText
    private lateinit var password:EditText
    private lateinit var submit_button:Button

    val url: String = " https://staging-partner.abfldirect.com/api/get-access-token"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.test_activity)
        user_name=findViewById(R.id.tst_username)
        password=findViewById(R.id.tst_password)
        submit_button=findViewById(R.id.tst_btn)

        val get_username =user_name.text.toString()
        val get_pass =password.text.toString()

        submit_button.setOnClickListener {
            // startActivity(Intent(this, ActivityHome::class.java))
            val requestQueue = Volley.newRequestQueue(this)
            val postRequest = object : StringRequest(Request.Method.GET, url,
                    object : Response.Listener<String> {
                        override fun onResponse(response: String) {
                            // response
                            Log.d("Response", response)
                        }
                    },
                    object : Response.ErrorListener {
                        override fun onErrorResponse(error: VolleyError) {
                            // error
                            Log.d("Error.Response", error.toString())
                        }
                    }
            ) {
//                override fun getParams(): MutableMap<String, String> {
//
//                    val params = HashMap<String, String>()
//                    params.put("client_name",get_username)
//                    params.put("client_password", get_pass)
//
//                    return params
//                }

                override fun getHeaders(): MutableMap<String, String>  {
                    val params = HashMap<String, String>()
                    val charset = Charsets.UTF_8
                    params["Content_Type"]="application/json"
//                    params.put("Content-Type", "application/json")
                      val credentials = get_username +":"+ get_pass
                    //val auth = "Basic"+"893b70ff19494c60a643db35b119bfd4781570"
                     val auth = "Basic "+ Base64.encodeToString(credentials.toByteArray(charset), Base64.NO_WRAP);
                   // params.put("Authorization", auth)
                    params["Authorization"] = auth
                    return params
                }

                override fun getBodyContentType(): String = "application/json"
            }



//            protected val params:Map<String, String>
//                get() {
//                    val params = HashMap<String, String>()
//                    params.put("name", "Alif")
//                    params.put("domain", "http://itsalif.info")
//                    return params
//                }
////        }
            postRequest.setRetryPolicy(DefaultRetryPolicy(
                    0,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))
            requestQueue.add(postRequest)


        }
    }
}