package com.delivery.delpro.activities

import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.delivery.delpro.adapters.SliderAdapter
import com.loan.izzpay.R
import com.loan.izzpay.activities.ActivityStart

class ActivityOnboarding: AppCompatActivity() {


    private lateinit var SlidePager: ViewPager
    private lateinit var DotLayout: LinearLayout
    private lateinit var mdots: Array<TextView?>
    private lateinit var slider_adapter: SliderAdapter
    private lateinit var back:Button
    private lateinit var next:Button
    private  var  mCurrentPage = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding)

        SlidePager = findViewById(R.id.sliderViewPager)
        DotLayout = findViewById(R.id.dotsLayout)
        slider_adapter = SliderAdapter(this)
        SlidePager.setAdapter(slider_adapter)
        addDotsIndicator(0)
        SlidePager.addOnPageChangeListener((viewListner))
        back=findViewById(R.id.prev)
        next=findViewById(R.id.next)

        next.setOnClickListener(View.OnClickListener {
            SlidePager.setCurrentItem(mCurrentPage + 1)
        })
        back.setOnClickListener(View.OnClickListener {
            SlidePager.setCurrentItem(mCurrentPage - 1)
        })
    }

    fun addDotsIndicator(position: Int) {
        mdots = arrayOfNulls<TextView>(3)
        DotLayout.removeAllViews()
        for (i in 0 until mdots.size) {
            mdots[i] = TextView(this)
            mdots[i]?.setText(Html.fromHtml("&#8226;"))
            mdots[i]?.setTextSize(35F)
            mdots[i]?.setTextColor(resources.getColor(R.color.colorGrey))
            DotLayout.addView(mdots[i])
        }
         if (mdots.size > 0) {
           mdots[position]?.setTextColor(resources.getColor(R.color.colorBlack))
         }
    }
    var viewListner:OnPageChangeListener=object: ViewPager.OnPageChangeListener {
        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) {

        }
        override fun onPageScrollStateChanged(state: Int) {

        }

        override fun onPageSelected(position: Int) {
            addDotsIndicator(position)
            mCurrentPage = position
            if (position == 0) {
                next.isEnabled = true
                back.isEnabled = false
                back.visibility = View.INVISIBLE
                next.setText("Next")
                back.setText("")
            } else if (position == mdots.size-1) {
                next.isEnabled = true
                back.isEnabled = true
                back.visibility = View.VISIBLE
                next.setText("Finish")
                back.setText("Back")

                next.setOnClickListener(View.OnClickListener {
                    val i= Intent(applicationContext, ActivityStart::class.java)
                    startActivity(i)
                })

            } else {
                next.isEnabled = true
                back.isEnabled = true
                back.visibility = View.VISIBLE
                next.setText("Next")
                back.setText("Back")
            }

        }

    }
}