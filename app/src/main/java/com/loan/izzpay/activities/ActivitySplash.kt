package com.loan.izzpay.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity

import com.loan.izzpay.R
import io.sentry.core.Sentry




class ActivitySplash : AppCompatActivity() {

    lateinit var sharedPreferences: SharedPreferences
    private val sharedPrefDB = "izzpay_db"

    private val SPLASH_DELAY: Long = 2000 //3 seconds

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        setContentView(R.layout.activity_splash)


        sharedPreferences = this.getSharedPreferences(sharedPrefDB, Context.MODE_PRIVATE)

        Handler().postDelayed({

            val userID = sharedPreferences.getString("userID", "")

            if (userID.equals("0")) {
                startActivity(Intent(this, ActivityStart::class.java))
            }
            else if (userID.equals("")) {
                startActivity(Intent(this, ActivityStart::class.java))
            }
            else {
                startActivity(Intent(this, ActivityHome::class.java))
            }
            finish()
        }, SPLASH_DELAY)
    }
}
