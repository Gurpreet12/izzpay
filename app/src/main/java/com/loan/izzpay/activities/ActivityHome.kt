package com.loan.izzpay.activities

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.Gravity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.Toast
import androidx.core.view.GravityCompat
import androidx.viewpager.widget.ViewPager
import com.google.android.material.navigation.NavigationView
import com.google.android.material.tabs.TabLayout
import com.loan.izzpay.R
import com.loan.izzpay.RetrofitConnections.ApiClient
import com.loan.izzpay.adapters.FragmentViewPager
import com.loan.izzpay.models.DefaultModel
import com.loan.izzpay.utils.Constant
import retrofit2.Call
import retrofit2.Callback
import java.lang.Exception


class ActivityHome : AppCompatActivity() {


    private lateinit var tabLayout: TabLayout
    private lateinit var NavView: NavigationView
    private lateinit var notification_bell: ImageView
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var home_frame: FrameLayout

    private val sharedPrefDB = "izzpay_db"
    lateinit var sharedPreferences: SharedPreferences

    lateinit var progerssProgressDialog: ProgressDialog


    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        sharedPreferences = this.getSharedPreferences(sharedPrefDB, Context.MODE_PRIVATE)
        progerssProgressDialog = ProgressDialog(this@ActivityHome)
        progerssProgressDialog.setTitle("Please wait...")
        progerssProgressDialog.setCancelable(false)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        notification_bell = findViewById(R.id.notificationImgBtn)
        drawerLayout = findViewById(R.id.drawer_layout)
        val navgationImgBtn: ImageView = findViewById(R.id.navgationImgBtn)
        NavView = findViewById(R.id.nav_view)
        // home_frame=findViewById(R.id.frame_home)
        notification_bell.setOnClickListener {
            val i = Intent(this@ActivityHome, ActivityBaseForFragment::class.java)
            i.putExtra("checkPage", "notification_bell")
            startActivity(i)
        }
        navgationImgBtn.setOnClickListener {
            drawerLayout.openDrawer(Gravity.START)
        }

        NavView.itemIconTintList = null
        NavView.setNavigationItemSelectedListener(object : NavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(p0: MenuItem): Boolean {
                when (p0.itemId) {
                    R.id.edit_profile -> {
                        drawerLayout.closeDrawer(Gravity.LEFT)
                        val i = Intent(this@ActivityHome, ActivityBaseForFragment::class.java)
                        i.putExtra("checkPage", "profile")
                        startActivity(i)

                    }
                    R.id.loan_statement -> {
                        val i = Intent(this@ActivityHome, ActivityBaseForFragment::class.java)
                        i.putExtra("checkPage", "loan_statement")
                        startActivity(i)
                    }
                    R.id.loan_eligibility -> {
                        val i = Intent(this@ActivityHome, ActivityBaseForFragment::class.java)
                        i.putExtra("checkPage", "eligible")
                        startActivity(i)
                    }
                    R.id.your_limit -> {
                        val i = Intent(this@ActivityHome, ActivityBaseForFragment::class.java)
                        i.putExtra("checkPage", "your_limit")
                        startActivity(i)
                    }

                    R.id.smart_repay -> {
                        val i = Intent(this@ActivityHome, ActivityBaseForFragment::class.java)
                        i.putExtra("checkPage", "smart_repay")
                        startActivity(i)
                    }
                    R.id.calculator -> {
                        val i = Intent(this@ActivityHome, ActivityBaseForFragment::class.java)
                        i.putExtra("checkPage", "calcultor")
                        startActivity(i)
                    }
                    R.id.refer_earn -> {
                        val i = Intent(this@ActivityHome, ActivityBaseForFragment::class.java)
                        i.putExtra("checkPage", "refer_earn")
                        startActivity(i)
                    }
                    R.id.faq -> {
                        val i = Intent(this@ActivityHome, ActivityBaseForFragment::class.java)
                        i.putExtra("checkPage", "faq")
                        startActivity(i)
                    }
                    R.id.about_us -> {
                        val i = Intent(this@ActivityHome, ActivityBaseForFragment::class.java)
                        i.putExtra("checkPage", "about_us")
                        startActivity(i)
                    }
                    R.id.rate_us -> {
                        val i = Intent(this@ActivityHome, ActivityBaseForFragment::class.java)
                        i.putExtra("checkPage", "rate_us")
                        startActivity(i)
                    } R.id.contact_us -> {
                        val i = Intent(this@ActivityHome, ActivityBaseForFragment::class.java)
                        i.putExtra("checkPage", "contact_us")
                        startActivity(i)
                    }
                    R.id.logout -> {
                        mLogout()
                    }

                }
                drawerLayout.closeDrawer(GravityCompat.START)
                return true
            }
        })

        tabLayout = findViewById(R.id.tabLayout)
        val viewPager = findViewById<ViewPager>(R.id.view_pager)

        tabLayout.addTab(tabLayout.newTab().setText("Home"))
        tabLayout.addTab(tabLayout.newTab().setText("Loan Types"))
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL)
        viewPager.adapter = FragmentViewPager(this, supportFragmentManager, tabLayout.getTabCount())
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {

            }

            override fun onTabSelected(p0: TabLayout.Tab?) {
                viewPager.setCurrentItem(p0!!.getPosition())
            }
        }
        )
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_home2_drawer, menu)
        return true
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed();
        }
    }

    var defaultModel: DefaultModel? = null

    private fun mLogout() {

        if (Constant.hasNetworkAvailable(applicationContext!!)) {

            val Access_Token = sharedPreferences.getString("Access_Token", "")

            progerssProgressDialog.show()

            try {
                val call: Call<DefaultModel> = ApiClient.getClient.Logout("Bearer "+Access_Token)
                call.enqueue(object : Callback<DefaultModel> {
                    override fun onFailure(call: Call<DefaultModel>, t: Throwable) {
                        progerssProgressDialog.dismiss()
                        Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<DefaultModel>, response: retrofit2.Response<DefaultModel>) {

                        progerssProgressDialog.dismiss()

                        try {

                            defaultModel = response.body()

                            if (defaultModel!!.getSuccess() == true)
                            {
                                val editor: SharedPreferences.Editor = sharedPreferences.edit()
                                editor.putString("userID", "0")
                                editor.apply()
                                editor.commit()
                                val intent = Intent(applicationContext, ActivityStart::class.java)
                                startActivity(intent)
                                finish()
                            }
                            else{

                                val editor: SharedPreferences.Editor = sharedPreferences.edit()
                                editor.putString("userID", "0")
                                editor.putString("Access_Token", "")
                                editor.apply()
                                editor.commit()
                                val intent = Intent(applicationContext, ActivityStart::class.java)
                                startActivity(intent)
                                finish()
                            }




                        } catch (exception: Exception) {
                            Toast.makeText(this@ActivityHome, exception.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                })

            } catch (ex: java.lang.Exception) {
                Toast.makeText(applicationContext, ex.message.toString(), Toast.LENGTH_SHORT).show()
            }

        } else {
            Toast.makeText(applicationContext, "No network available!", Toast.LENGTH_SHORT).show()
        }
    }


}
