package com.loan.izzpay.activities

import android.Manifest
import android.app.ProgressDialog
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*

import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.common.ConnectionResult
import com.loan.izzpay.R

import com.facebook.*


import org.json.JSONObject

import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.*
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import org.json.JSONException
import java.util.*


class ActivitySigin : AppCompatActivity() {
    private var tvName=""
    private var tvEmail=""
    private var fbname=""
    private var fblastname=""
    private var fbemail=""
    val RC_SIGN_IN: Int = 1
    lateinit var mGoogleSignInClient: GoogleSignInClient
    lateinit var mGoogleSignInOptions: GoogleSignInOptions
    private var callbackManager: CallbackManager? = null

    private lateinit var loginBtn: Button
    private lateinit var google_btn: ImageButton
    private lateinit var fb_btn: ImageButton

    private lateinit var Register: TextView

    lateinit var progerssProgressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_signin)

        val permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_PHONE_STATE)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_PHONE_STATE), 1)
        }


        progerssProgressDialog = ProgressDialog(this)
        progerssProgressDialog.setTitle("Please wait...")
        progerssProgressDialog.setCancelable(false)
        google_btn=findViewById(R.id.first_imagebtn1)
        fb_btn=findViewById(R.id.first_imagebtn2)
        loginBtn = findViewById(R.id.loginBtn)
        Register = findViewById(R.id.register_intent)

        loginBtn.setOnClickListener {

            startActivity(Intent(this, ActivitySignInOTPVerify::class.java))
            //loginUserByEmailDialogBox()
        }
        Register.setOnClickListener {
            startActivity(Intent(this, GauravActivityRegister::class.java))
        }

        configureGoogleSignIn()
        setupUI()

        callbackManager = CallbackManager.Factory.create()
        fb_btn.setOnClickListener {
            facebook_click()

    }
    }
                private fun facebook_click(){
                    LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"))
                    LoginManager.getInstance().registerCallback(callbackManager,
                            object : FacebookCallback<LoginResult> {
                                override fun onSuccess(result: LoginResult?) {
                                    Log.d("MainActivity", "Facebook token: " + result?.accessToken!!.token)
                                    val accessToken = result?.accessToken!!.token
                                    Log.i("accessToken", accessToken)

                                    val request=GraphRequest.newMeRequest(result.accessToken,object:GraphRequest.GraphJSONObjectCallback{
                                        override fun onCompleted(
                                                `object`: JSONObject?,
                                                response: GraphResponse?
                                        ) {
                                            Log.i("LoginActivity", response.toString());
                                            //
                                            getFacebookData(`object`!!)
                                        }
                                    });
                                    val parameters = Bundle()
                                    parameters.putString("fields", "id, first_name, last_name, email") // Parámetros que pedimos a facebook
                                    request.setParameters(parameters)
                                    request.executeAsync()

                                 //    handleFacebookAccessToken(result?.accessToken);
                                }

                                override fun onCancel() {
                                    Log.d("MainActivity", "Facebook onCancel.")
                                }

                                override fun onError(error: FacebookException?) {
                                    Log.d("MainActivity", "Facebook onError.")
//
                                }
//
                            })
                }

    private fun getFacebookData(`object`:JSONObject): Bundle? {
        try
        {
            val bundle = Bundle()
            val id = `object`.getString("id")
            Log.d("id",id)
            bundle.putString("idFacebook", id)
            if (`object`.has("first_name"))
                bundle.putString("first_name", `object`.getString("first_name"))
            fbname= bundle.getString("first_name").toString()
            if (`object`.has("last_name"))
                bundle.putString("last_name", `object`.getString("last_name"))
            fblastname= bundle.getString("last_name").toString()
            if (`object`.has("email"))
                bundle.putString("email", `object`.getString("email"))
            fbemail= bundle.getString("email").toString()

            Toast.makeText(this, "fb_details"+"\nname:"+fbname+"\nemail:"+fbemail, Toast.LENGTH_LONG).show()
            return bundle
        }
        catch (e: JSONException) {
            Log.d("FROM FA", "Error parsing JSON")
        }
        return null
    }


    private fun configureGoogleSignIn() {
        mGoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, mGoogleSignInOptions)
    }
    private fun setupUI() {
        google_btn.setOnClickListener {
            signIn()
        }
    }
    private fun signIn() {
        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
       callbackManager?.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                firebaseAuthWithGoogle()
            } catch (e: ApiException) {
                Toast.makeText(this, "Google sign in failed:(", Toast.LENGTH_LONG).show()
            }
        }
    }
    private fun firebaseAuthWithGoogle() {
        val act=GoogleSignIn.getLastSignedInAccount(applicationContext)
            if (act != null) {
                Log.i("Detail", "Display Name : ${act?.displayName}")
                Log.i("Detail", "Email : ${act?.email}")
                Toast.makeText(this, "google_details"+"\nname"+act.displayName+"\nemail"+act.email, Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this, "Google sign in failed:(", Toast.LENGTH_LONG).show()
            }
        }


}