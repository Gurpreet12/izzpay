package com.loan.izzpay.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

import android.content.ActivityNotFoundException

import android.net.Uri
import com.loan.izzpay.R


class ActivityStart : AppCompatActivity() {

    private lateinit var lets_startBtn: Button
    private lateinit var how_it_work_btn: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_start)

        lets_startBtn = findViewById(R.id.lets_startBtn) as Button
        how_it_work_btn = findViewById(R.id.how_it_work_btn)




        lets_startBtn.setOnClickListener(View.OnClickListener {
            val i = Intent(this, ActivitySigin::class.java)
            startActivity(i)
        })



        how_it_work_btn.setOnClickListener(View.OnClickListener {
            val urlString = "https://izz-pay.com/about-izz-pay.html"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(urlString))
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.setPackage("com.android.chrome")
            try {
                startActivity(intent)
            } catch (ex: ActivityNotFoundException) {
                intent.setPackage("com.amazon.cloud9")
                startActivity(intent)
            }

        })


    }
}