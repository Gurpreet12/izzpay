package com.loan.izzpay.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.cardview.widget.CardView
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.loan.izzpay.R
import com.loan.izzpay.RetrofitConnections.ApiClient_
import com.loan.izzpay.RetrofitConnections.ApiInterface
import com.loan.izzpay.activities.UploadSelfProfileImage.ActivitySelfPicUpload.*
import com.loan.izzpay.activities.UploadSelfProfileImage.ImagePickerActivity
import com.loan.izzpay.models.Mydata
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class ActivityImageUpload : AppCompatActivity() {

    private val REQUEST_IMAGE = 1
    internal var myUri: Uri? = null
    private var self_image_view: ImageView? = null
    private lateinit var self_upload: Button
    internal var finalFile: File? = null
    @SuppressLint("NewApi")


    @TargetApi(Build.VERSION_CODES.N)
    @RequiresApi(Build.VERSION_CODES.N)

    lateinit var cardViewCamera:CardView
    lateinit var cardViewGallery:CardView

    private val sharedPrefDB = "izzpay_db"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload)


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            sharedPreferences = getSharedPreferences(sharedPrefDB, Context.MODE_PRIVATE)
        }

        cardViewGallery = findViewById(R.id.cardViewGallery)
        cardViewCamera = findViewById(R.id.cardViewCamera)
        self_upload = findViewById(R.id.self_upload)
        self_image_view = findViewById(R.id.self_image_view)


        cardViewCamera.setOnClickListener(View.OnClickListener {
            Dexter.withActivity(this)
                    .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .withListener(object : MultiplePermissionsListener {


                        override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                            if (report.areAllPermissionsGranted()) {
                                launchCameraIntent()
                            }

                            if (report.isAnyPermissionPermanentlyDenied) {
                                showSettingsDialog()
                            }
                        }

                        override fun onPermissionRationaleShouldBeShown(
                                permissions: MutableList<com.karumi.dexter.listener.PermissionRequest>?,
                                token: PermissionToken?) {
                            token!!.continuePermissionRequest()
                        }
                    }).check()
        })


        cardViewGallery.setOnClickListener(View.OnClickListener {
            Dexter.withActivity(this)
                    .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .withListener(object : MultiplePermissionsListener {


                        override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                            if (report.areAllPermissionsGranted()) {
                                launchGalleryIntent()
                            }

                             if (report.isAnyPermissionPermanentlyDenied) {
                                 showSettingsDialog()
                             }
                        }

                        override fun onPermissionRationaleShouldBeShown(
                                permissions: MutableList<com.karumi.dexter.listener.PermissionRequest>?,
                                token: PermissionToken?
                        ) {
                            token!!.continuePermissionRequest()
                        }
                    }).check()
        })

        self_upload.setOnClickListener{
            uploadSelfiePhoto()

        }
    }


    private fun showSettingsDialog() {

        val builder = AlertDialog.Builder(this@ActivityImageUpload)
        builder.setTitle(getString(R.string.dialog_permission_title))
        builder.setMessage(getString(R.string.dialog_permission_message))
        builder.setPositiveButton(getString(R.string.go_to_settings)) { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton(getString(android.R.string.cancel)) { dialog, which -> dialog.cancel() }
        builder.show()

    }


    // navigating user to app settings
    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", applicationContext.packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    private fun launchCameraIntent() {
        val intent = Intent(applicationContext, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000)

        startActivityForResult(intent, REQUEST_IMAGE)
    }


    private fun launchGalleryIntent() {
        val intent = Intent(applicationContext, ImagePickerActivity::class.java)
        intent.putExtra(INTENT_IMAGE_PICKER_OPTION, REQUEST_GALLERY_IMAGE)
        intent.putExtra(INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, REQUEST_IMAGE)
    }
    @RequiresApi(api = Build.VERSION_CODES.N)

    lateinit var sharedPreferences: SharedPreferences

    fun uploadSelfiePhoto() {

        val firstName = RequestBody.create(MediaType.parse("text/plain"),  "Gaurav")

        if (null == finalFile) {
            Toast.makeText(applicationContext, "No image found", Toast.LENGTH_SHORT).show()
        } else {

            val pd = ProgressDialog(this)
            pd.setMessage("Uploading Image")
            pd.show()

            val Access_Token = sharedPreferences.getString("Access_Token", "")

            val token =  "Bearer "+ Access_Token
            val headers: HashMap<String, String> = HashMap<String, String>()
            headers.put("Authorization", token)

            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), finalFile)
            val image = MultipartBody.Part.createFormData("profileImage", finalFile!!.getName(), requestFile)

            val RetrofitInterface = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                ApiClient_.getClient().create(ApiInterface::class.java)
            } else {
                TODO("VERSION.SDK_INT < N")
            }

            val rqstCall = RetrofitInterface.uploadImage(headers,image)
            rqstCall.enqueue(object : Callback<Mydata> {
                override fun onResponse(call: Call<Mydata>, response: Response<Mydata>) {

                    pd.hide()
                    if (response.code() ==200)
                    {
                        ShowMessage("Photo uploaded successfully")
                    } else if (response.code() == 404) {
                        ShowMessage(response.message())
                    } else if (response.code() == 409) {
                        ShowMessage(response.message())
                    } else if (response.code() == 500) {
                        ShowMessage(response.message())
                    } else if (response.code() == 502) {
                        ShowMessage(response.message())
                    }

                }

                override fun onFailure(call: Call<Mydata>, t: Throwable) {
                    pd.hide()
                    ShowMessage(t.message.toString())
                }
            })
        }


    }
    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                myUri = data!!.getParcelableExtra<Uri>("path")

                finalFile = File(myUri!!.getPath()!!)
                //                Log.e("as", String.valueOf(finalFile));
                self_image_view!!.setImageURI(myUri)
            }
        }
    }
    private fun ShowMessage(s: String) {
        Toast.makeText(applicationContext, "" + s, Toast.LENGTH_SHORT).show()
    }
}