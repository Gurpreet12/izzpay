package com.loan.izzpay.activities

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.PersistableBundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.loan.izzpay.R
import com.loan.izzpay.RetrofitConnections.ApiClient
import com.loan.izzpay.models.DataModelLogin
import com.loan.izzpay.models.ModelGetAllAddress
import com.loan.izzpay.models.ModelKYCDocumentShow
import com.loan.izzpay.utils.Constant
import retrofit2.Call
import retrofit2.Callback
import java.lang.Exception

class ActivityProofs : AppCompatActivity(){




    private lateinit var cardViewUploadSelfPic:CardView
    private lateinit var cardViewUploadAadharCard:CardView
    private lateinit var cardViewUploadPanCardCardView:CardView
    private lateinit var cardViewUploadEmployeCardView:CardView
    private lateinit var bankStatementCardView:CardView
    private lateinit var salarySlipsCardView:CardView
    private lateinit var resident_proof_cardview:CardView

    private lateinit var sign_back_arrow:ImageView
    private lateinit var status_adhar_txt:TextView

    lateinit var sharedPreferences: SharedPreferences
    private val sharedPrefDB = "izzpay_db"

    lateinit var progerssProgressDialog: ProgressDialog

    var modelKYCDocumentShow: ModelKYCDocumentShow? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_proofs)

        cardViewUploadAadharCard=findViewById(R.id.cardViewUploadAadharCard)
        status_adhar_txt=findViewById(R.id.status_adhar_txt)
        sign_back_arrow=findViewById(R.id.sign_back_arrow)
        cardViewUploadPanCardCardView=findViewById(R.id.cardViewUploadPanCardCardView)
        cardViewUploadEmployeCardView=findViewById(R.id.cardViewUploadEmployeCardView)
        cardViewUploadSelfPic=findViewById(R.id.cardViewUploadSelfPic)
        bankStatementCardView=findViewById(R.id.bankStatementCardView)
        salarySlipsCardView=findViewById(R.id.salarySlipsCardView)
        resident_proof_cardview=findViewById(R.id.resident_proof_cardview)

        sharedPreferences = getSharedPreferences(sharedPrefDB, Context.MODE_PRIVATE)

        progerssProgressDialog = ProgressDialog(this@ActivityProofs)
        progerssProgressDialog.setTitle("Please wait...")
        progerssProgressDialog.setCancelable(false)

        cardViewUploadAadharCard.setOnClickListener {
            val i = Intent(this, ActivityBaseForFragment::class.java)
            i.putExtra("checkPage", "proof_aadhar")
            startActivity(i)
        }
        cardViewUploadSelfPic.setOnClickListener {
            val i = Intent(this, ActivityBaseForFragment::class.java)
            i.putExtra("checkPage", "proof_self")
            startActivity(i)
        }

        salarySlipsCardView.setOnClickListener {
            val i = Intent(this, ActivityBaseForFragment::class.java)
            i.putExtra("checkPage", "proof_salary_slip")
            startActivity(i)
        }
        resident_proof_cardview.setOnClickListener {
            val i = Intent(this, ActivityBaseForFragment::class.java)
            i.putExtra("checkPage", "proof_residental")
            startActivity(i)
        }
        cardViewUploadPanCardCardView.setOnClickListener {
            val i = Intent(this, ActivityBaseForFragment::class.java)
            i.putExtra("checkPage", "proof_pancard")
            startActivity(i)
        }
        cardViewUploadEmployeCardView.setOnClickListener {
            val i = Intent(this, ActivityBaseForFragment::class.java)
            i.putExtra("checkPage", "proof_identityCard")
            startActivity(i)
        }
        bankStatementCardView.setOnClickListener {
            val i = Intent(this, ActivityBaseForFragment::class.java)
            i.putExtra("checkPage", "bank_details")
            startActivity(i)
        }
        sign_back_arrow.setOnClickListener {
           finish()
        }

        getAllKYCDocuments()
    }

    private fun getAllKYCDocuments() {

        if (Constant.hasNetworkAvailable(applicationContext!!)) {

            val Access_Token = sharedPreferences.getString("Access_Token", "")

            progerssProgressDialog.show()

            try {
                val call: Call<ModelKYCDocumentShow> = ApiClient.getClient.showKYCProofsAndCheck("Bearer "+Access_Token)
                call.enqueue(object : Callback<ModelKYCDocumentShow> {
                    override fun onFailure(call: Call<ModelKYCDocumentShow>, t: Throwable) {
                        progerssProgressDialog.dismiss()
                        Toast.makeText(applicationContext, t.message, Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<ModelKYCDocumentShow>, response: retrofit2.Response<ModelKYCDocumentShow>) {

                        progerssProgressDialog.dismiss()

                        try {
                            if (response.code() ==200)
                            {

                                if (modelKYCDocumentShow == null)
                                {
                                    val myBody = response.body()

                                    val size = myBody!!.getData()!!.size

                                    if (myBody!!.getData()!!.size == 1)
                                    {
                                        val AadharApproved = myBody!!.getData()!!.get(0).approved

                                        if (AadharApproved == 0)
                                        {
                                            status_adhar_txt.setText("Pending")
                                            status_adhar_txt.setTextColor(ContextCompat.getColor(this@ActivityProofs,R.color.color_red))
                                        }
                                        else if (AadharApproved == 1)
                                        {
                                            status_adhar_txt.setText("Approved")
                                            status_adhar_txt.setTextColor(ContextCompat.getColor(this@ActivityProofs,R.color.color_green))
                                        }
                                    }


                                }
                                else{
                                    Toast.makeText(applicationContext, response.message(), Toast.LENGTH_SHORT).show()
                                }
                            }
                            else if (response.code() == 400)
                            {
                                Toast.makeText(applicationContext, response.message(), Toast.LENGTH_SHORT).show()
                            }
                            else if (response.code() == 404)
                            {
                                Toast.makeText(applicationContext, response.message(), Toast.LENGTH_SHORT).show()
                            }
                            else if (response.code() == 500)
                            {
                                Toast.makeText(applicationContext, response.message(), Toast.LENGTH_SHORT).show()
                            }

                        }catch (exception: Exception)
                        {
                            Toast.makeText(applicationContext, exception.message, Toast.LENGTH_SHORT).show()
                        }


                    }

                })

            } catch (ex: java.lang.Exception) {
                Toast.makeText(applicationContext, ex.message.toString(), Toast.LENGTH_SHORT).show()
            }

        } else {
            Toast.makeText(applicationContext, "No network available!", Toast.LENGTH_SHORT).show()
        }
    }
}