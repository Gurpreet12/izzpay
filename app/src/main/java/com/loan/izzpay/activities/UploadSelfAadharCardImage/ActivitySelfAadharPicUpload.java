package com.loan.izzpay.activities.UploadSelfAadharCardImage;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.loan.izzpay.R;
import com.loan.izzpay.RetrofitConnections.ApiClient_;
import com.loan.izzpay.RetrofitConnections.ApiInterface;
import com.loan.izzpay.activities.UploadSelfProfileImage.ImagePickerActivity;
import com.loan.izzpay.models.AadharUploadModel;
import com.loan.izzpay.models.ProfileModel;
import com.loan.izzpay.models.UploadKYC;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitySelfAadharPicUpload extends AppCompatActivity {

    private TextView self_btnCamera, self_gallery;
    private ImageView self_image_view;
    private Button self_upload;

    private static final String TAG = ActivitySelfAadharPicUpload.class.getSimpleName();
    public static final String INTENT_IMAGE_PICKER_OPTION = "image_picker_option";
    public static final String INTENT_ASPECT_RATIO_X = "aspect_ratio_x";
    public static final String INTENT_ASPECT_RATIO_Y = "aspect_ratio_Y";
    public static final String INTENT_LOCK_ASPECT_RATIO = "lock_aspect_ratio";
    public static final String INTENT_IMAGE_COMPRESSION_QUALITY = "compression_quality";
    public static final String INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT = "set_bitmap_max_width_height";
    public static final String INTENT_BITMAP_MAX_WIDTH = "max_width";
    public static final String INTENT_BITMAP_MAX_HEIGHT = "max_height";

    public static final int REQUEST_IMAGE_CAPTURE = 0;
    public static final int REQUEST_GALLERY_IMAGE = 1;

    private boolean lockAspectRatio = false, setBitmapMaxWidthHeight = false;
    private int ASPECT_RATIO_X = 16, ASPECT_RATIO_Y = 9, bitmapMaxWidth = 1000, bitmapMaxHeight = 1000;
    private int IMAGE_COMPRESSION = 80;
    public static String fileName;

    private static final int REQUEST_IMAGE = 1;

    Uri myUri;
    Intent intent;
    File finalFile;
    CircleImageView profile_image;
    MultipartBody.Part image;
    private ImageView self_image1;

    private EditText form_adhar_edt;

    private TextView self_text1;

    SharedPreferences sharedPreferences;
    private String sharedPrefDB = "izzpay_db";
    UploadKYC uploadKYC = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_self_pic_aadhar_upload);

        sharedPreferences = getSharedPreferences(sharedPrefDB, Context.MODE_PRIVATE);

        init();

        mListeners();

    }


    private void init()
    {
        self_btnCamera = findViewById(R.id.self_btnCamera);
        self_text1 = findViewById(R.id.self_text1);
        form_adhar_edt = findViewById(R.id.form_adhar_edt);
        self_gallery = findViewById(R.id.self_gallery);
        self_image_view = findViewById(R.id.self_image_view);
        self_upload = findViewById(R.id.self_upload);
        self_image1 = findViewById(R.id.self_image1);

        String TypeOfProof = getIntent().getExtras().getString("TypeOfProof");


        if (TypeOfProof.equals("Aadhar"))
        {
            self_text1.setText("Upload Your Aadhar Card");
            form_adhar_edt.setHint(getString(R.string.aadhar_enter));
            AadharUpload(TypeOfProof);
        }
        else  if (TypeOfProof.equals("PanCard"))
        {
            self_text1.setText("Upload Your Pan Card");
            form_adhar_edt.setHint(getString(R.string.pancard_enter));
            PanCardUpload(TypeOfProof);
        }
        else  if (TypeOfProof.equals("IdentityCard"))
        {
            self_text1.setText("Upload Your Identity Card Number");
            form_adhar_edt.setHint(getString(R.string.identity_card_enter));
            IdentityCardUpload(TypeOfProof);
        }


    }

    private void PanCardUpload(String TypeOfProof)
    {
        self_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (form_adhar_edt.getText().toString().isEmpty())
                {
                    Toast.makeText(ActivitySelfAadharPicUpload.this, "Please enter your Pan card number", Toast.LENGTH_SHORT).show();
                }
                else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        Toast.makeText(ActivitySelfAadharPicUpload.this, "Check things", Toast.LENGTH_SHORT).show();
                        uploadProof("19",TypeOfProof);
                    }
                }


            }
        });
    }


    private void AadharUpload(String TypeOfProof)
    {
        self_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (form_adhar_edt.getText().toString().isEmpty())
                {
                    Toast.makeText(ActivitySelfAadharPicUpload.this, "Please enter your aadhar card number", Toast.LENGTH_SHORT).show();
                }
                else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        uploadProof("19",TypeOfProof);
                    }
                }


            }
        });
    }


    private void IdentityCardUpload(String TypeOfProof)
    {
        self_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (form_adhar_edt.getText().toString().isEmpty())
                {
                    Toast.makeText(ActivitySelfAadharPicUpload.this, "Please enter your identity card number", Toast.LENGTH_SHORT).show();
                }
                else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                        String userID = sharedPreferences.getString("userID", "");

                        uploadProof(userID,TypeOfProof);
                    }
                }
            }
        });
    }



    private void mListeners(){

        self_btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Dexter.withActivity(ActivitySelfAadharPicUpload.this)
                        .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()) {
                                    launchCameraIntent();
                                }

                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    showSettingsDialog();
                                }
                            }
                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        }).check();


            }
        });


        self_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Dexter.withActivity(ActivitySelfAadharPicUpload.this)
                        .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()) {
                                    launchGalleryIntent();
                                }

                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    showSettingsDialog();
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        }).check();


            }
        });



        self_image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(getApplicationContext(), ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(getApplicationContext(), ImagePickerActivity.class);
        intent.putExtra(INTENT_IMAGE_PICKER_OPTION, REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void showSettingsDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySelfAadharPicUpload.this);
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getApplicationContext().getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                myUri = data.getParcelableExtra("path");

                finalFile = new File(myUri.getPath());
//                Log.e("as", String.valueOf(finalFile));
                self_image_view.setImageURI(myUri);
            }
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    public void uploadProof(String userID, String type) {

        if (null == finalFile) {
            Toast.makeText(getApplicationContext(), "No image found", Toast.LENGTH_SHORT).show();
        } else {

            ProgressDialog pd = new ProgressDialog(ActivitySelfAadharPicUpload.this);

            pd.show();

            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), finalFile);
            image = MultipartBody.Part.createFormData("image", finalFile.getName(), requestFile);

            RequestBody id = RequestBody.create(MediaType.parse("text/plain"), userID);
            RequestBody adharNo = RequestBody.create(MediaType.parse("text/plain"), form_adhar_edt.getText().toString());

            RequestBody typeRequestBody = null;
            if (type.equals("Aadhar"))
            {
                typeRequestBody = RequestBody.create(MediaType.parse("text/plain"), "Aadhar");
                pd.setMessage("Uploading Aadhar Card");
            }
            else if (type.equals("PanCard")){
                typeRequestBody = RequestBody.create(MediaType.parse("text/plain"), "PanCard");
                pd.setMessage("Uploading Pan Card");
            }
            else if (type.equals("IdentityCard")){
                typeRequestBody = RequestBody.create(MediaType.parse("text/plain"), "EmployeeIdentityCard");
                pd.setMessage("Uploading Employee Identity Card");
            }

            String Access_Token = sharedPreferences.getString("Access_Token", "");

            String token =  "Bearer "+ Access_Token;
            HashMap<String, String> headers = new HashMap<>();
            headers.put("Authorization", token);

            ApiInterface apiInterface = ApiClient_.getClient().create(ApiInterface.class);

            Call<UploadKYC> callAadharCard = apiInterface.addProofs(headers,typeRequestBody,adharNo,image);

            callAadharCard.enqueue(new Callback<UploadKYC>() {
                @Override
                public void onResponse(Call<UploadKYC> call, Response<UploadKYC> response) {

                    pd.hide();

                    if (response.code() ==200)
                    {
                        UploadKYC uploadKYC = response.body();
                        String message = uploadKYC.getMessage();

                        ShowMessage("Photo uploaded successfully");
                    } else if (response.code() == 404) {
                        ShowMessage(response.message());
                    } else if (response.code() == 409) {
                        ShowMessage(response.message());
                    } else if (response.code() == 500) {
                        ShowMessage(response.message());
                    } else if (response.code() == 502) {
                        ShowMessage(response.message());
                    }
                }

                @Override
                public void onFailure(Call<UploadKYC> call, Throwable t) {
                    pd.hide();
                    ShowMessage(t.getMessage());
                }
            });
        }


    }


    private void ShowMessage(String s) {
        Toast.makeText(getApplicationContext(), "" + s, Toast.LENGTH_SHORT).show();
    }

}
