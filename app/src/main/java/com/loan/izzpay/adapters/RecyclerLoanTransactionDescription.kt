package com.loan.izzpay.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.loan.izzpay.R
import com.loan.izzpay.dataModels.LoanTransactionDetailsPojo

class RecyclerLoanTransactionDescription(val ls_desciption: ArrayList<LoanTransactionDetailsPojo>) : RecyclerView.Adapter<RecyclerLoanTransactionDescription.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerLoanTransactionDescription.MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.loan_transaction_description, parent, false)
        return MyViewHolder(view).listen{position, type ->
            val item=ls_desciption.get(position)


        }
    }

    override fun getItemCount(): Int {
        return ls_desciption.size
    }

    override fun onBindViewHolder(holder: RecyclerLoanTransactionDescription.MyViewHolder, position: Int) {
        var notification_data = ls_desciption[position];
        holder.amount.text = "\u20B9 " + notification_data.amount.toString()
        holder.date.text = notification_data.due_date
        holder.status.text = notification_data.emi_status
        holder.text.text =notification_data.texxt


    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var date: TextView
        var status: TextView
        var amount: TextView
        var text:TextView

        init {
            text=itemView.findViewById(R.id.desc_text1)
            date = itemView.findViewById(R.id.desc_date)
            amount = itemView.findViewById(R.id.desc_amount)
            status = itemView.findViewById(R.id.desc_status)


        }

        fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
            itemView.setOnClickListener {
                event.invoke(getAdapterPosition(), getItemViewType())
            }
            return this
        }
    }

    fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
        itemView.setOnClickListener {
            event.invoke(getAdapterPosition(), getItemViewType())

        }
        return this

    }

}