package com.delivery.delpro.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.loan.izzpay.R

class SliderAdapter() : PagerAdapter() {
    lateinit var context: Context

    constructor(context: Context?) : this() {
        this.context = context!!

    }

    val image_slider = intArrayOf(
       R.drawable.image_fill_form,
       R.drawable.image_id_card,
       R.drawable.image_bank

    )
    val string_heading = arrayOf<String>("Fill Details", "Submit Proofs", "Get Your Amount")


    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`

    }


    override fun getCount(): Int {
        return string_heading.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater: LayoutInflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: ViewGroup =
            inflater.inflate(R.layout.slider_layout, container, false) as ViewGroup
        val ImageView = view.findViewById(R.id.imageView1) as ImageView
        val txt = view.findViewById(R.id.txtvw) as TextView
        ImageView.setImageResource(image_slider[position])
        txt.setText(string_heading[position])
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }
}