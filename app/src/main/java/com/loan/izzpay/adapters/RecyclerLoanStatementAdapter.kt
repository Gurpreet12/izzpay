package com.loan.izzpay.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.loan.izzpay.R
import com.loan.izzpay.activities.ActivityBaseForFragment
import com.loan.izzpay.dataModels.loan_statement_pojo

class RecyclerLoanStatementAdapter (val ls_data: ArrayList<loan_statement_pojo>) : RecyclerView.Adapter<RecyclerLoanStatementAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.loan_statement_items, parent, false)
        return MyViewHolder(view).listen{position, type ->
            val item=ls_data.get(position)


        }
    }

    override fun getItemCount(): Int {
        return ls_data.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var notification_data = ls_data[position];
        holder.amount.text = "\u20B9 " + notification_data.amount.toString()
        holder.date.text = notification_data.date
        holder.status.text = notification_data.status
        holder.tenure.text = notification_data.tenure

        holder.cardview.setOnClickListener {
            val i = Intent(it.context, ActivityBaseForFragment::class.java)
            i.putExtra("checkPage", "description")
            it.context.startActivity(i)
        }

    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var date: TextView
        var status: TextView
        var tenure: TextView
        var amount: TextView
        var cardview: CardView

        init {

            date = itemView.findViewById(R.id.stat_date)
            amount = itemView.findViewById(R.id.stat_amount)
            status = itemView.findViewById(R.id.stat_status)
            tenure = itemView.findViewById(R.id.stat_tenure)
            cardview = itemView.findViewById(R.id.cardview)

        }

        fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
            itemView.setOnClickListener {
                event.invoke(getAdapterPosition(), getItemViewType())
            }
            return this
        }
    }

    fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
        itemView.setOnClickListener {
            event.invoke(getAdapterPosition(), getItemViewType())

        }
        return this

    }
}