package com.loan.izzpay.adapters



import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

import androidx.fragment.app.FragmentPagerAdapter
import com.loan.izzpay.fragments.FragmentHome
import com.loan.izzpay.fragments.FragmentTypeOfLoans

@Suppress("DEPRECATION")
class FragmentViewPager(context:Context, fm: FragmentManager, totalTabs:Int) : FragmentPagerAdapter(fm) {

    private val myContext:Context
    private var totalTabs:Int=0
    init{
        myContext = context
        this.totalTabs = totalTabs
    }

    override fun getItem(position: Int): Fragment {
        when (position) {
           0 -> return FragmentHome() //ChildFragment1 at position 0
           1 -> return FragmentTypeOfLoans() //ChildFragment2 at position 1

        }
        return Fragment() //does not happen
    }

    override fun getCount(): Int {
        return totalTabs
    }
}
