package com.loan.izzpay.adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.loan.izzpay.R
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.loan.izzpay.activities.ActivityBaseForFragment
import com.loan.izzpay.models.InstitutesModel
import com.loan.izzpay.models.ModelsAllInstitutes


class AdapterCustomFragmentInstitutesAll(context: Context, data: ModelsAllInstitutes?) : RecyclerView.Adapter<AdapterCustomFragmentInstitutesAll.MyViewHolder>() {

    private var myArray: ModelsAllInstitutes? = null
    private var mContext: Context? = null

    init {
        this.myArray = data
        this.mContext = context
    }


    inner  class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var instituteNameTv: TextView
        var instituteEmailTv: TextView
        var institutePhoneTv: TextView
        var loanImage: ImageView

        init {

            instituteNameTv = itemView.findViewById(R.id.instituteNameTv)
            instituteEmailTv = itemView.findViewById(R.id.instituteEmailTv)
            institutePhoneTv = itemView.findViewById(R.id.institutePhoneTv)
            loanImage = itemView.findViewById(R.id.main_iconIv)

        }
    }



    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.custom_fragment_institutes, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.instituteNameTv.setText("Institute Name : "+myArray!!.getData()!!.get(position).instiName)
        holder.instituteEmailTv.setText("Email : "+myArray!!.getData()!!.get(position).iEmail)
        holder.institutePhoneTv.setText("Phone : "+myArray!!.getData()!!.get(position).iPhoneNo)

        GlideToVectorYou
                .init()
                .with(mContext as Activity?)
                .setPlaceHolder(R.drawable.ic_loan, R.drawable.ic_loan)
                .load(Uri.parse(myArray!!.getData()!!.get(position).iLogo), holder.loanImage)


//        holder.loanImage.setOnClickListener {
//            val i = Intent(mContext, ActivityBaseForFragment::class.java)
//            i.putExtra("checkPage", "educational_loan")
//            mContext!!.startActivity(i)
//
//        }


    }

    override fun getItemCount(): Int {
        return myArray!!.getData()!!.size
    }

}