package com.loan.izzpay.adapters


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.loan.izzpay.R
import android.widget.ImageView
import com.loan.izzpay.models.ListOfLoansModel
import com.loan.izzpay.pojo.NotificationModelClass

import kotlin.collections.ArrayList


class NotificationAdapter(context: Context, data: NotificationModelClass?) : RecyclerView.Adapter<NotificationAdapter.MyViewHolder>() {

    private var myArray: NotificationModelClass? = null
    private var mContext: Context? = null



    init {
        this.myArray = data
        this.mContext = context
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var text1: TextView
        var text2: TextView
        var date: TextView
        var image: ImageView? = null

        init {

            text1 = itemView.findViewById(R.id.notification_text2)
            text2 = itemView.findViewById(R.id.notification_text3)
            date = itemView.findViewById(R.id.notification_date1)
            image = itemView.findViewById(R.id.notification_image2)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.custom_notification_list, parent, false)

        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var notification_data = myArray!!.getData()!!.get(position)

        holder.text1.text = notification_data.title.toString()
        holder.text2.text = notification_data.description.toString()
        holder.date.text = notification_data.created.toString()
        holder.image!!.setImageResource(R.drawable.ic_bell_2)

    }

    override fun getItemCount(): Int {
        return myArray!!.getData()!!.size
    }


    fun removeItem(position: Int) {
//        myArray!!.getData()!!.rem(position)
        notifyItemRemoved(position)
    }

    fun restoreItem(item: NotificationModelClass, position: Int) {
//        listdata.add(position, item)
        notifyItemInserted(position)
    }

//    fun getData(): ArrayList<NotificationModelClass> {
//        return listdata
//    }
}