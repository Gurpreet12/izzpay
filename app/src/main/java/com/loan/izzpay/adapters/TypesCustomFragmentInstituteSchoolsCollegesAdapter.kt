package com.loan.izzpay.adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.loan.izzpay.R
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.loan.izzpay.activities.ActivityBaseForFragment
import com.loan.izzpay.models.InstitutesModel


class TypesCustomFragmentInstituteSchoolsCollegesAdapter(context: Context, data: InstitutesModel?) : RecyclerView.Adapter<TypesCustomFragmentInstituteSchoolsCollegesAdapter.MyViewHolder>() {

    private var myArray: InstitutesModel? = null
    private var mContext: Context? = null

    init {
        this.myArray = data
        this.mContext = context
    }


    inner  class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var titleLoanTv: TextView
        var loanImage: ImageView

        init {

            titleLoanTv = itemView.findViewById(R.id.titleLoanTv)
            loanImage = itemView.findViewById(R.id.icon)

        }
    }



    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.custom_fragment_institute, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.titleLoanTv.setText(myArray!!.getData()!!.get(position).name)

        GlideToVectorYou
                .init()
                .with(mContext as Activity?)
                .setPlaceHolder(R.drawable.ic_loan, R.drawable.ic_loan)
                .load(Uri.parse(myArray!!.getData()!!.get(position).image), holder.loanImage)


        holder.titleLoanTv.setOnClickListener {
            val i = Intent(mContext, ActivityBaseForFragment::class.java)
            i.putExtra("checkPage", "all_institutes")
            mContext!!.startActivity(i)

        }


    }

    override fun getItemCount(): Int {
        return myArray!!.getData()!!.size
    }

}