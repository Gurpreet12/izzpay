package com.loan.izzpay.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.LayoutInflater.*
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.loan.izzpay.R
import com.loan.izzpay.dataModels.FaqQuestions
import com.loan.izzpay.fragments.FragmentFaq
import com.loan.izzpay.utils.ExpandableLayout
import kotlinx.android.synthetic.main.faq_list_items.view.*

class RecyclerFaqQuestions(val que: ArrayList<FaqQuestions>) : RecyclerView.Adapter<RecyclerFaqQuestions.ViewHolder>() {
    private val expandedPositionSet: HashSet<Int> = HashSet()
    lateinit var context: Context

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.faq_list_items, parent, false)
        val vh = ViewHolder(v)
        context = parent.context
        return vh
    }

    override fun getItemCount(): Int {
        return que.size
    }

    override fun onBindViewHolder(holder: RecyclerFaqQuestions.ViewHolder, position: Int) {
        holder.itemView.faq_text1.text = que[position].questions
        holder.itemView.answer_textview.text = que[position].answer
        // holder.initialize(que.get(position), clicklistner)
        holder.itemView.expand_layout.setOnExpandListener(object :
                ExpandableLayout.OnExpandListener {
            override fun onExpand(expanded: Boolean) {
                if (expandedPositionSet.contains(position)) {
                    expandedPositionSet.remove(position)
                } else {
                    expandedPositionSet.add(position)
                }
            }
        })
        holder.itemView.expand_layout.setExpand(expandedPositionSet.contains(position))
    }
}


